﻿//Bella

var app = angular.module('app');
// add controller
app.controller('appCtrl', function appCtrl($scope) {
    // data context
    $scope.ctx = {
        multiRow: null
    };

    // create some data
    var customers = [];
    var firstNames = 'Aaron,Paul,John,Mark,Sue,Tom,Bill,Joe,Tony,Brad,Frank,Chris,Pat'.split(',');
    var lastNames = 'Smith,Johnson,Richards,Bannon,Wong,Peters,White,Brown,Adams,Jennings'.split(',');
    var cities = 'York,Paris,Rome,Cairo,Florence,Sidney,Hamburg,Vancouver'.split(',');
    var states = 'SP,RS,RN,SC,CS,RT,BC'.split(',');  
    for (var i = 0; i < 20; i++) {
        var fname= i% firstNames.length;
        var lname= i% lastNames.length;
        var city = i% cities.length;
        var state = i% states.length;
        var first = firstNames[fname],
            last = lastNames[lname];
        customers.push({
            id: i,
            name: first + ' ' + last,
            address: (i+1) * 12 + ' ' + lastNames[lname] + ' St.',
            city: cities[city],
            state: states[state],
            zip: wijmo.format('{p1:d5}-{p2:d3}', {
                p1: i * 999 + 100,
                p2: i * 9 + 100
            }),
            email: first + '.' + last + '@gmail.com',
            phone: wijmo.format('{p1:d3}-{p2:d4}', {
                p1: (i) * 9 + 100,
                p2: (i) * 99 + 10
            })
        });
    }
    var cityMap = new wijmo.grid.DataMap(cities);
    var shippers = [
        { id: 0, name: 'Speedy Express', email: 'speedy@gmail.com', phone: '431-3234', express: false },
        { id: 1, name: 'Flash Delivery', email: 'flash@gmail.com', phone: '431-6563', express: true },
        { id: 2, name: 'Logitrax', email: 'logitrax@gmail.com', phone: '431-3981', express: false },
        { id: 3, name: 'Acme Inc', email: 'acme@gmail.com', phone: '431-3113', express: true }
    ];
    var orders = [];
    var today = new Date(2016, 10, 20);
    var dt = new Date(2017, 1, 2);
    for (var i = 0; i < 20; i++) {
        var date = new Date(today.getFullYear(), i % 12, 25, i % 24, i % 60, i % 60)
        var shipped = new Date(dt.getFullYear(), i % 12, 25, i % 24, i % 60, i % 60)
        var customerId = i% customers.length;
        var shipperId = i% shippers.length;
        orders.push({
            id: i,
            date: date,
            shippedDate: shipped,
            amount: (i+1) * 99 * 1.33,
            customer: customers[customerId],
            shipper: shippers[shipperId]
        });
    }

    // expose orders to the controller
    $scope.orders = orders;

    // for validation
    $scope.showErrors = true;
    $scope.validateEdits = true;

    $scope.$watch('multiRowGrid', function () {
        var multiRowGrid = $scope.multiRowGrid;
        if (multiRowGrid) {
            multiRowGrid.errorTip = null;
        }
    });

    $scope.validateOrders = new wijmo.CollectionView(orders, {
        newItemCreator: function () {
            return {
                customer: customers[0],
                shipper: shippers[0],
                city: cities[0],
                amount: 0,
                express: false
            }
        },
        getError: function (item, property) {
            switch (property) {
                case 'city':
                    return cities.indexOf(item.city) < 0
                        ? 'Invalid city'
                        : null;
                case 'amount':
                    return item[property] < 2000
                        ? 'Cannot be less than 2,000!'
                        : null;
                case 'shipper.express':
                    return item.shipper.express && item.customer.city.match(/York|Paris|Cairo/)
                        ? 'Active items are not allowed in the York, Paris or Cairo!'
                        : null;
                case 'shipper.name':
                    return item.shipper.name.match(/^(Speedy Express|Flash Delivery|Logitrax|Acme Inc)$/)
                        ? null
                        : 'That\'s not a Beatle!!';
                case 'customer.state':
                    return item.customer.state.match(/^(SP|RS|RN|SC|CS|RT|BC)$/)
                        ? null
                        : 'That\'s not a state!!';
            }
            return null;
        }
    });

    // expose grouped orders to the controller
    $scope.groupedOrders = new wijmo.CollectionView(orders, {	
		newItemCreator: function () {
            return { // add empty customer and shipper objects to new orders
                customer: {},
                shipper: {}
            }
        },
        groupDescriptions: [
            'customer.city'
        ]
    });

    // expose paged orders to the controller
    $scope.pagedOrders = new wijmo.CollectionView(orders, {
        pageSize: 4
    });

    // create 'addNewOrders' collection, start with last item selected
    $scope.addNewOrders = new wijmo.CollectionView(orders, {
        newItemCreator: function () {
            return { // add empty customer and shipper objects to new orders
                customer: {},
                shipper: {}
            }
        },
    });
    $scope.addNewOrders.moveCurrentToLast();

    // toggle frozen rows/columns
    $scope.toggleFreeze = function (rows, cols) {
        var flex = $scope.frozenGrid;
        if (flex) {
            flex.frozenColumns = flex.frozenColumns ? 0 : cols;
            flex.frozenRows = flex.frozenRows ? 0 : rows;
        }
    }

    // add a filter to the MultiRow
    $scope.initFlexFilter = function (s, e) {
        var filter = new wijmo.grid.filter.FlexGridFilter(s);
    }

    // sample layout definitions
    $scope.ldOneLine = [
        { cells: [{ binding: 'id', header: 'ID', cssClass: 'id' }] },
        { cells: [{ binding: 'date', header: 'Ordered' }] },
        { cells: [{ binding: 'shippedDate', header: 'Shipped' }] },
        { cells: [{ binding: 'amount', header: 'Amount', format: 'c', cssClass: 'amount' }] },
        { cells: [{ binding: 'customer.name', header: 'Customer' }] },
        { cells: [{ binding: 'customer.address', header: 'Address' }] },
        { cells: [{ binding: 'customer.city', header: 'City', dataMap: cityMap }] },
        { cells: [{ binding: 'customer.state', header: 'State', width: 45 }] },
        { cells: [{ binding: 'customer.zip', header: 'Zip' }] },
        { cells: [{ binding: 'customer.email', header: 'Customer Email', cssClass: 'email' }] },
        { cells: [{ binding: 'customer.phone', header: 'Customer Phone' }] },
        { cells: [{ binding: 'shipper.name', header: 'Shipper' }] },
        { cells: [{ binding: 'shipper.email', header: 'Shipper Email', cssClass: 'email' }] },
        { cells: [{ binding: 'shipper.phone', header: 'Shipper Phone' }] },
        { cells: [{ binding: 'shipper.express', header: 'Express' }] }
    ];
    $scope.ldOneLineAggregate = [
        { cells: [{ binding: 'id', header: 'ID', cssClass: 'id' }] },
        { cells: [{ binding: 'date', header: 'Ordered', aggregate: 'Min' }] },
        { cells: [{ binding: 'shippedDate', header: 'Shipped', aggregate: 'Max' }] },
        { cells: [{ binding: 'amount', header: 'Amount', aggregate: 'Sum' }] },
        { cells: [{ binding: 'customer.name', header: 'Customer' }] },
        { cells: [{ binding: 'customer.address', header: 'Address' }] },
        { cells: [{ binding: 'customer.city', header: 'City', dataMap: cityMap }] },
        { cells: [{ binding: 'customer.state', header: 'State', width: 45 }] },
        { cells: [{ binding: 'customer.zip', header: 'Zip' }] },
        { cells: [{ binding: 'customer.email', header: 'Customer Email', cssClass: 'email' }] },
        { cells: [{ binding: 'customer.phone', header: 'Customer Phone' }] },
        { cells: [{ binding: 'shipper.name', header: 'Shipper' }] },
        { cells: [{ binding: 'shipper.email', header: 'Shipper Email', cssClass: 'email' }] },
        { cells: [{ binding: 'shipper.phone', header: 'Shipper Phone' }] },
        { cells: [{ binding: 'shipper.express', header: 'Express', aggregate: 'Cnt' }] }
    ];
    $scope.ldOneLineFormat = [
        { cells: [{ binding: 'id', header: 'ID', cssClass: 'id' }] },
        { cells: [{ binding: 'date', header: 'Ordered', format: 'MMM/dd/yyyy', aggregate: 'Min' }] },
        { cells: [{ binding: 'shippedDate', header: 'Shipped', format: 'yyyy/MM/dd', aggregate: 'Max' }] },
        { cells: [{ binding: 'amount', header: 'Amount', format: 'c0', cssClass: 'amount', aggregate: 'Sum' }] },
        { cells: [{ binding: 'customer.name', header: 'Customer' }] },
        { cells: [{ binding: 'customer.address', header: 'Address' }] },
        { cells: [{ binding: 'customer.city', header: 'City', dataMap: cityMap }] },
        { cells: [{ binding: 'customer.state', header: 'State', width: 45 }] },
        { cells: [{ binding: 'customer.zip', header: 'Zip' }] },
        { cells: [{ binding: 'customer.email', header: 'Customer Email', cssClass: 'email' }] },
        { cells: [{ binding: 'customer.phone', header: 'Customer Phone' }] },
        { cells: [{ binding: 'shipper.name', header: 'Shipper' }] },
        { cells: [{ binding: 'shipper.email', header: 'Shipper Email', cssClass: 'email' }] },
        { cells: [{ binding: 'shipper.phone', header: 'Shipper Phone' }] },
        { cells: [{ binding: 'shipper.express', header: 'Express', aggregate: 'Cnt' }] }
    ];
    $scope.ldTwoLines = [
        {
            header: 'Order', colspan: 2, cells: [
                { binding: 'id', header: 'ID', cssClass: 'id' },
                { binding: 'date', header: 'Ordered' },
                { binding: 'amount', header: 'Amount', format: 'c', cssClass: 'amount' },
                { binding: 'shippedDate', header: 'Shipped' }
            ]
        },
        {
            header: 'Customer', colspan: 3, cells: [
                { binding: 'customer.name', header: 'Name' },
                { binding: 'customer.email', header: 'EMail', colspan: 2, cssClass: 'email' },
                { binding: 'customer.address', header: 'Address' },
                { binding: 'customer.city', header: 'City', dataMap: cityMap },
                { binding: 'customer.state', header: 'State', width: 45 }
            ]
        },
        {
            header: 'Shipper', cells: [
                { binding: 'shipper.name', header: 'Shipper', colspan: 2 },
                { binding: 'shipper.email', header: 'EMail', cssClass: 'email' },
                { binding: 'shipper.express', header: 'Express' }
            ]
        }
    ];
    $scope.ldThreeLines = [
        {
            header: 'Order', colspan: 2, cells: [
                { binding: 'id', header: 'ID', colspan: 2, cssClass: 'id' },
                { binding: 'amount', header: 'Amount', format: 'c', colspan: 2, cssClass: 'amount' },
                { binding: 'date', header: 'Ordered', cssClass: 'ordered' },
                { binding: 'shippedDate', header: 'Shipped', cssClass: 'shipped' }
            ]
        },
        {
            header: 'Customer', colspan: 3, cells: [
                { binding: 'customer.name', header: 'Name', cssClass: 'customerName' },
                { binding: 'customer.email', header: 'EMail', colspan: 2, cssClass: 'customerEmail' },
                { binding: 'customer.address', header: 'Address', colspan: 2, cssClass: 'address' },
                { binding: 'customer.phone', header: 'Phone', cssClass: 'customerPhone' },
                { binding: 'customer.city', header: 'City', dataMap: cityMap, cssClass: 'city' },
                { binding: 'customer.state', header: 'State', width: 45, cssClass: 'state' },
                { binding: 'customer.zip', header: 'Zip', cssClass: 'zip' },
            ]
        },
        {
            header: 'Shipper', cells: [
                { binding: 'shipper.name', header: 'Shipper', cssClass: 'shipperName' },
                { binding: 'shipper.email', header: 'EMail', cssClass: 'shipperEmail' },
                { binding: 'shipper.express', header: 'Express', cssClass: 'express' }
            ]
        }
    ];
    $scope.customLayout = [
         {
             header: 'ID', colspan: 2, cells: [
                 { binding: 'id', header: 'ID', cssClass: 'id', width: 60 }
             ]
         },
        {
            header: 'Order', colspan: 2, cells: [
                { binding: 'date', header: 'Ordered' },
                { binding: 'amount', header: 'Amount', format: 'c', cssClass: 'amount' },
                { binding: 'shippedDate', header: 'Shipped' }
            ]
        },
        {
            header: 'Customer', colspan: 3, cells: [
                { binding: 'customer.name', header: 'Name' },
                { binding: 'customer.email', header: 'EMail', colspan: 2, cssClass: 'email' },
                { binding: 'customer.address', header: 'Address' },
                { binding: 'customer.city', header: 'City', dataMap: cityMap },
                { binding: 'customer.state', header: 'State', width: 80 }
            ]
        },
        {
            header: 'Shipper', cells: [
                { binding: 'shipper.name', header: 'Shipper', colspan: 2 },
                { binding: 'shipper.email', header: 'EMail', cssClass: 'email' }
            ]
        },
        {
            header: 'Checkbox', cells: [
                { binding: 'shipper.express', header: 'Express', cssClass: 'express' }
            ]
        }
    ];
    $scope.layoutDefs = new wijmo.CollectionView([
        {
            name: 'Traditional',
            description: 'Traditional grid view, with one row per record. The user must scroll horizontally to see the whole record.',
            def: $scope.ldOneLine
        },
        {
            name: 'Compact',
            description: 'This view uses two rows per record. The layout is divided into three groups: order, customer, and shipper',
            def: $scope.ldTwoLines
        },
        {
            name: 'Detailed',
            description: 'This view uses three rows per record. The layout is divided into three groups: order, customer, and shipper',
            def: $scope.ldThreeLines
        },
        {
            name: 'Custom',
            description: 'This view uses two rows per record. The layout is divided into 5 groups: id, order, customer, shipper and checkbox',
            def: $scope.customLayout
        }
    ]);

    var cols = '0,1,2,3,4'.split(',');  
    var rows = '0,1,2,3,4'.split(',');
    $scope.frozenCols = cols;
    $scope.frozenRows = rows;

    $scope.addFooterRow = function (s, e) {
        var row = new wijmo.grid.GroupRow();
        s.columnFooters.rows.push(row);
        s.bottomLeftCells.setCellData(0, 0, '\u03A3');
    }


});