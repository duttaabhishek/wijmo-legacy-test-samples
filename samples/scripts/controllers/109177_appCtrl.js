﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {

        chart: null,
        fruits: [],
        pos: 2
    }
    $scope.fruits = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 2000, "y2": 6000, "y3": 6000, "y4": 6000 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 4000, "y2": 4000, "y3": 5000, "y4": 2000 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 6000, "y2": 10000, "y3": 7000, "y4": 5000 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 8000, "y2": 8000, "y3": 8000, "y4": 8000 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 10000, "y2": 5000, "y3": 4000, "y4": 4000 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 12000, "y2": 2000, "y3": 9000, "y4": 10000 }

    ];

    $scope.borderClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.dataLabel.border = menu.selectedValue;
    }
    $scope.hasLabels = function () {
        var chart = $scope.ctx.chart;
        return chart && chart.dataLabel.position != 0;
    };
    $scope.positionClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.dataLabel.position = menu.selectedValue;
    }
});