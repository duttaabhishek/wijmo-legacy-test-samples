﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {

        chart: null,
        data: [],
    };

    //to test y annotation can show string value
    $scope.data = [
     { "country":"US","name": "Orange", "food": "Fried Rice", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 1, 1), "y1": 10, "y2": 8, "y3": 10, "y4": 16, "hi": 500, "lo": 100, "open": 200, "close": 600 },
     { "country": "Germany", "name": "apple", "food": "Fried Noodle", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 2, 2), "y1": 20, "y2": 12, "y3": 16, "y4": 19, "hi": 400, "lo": 200, "open": 100, "close": 700 },
     { "country": "UK", "name": "lime", "food": "Fried Egg", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 3, 3), "y1": 30, "y2": 10, "y3": 17, "y4": 15, "hi": 200, "lo": 300, "open": 300, "close": 400 },
     { "country": "Japan", "name": "lemon", "food": "Fried Vermicelli", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 4, 4), "y1": 40, "y2": 12, "y3": 15, "y4": 22, "hi": 300, "lo": 100, "open": 200, "close": 800 },
     { "country": "Italy", "name": "mango", "food": "Chicken Soup", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 5, 5), "y1": 50, "y2": 15, "y3": 23, "y4": 18, "hi": 500, "lo": 100, "open": 200, "close": 300 },
    ];
});