﻿//Bella

var app = angular.module('app');

// add controller
app.controller('appCtrl', function appCtrl($scope) {
    // data context
    $scope.ctx = {
        flex: null,
        flex2: null,
        pageSize: 0,
        groupBy: ''
    };

    var countries = ['US', 'Germany', 'UK', 'Japan', 'Italy', 'Greece'];
    var products = ['Widget', 'Gadget', 'Doohickey'];
    var colors = ['Black', 'White', 'Red', 'Green', 'Blue'];

    var data = [
        { id: 0, date: new Date(2015, 0, 1), time: new Date(2015, 0, 1, 7, 10, 10), country: 'US', product: 'Gadget', color: 'Black', countryId: 0, productId: 1, colorId: 0, amount: 474.28, discount: 0.27, active: true },
        { id: 1, date: new Date(2015, 1, 1), time: new Date(2015, 1, 1, 8, 20, 20), country: 'Germany', product: 'Widget', color: 'Red', countryId: 1, productId: 0, colorId: 2, amount: 7476.87, discount: 0.67, active: false },
        { id: 2, date: new Date(2015, 2, 1), time: new Date(2015, 2, 1, 9, 30, 30), country: 'UK', product: 'Doohickey', color: 'White', countryId: 2, productId: 2, colorId: 1, amount: 6203.51, discount: 0.15, active: false },
        { id: 3, date: new Date(2015, 3, 1), time: new Date(2015, 3, 1, 10, 40, 40), country: 'Japan', product: 'Widget', color: 'Blue', countryId: 3, productId: 0, colorId: 4, amount: 2562.53, discount: 0.90, active: true },
        { id: 4, date: new Date(2015, 4, 1), time: new Date(2015, 4, 1, 11, 50, 50), country: 'Italy', product: 'Gadget', color: 'Red', countryId: 4, productId: 1, colorId: 2, amount: 9906.36, discount: 0.48, active: false },
        { id: 5, date: new Date(2015, 5, 1), time: new Date(2015, 5, 1, 12, 5, 5), country: 'Greece', product: 'Widget', color: 'Black', countryId: 5, productId: 0, colorId: 0, amount: 7310.17, discount: 0.39, active: false },
        { id: 6, date: new Date(2015, 6, 1), time: new Date(2015, 6, 1, 1, 15, 15), country: 'US', product: 'Gadget', color: 'Red', countryId: 0, productId: 1, colorId: 2, amount: 2443.69, discount: 0.37, active: true },
        { id: 7, date: new Date(2015, 7, 1), time: new Date(2015, 7, 1, 2, 25, 25), country: 'Germany', product: 'Doohickey', color: 'White', countryId: 1, productId: 2, colorId: 1, amount: 6989.79, discount: 0.79, active: false },
        { id: 8, date: new Date(2015, 8, 1), time: new Date(2015, 8, 1, 3, 35, 35), country: 'UK', product: 'Gadget', color: 'Blue', countryId: 2, productId: 1, colorId: 4, amount: 8618.44, discount: 0.59, active: false },
        { id: 9, date: new Date(2015, 9, 1), time: new Date(2015, 9, 1, 4, 45, 45), country: 'Japan', product: 'Doohickey', color: 'Black', countryId: 3, productId: 2, colorId: 0, amount: 5466.72, discount: 0.02, active: true },
        { id: 10, date: new Date(2015, 10, 1), time: new Date(2015, 10, 1, 5, 55, 55), country: 'US', product: 'Gadget', color: 'Green', countryId: 0, productId: 1, colorId: 3, amount: 818.44, discount: 0.33, active: false },
        { id: 11, date: new Date(2015, 11, 1), time: new Date(2015, 11, 1, 6, 0, 0), country: 'Germany', product: 'Doohickey', color: 'White', countryId: 1, productId: 2, colorId: 1, amount: 566.72, discount: 0.25, active: false }
    ];

    var data2 = [
        { id: 0, date: new Date(2015, 0, 1), time: new Date(2015, 0, 1, 7, 10, 10), country: 'US', product: 'Gadget', color: 'Black', countryId: 0, productId: 1, colorId: 0, amount: 474.28, discount: 0.27, active: true },
        { id: 1, date: new Date(2015, 1, 1), time: new Date(2015, 1, 1, 8, 20, 20), country: 'Germany', product: 'Widget', color: 'Red', countryId: 1, productId: 0, colorId: 2, amount: 7476.87, discount: 0.67, active: false },
        { id: 2, date: new Date(2015, 2, 1), time: new Date(2015, 2, 1, 9, 30, 30), country: 'UK', product: 'Doohickey', color: 'White', countryId: 2, productId: 2, colorId: 1, amount: 6203.51, discount: 0.15, active: false },
        { id: 3, date: new Date(2015, 3, 1), time: new Date(2015, 3, 1, 10, 40, 40), country: 'Japan', product: 'Widget', color: 'Blue', countryId: 3, productId: 0, colorId: 4, amount: 2562.53, discount: 0.90, active: true },
        { id: 4, date: new Date(2015, 4, 1), time: new Date(2015, 4, 1, 11, 50, 50), country: 'Italy', product: 'Gadget', color: 'Red', countryId: 4, productId: 1, colorId: 2, amount: 9906.36, discount: 0.48, active: false }
    ];

    var data3 = [
        { id: 0, date: new Date(2015, 0, 1), time: new Date(2015, 0, 1, 7, 10, 10), country: 'US', product: 'Gadget', color: 'Black', countryId: 0, productId: 1, colorId: 0, amount: 4741.288345324, discount: 0.04, active: true },
        { id: 1, date: new Date(2015, 1, 1), time: new Date(2015, 1, 1, 8, 20, 20), country: 'Germany', product: 'Widget', color: 'Red', countryId: 1, productId: 0, colorId: 2, amount: 7476.870845134, discount: 0.14, active: false },
        { id: 2, date: new Date(2015, 2, 1), time: new Date(2015, 2, 1, 9, 30, 30), country: 'UK', product: 'Doohickey', color: 'White', countryId: 2, productId: 2, colorId: 1, amount: 6203.511230789, discount: 0.67, active: false },
        { id: 3, date: new Date(2015, 3, 1), time: new Date(2015, 3, 1, 10, 40, 40), country: 'Japan', product: 'Widget', color: 'Blue', countryId: 3, productId: 0, colorId: 4, amount: 2562.533495201, discount: 0.05, active: true },
        { id: 4, date: new Date(2015, 4, 1), time: new Date(2015, 4, 1, 11, 50, 50), country: 'Italy', product: 'Gadget', color: 'Red', countryId: 4, productId: 1, colorId: 2, amount: 9906.013690017, discount: 0.48, active: false }
    ];

    //for HTML formatted data
    var htmlData = [
        { id: 0, date: new Date(2015, 0, 1), time: new Date(2015, 0, 1, 7, 10, 10), country: '<b>US</b>', product: '<b>Gadget&amp;Widget</b>', color: 'Black', countryId: 0, productId: 1, colorId: 0, amount: 474.28, discount: 0.27, active: true },
        { id: 1, date: new Date(2015, 1, 1), time: new Date(2015, 1, 1, 8, 20, 20), country: '<i>Germany</i>', product: '<b><i>Widget</i></b>', color: 'Red', countryId: 1, productId: 0, colorId: 2, amount: 7476.87, discount: 0.67, active: false },
        { id: 2, date: new Date(2015, 2, 1), time: new Date(2015, 2, 1, 9, 30, 30), country: 'UK', product: 'Doohickey', color: 'White', countryId: 2, productId: 2, colorId: 1, amount: 6203.51, discount: 0.15, active: false },
        { id: 3, date: new Date(2015, 3, 1), time: new Date(2015, 3, 1, 10, 40, 40), country: 'Japan', product: 'Widget', color: 'Blue', countryId: 3, productId: 0, colorId: 4, amount: 2562.53, discount: 0.90, active: true },
        { id: 4, date: new Date(2015, 4, 1), time: new Date(2015, 4, 1, 11, 50, 50), country: 'Italy', product: 'Gadget', color: 'Red', countryId: 4, productId: 1, colorId: 2, amount: 9906.36, discount: 0.48, active: false }
    ];
    $scope.htmlData = new wijmo.CollectionView(htmlData);
    var htmlView = new wijmo.CollectionView(htmlData);
    htmlView.groupDescriptions.push(new wijmo.PropertyGroupDescription('country'));
    $scope.htmlView = htmlView;

    // expose data
    $scope.data = new wijmo.CollectionView(data);
    $scope.data2 = new wijmo.CollectionView(data2);
    $scope.data3 = new wijmo.CollectionView(data3);

    $scope.initialized = function (s, e) {
        var flex = s;
        $scope.filter = new wijmo.grid.filter.FlexGridFilter(flex);
        $scope.amountColumnFilterType = wijmo.grid.filter.FilterType.Condition;
        $scope.$apply();
    }

    $scope.initializedFilter = function (s, e) {
        var flex = s;
        //$scope.filter = new wijmo.grid.filter.FlexGridFilter(flex);

        var filter = new wijmo.grid.filter.FlexGridFilter(flex);
        // assign list of unique values to country filter
        var uniqueCountries = 'US,UK'.split(',');
        var cf = filter.getColumnFilter('country');
        cf.valueFilter.uniqueValues = uniqueCountries;

        cf.valueFilter.sortValues = true;
    }

    $scope.initializedValueFilter1 = function (s, e) {
        var flex = s;
        var filter = new wijmo.grid.filter.FlexGridFilter(flex);
        var cf = filter.getColumnFilter('country');
        cf.valueFilter.sortValues = true;
    }
    $scope.initializedValueFilter2 = function (s, e) {
        var flex = s;
        var filter = new wijmo.grid.filter.FlexGridFilter(flex);
        var cf = filter.getColumnFilter('country');
        cf.valueFilter.sortValues = false;
    }

    $scope.initUnbound = function (s, e) {
        if ($scope.ctx.flex) {
            var flex = $scope.ctx.flex;
            flex.allowResizing = wijmo.grid.AllowResizing.Both;
            flex.allowDragging = wijmo.grid.AllowDragging.Both;
            // add 50 rows, 10 columns
            for (var r = 0; r < 50; r++) {
                flex.rows.push(new wijmo.grid.Row());
            }
            for (var c = 0; c < 10; c++) {
                flex.columns.push(new wijmo.grid.Column());
            }
            // populate the scrollable area
            for (var r = 0; r < flex.rows.length; r++) {
                for (var c = 0; c < flex.columns.length; c++) {
                    flex.setCellData(r, c, 'r' + r + ',c' + c);
                }
            }
            // add 3 rows to the column header and 3 columns to the row header panels
            for (var i = 0; i < 3; i++) {
                flex.columnHeaders.rows.insert(0, new wijmo.grid.Row());
                flex.rowHeaders.columns.insert(0, new wijmo.grid.Column());
            }
            // populate the fixed area
            var p = flex.columnHeaders;
            for (var r = 0; r < p.rows.length; r++) {
                for (var c = 0; c < p.columns.length; c++) {
                    p.setCellData(r, c, 'cHdr r' + r + ',c' + c);
                }
            }
            p = flex.rowHeaders;
            for (var r = 0; r < p.rows.length; r++) {
                for (var c = 0; c < p.columns.length; c++) {
                    p.setCellData(r, c, 'rHdr r' + r + ',c' + c);
                }
            }
            p = flex.topLeftCells;
            for (var r = 0; r < p.rows.length; r++) {
                for (var c = 0; c < p.columns.length; c++) {
                    p.setCellData(r, c, 'tl r' + r + ',c' + c);
                }
            }
        }
    }

    // update filter type for "downloads" column
    $scope.$watch('amountColumnFilterType', function () {
        var f = $scope.filter;
        if (f) {
            var col = f.grid.columns.getColumn('amount'),
                cf = f.getColumnFilter(col, true);
            cf.filterType = $scope.amountColumnFilterType;
        }
    });

    //for group data
    var view = new wijmo.CollectionView(data);
    view.groupDescriptions.push(new wijmo.PropertyGroupDescription('country'));
    $scope.view = view;

    var view2 = new wijmo.CollectionView(data);
    view2.groupDescriptions.push(new wijmo.PropertyGroupDescription('product'));
    $scope.view2 = view2;

    $scope.$watch('ctx.flex2', function () {
        // update data maps, formatting, and headers when the grid is populated
        updateHeaders();
    });

    function updateHeaders() {
        var flex = $scope.ctx.flex2;
        if (flex) {

            // insert new row if not yet
            if (flex.columnHeaders.rows.length === 1) {
                flex.columnHeaders.rows.insert(0, new wijmo.grid.Row());
            }
            var row = flex.columnHeaders.rows[0];
            row.allowMerging = true;

            // set headings so the cells merge
            for (var i = 0; i < flex.columns.length; i++) {
                var hdr = 'String';
                switch (flex.columns[i].binding) {
                    case 'id':
                    case 'amount':
                    case 'discount':
                        hdr = 'Number';
                        break;
                    case 'date':
                    case 'time':
                        hdr = 'Date';
                        break;
                    case 'active':
                        hdr = 'Boolean';
                        break;
                }
                flex.columnHeaders.setCellData(0, i, hdr);
            }
        }
    }

    $scope.$watch('ctx.groupBy', function () {
        if ($scope.ctx.flex) {

            // get the collection view, start update
            var cv = $scope.ctx.flex.collectionView;
            cv.beginUpdate();

            // clear existing groups
            cv.groupDescriptions.clear();

            // add new groups
            var groupNames = $scope.ctx.groupBy.split('/'),
                groupDesc;
            for (var i = 0; i < groupNames.length; i++) {
                var propName = groupNames[i].toLowerCase();
                if (propName == 'amount') {

                    // group amounts in ranges
                    // (could use the mapping function to group countries into continents, 
                    // names into initials, etc)
                    groupDesc = new wijmo.PropertyGroupDescription(propName, function (item, prop) {
                        var value = item[prop];
                        if (value > 1000) return 'Large Amounts';
                        if (value > 100) return 'Medium Amounts';
                        if (value > 0) return 'Small Amounts';
                        return 'Negative';
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else if (propName) {

                    // group other properties by their specific values
                    groupDesc = new wijmo.PropertyGroupDescription(propName);
                    cv.groupDescriptions.push(groupDesc);
                }
            }

            // done updating
            cv.endUpdate();
        }
    });

    $scope.cultures = [
        { name: 'English', value: 'en' },
        { name: 'Japanese', value: 'ja' },
        { name: 'German', value: 'de' },
        { name: 'Chinese', value: 'zh' },
        { name: 'Portuguese', value: 'pt' },
        { name: 'Russian', value: 'ru' },
    ];
    $scope.culture = 'en';
    $scope.$watch('culture', function () {

        // remove old localization reference
        var loc = document.getElementById('loc');
        if (loc) {
            document.head.removeChild(loc);
        }

        // add new localization reference
        loc = document.createElement('script');
        loc.id = 'loc';
        loc.type = 'text/javascript';
        loc.async = false;
        loc.src = '../scripts/vendor/wijmo.culture.' + $scope.culture + '.min.js';
        document.getElementsByTagName('head')[0].appendChild(loc);

        // show changes
        invalidateAll();
    });
    function invalidateAll() {
        wijmo.Control.invalidateAll();

        $scope.ctx.flex.invalidate();
    }

    // add freezing handle to the grid
    $scope.initGrid = function (s, e) {
        setTimeout(function () {
            addFreezeBar(s);
        }, 100);
    }

    $scope.$watch('ctx.pageSize', function () {
        var flex = $scope.ctx.flex;
        if (flex && $scope.ctx.pageSize != null) {
            var cv = flex.collectionView;
            cv.pageSize = $scope.ctx.pageSize;
        }
    });

    // add a freezing handle to a FlexGrid
    function addFreezeBar(flex) {

        // create freeze handle
        var freezeBar = document.createElement('div'),
            host = flex.hostElement,
            freeze = -1,
            dragging = false;
        wijmo.addClass(freezeBar, 'freeze-bar');

        // add handle to flex
        var root = host.querySelector('[wj-part="root"]');
        root.parentElement.appendChild(freezeBar);
        positionfreezeBar(flex, freezeBar, flex.frozenColumns);

        // attach event handlers
        flex.columns.collectionChanged.addHandler(function () {

            // update bar position in case user dragged columns
            positionfreezeBar(flex, freezeBar, flex.frozenColumns);
        });
        host.addEventListener('mousedown', function (e) {
            var el = document.elementFromPoint(e.clientX, e.clientY);
            if (el == freezeBar) {

                // prepare to freeze
                flex.frozenColumns = 0;
                flex.scrollPosition = new wijmo.Point(0, flex.scrollPosition.y);
                dragging = true;
                freeze = -1;
                host.style.cursor = 'col-resize';

                // done with this event
                e.preventDefault();
                e.stopPropagation();
            }
        }, true);
        host.addEventListener('mousemove', function (e) {
            if (dragging) {

                // find closest edge
                var minDist = null;
                for (var i = 0; i < flex.columns.length; i++) {
                    var rc = flex.getCellBoundingRect(0, i),
                        dist = Math.abs(rc.left - e.clientX);
                    if (minDist == null || dist < minDist) {
                        minDist = dist;
                        freeze = i;
                    }
                }

                // move element to edge
                positionfreezeBar(flex, freezeBar, freeze);

                // done with this event
                e.preventDefault();
                e.stopPropagation();
            }
        }, true);
        host.addEventListener('mouseup', function (e) {
            if (dragging) {

                // stop dragging, apply new freeze value
                dragging = false;
                if (freeze > -1) {
                    flex.frozenColumns = freeze;
                }
                host.style.cursor = '';

                // done with this event
                e.preventDefault();
                e.stopPropagation();
            }
            else {

                // update bar position in case user resized columns
                positionfreezeBar(flex, freezeBar, flex.frozenColumns);
            }
        }, true);
    }
    function positionfreezeBar(flex, div, frozen) {
        var left = flex.getCellBoundingRect(0, frozen).left;
        left -= div.parentElement.getBoundingClientRect().left;
        div.style.left = left + 'px';
    }

    // expose countries, country map
    $scope.countries = countries;
    var countryMap = [
        { name: 'US', key: 0 },
        { name: 'Germany', key: 1 },
        { name: 'UK', key: 2 },
        { name: 'Japan', key: 3 },
        { name: 'Italy', key: 4 },
        { name: 'Greece', key: 5 }
    ];
    $scope.countryMap = new wijmo.grid.DataMap(new wijmo.CollectionView(countryMap), 'key', 'name');

    // expose products, product map
    $scope.products = products;
    var productMap = [
        { name: 'Widget', key: 0 },
        { name: 'Gadget', key: 1 },
        { name: 'Doohickey', key: 2 }
    ];
    $scope.productMap = new wijmo.grid.DataMap(new wijmo.CollectionView(productMap), 'key', 'name');

    // expose colors, color map
    $scope.colors = colors;
    var colorMap = [
        { name: 'Black', key: 0 },
        { name: 'White', key: 1 },
        { name: 'Red', key: 2 },
        { name: 'Green', key: 3 },
        { name: 'Blue', key: 4 }
    ];
    $scope.colorMap = new wijmo.grid.DataMap(new wijmo.CollectionView(colorMap), 'key', 'name');

    $("#chkAutoClipboard").change(function () {
        var flex = $scope.ctx.flex;
        flex.autoClipboard = $("#chkAutoClipboard").is(":checked");
    });

    $scope.col_format = function () {
        var flex = $scope.ctx.flex;
        var col = flex.columns[3];
        col.format = "yyyy/MMM/d";
    };

    $("#chkRequired").change(function () {
        var flex = $scope.ctx.flex;
        for (var i = 0; i < flex.columns.length; i++) {
            var col = flex.columns[i];
            col.required = $("#chkRequired").is(":checked");
        }
    });

    $("#chkIsReadOnly").change(function () {
        var flex = $scope.ctx.flex;
        for (var i = 0; i < flex.columns.length; i++) {
            var col = flex.columns[i];
            col.isReadOnly = $("#chkIsReadOnly").is(":checked");
        }
    });

    $("#chkDisabled").change(function () {
        $scope.ctx.flex.isDisabled = $("#chkDisabled").is(":checked");
    });

    $scope.flexFilterApplied = function (sender, args) {
        var value = "filterApplied.";
        $("#EventList").append('<li>' + value + '</li>');
    }

    $scope.flexFilterChanged = function (sender, args) {
        var value = "filterChanged.";
        $("#EventList").append('<li>' + value + '</li>');
    }

    $scope.flexFilterChanging = function (sender, args) {
        var value = "filterChanging.";
        $("#EventList").append('<li>' + value + '</li>');
    }

    $scope.flex_Dispose = function () {
        var flex = $scope.ctx.flex;
        flex.dispose();
    };

    $("#chkShowDropDown").change(function () {
        var flex = $scope.ctx.flex;
        var col = flex.columns[1];
        col.showDropDown = $("#chkShowDropDown").is(":checked");
    });

    $scope.cols_clear = function () {
        var flex = $scope.ctx.flex;
        flex.columns.clear();
    };

    $scope.cols_removeAt = function () {
        var flex = $scope.ctx.flex;
        flex.columns.removeAt(0);
    };

    $scope.rows_clear = function () {
        var flex = $scope.ctx.flex;
        flex.rows.clear();
    };

    $scope.rows_removeAt = function () {
        var flex = $scope.ctx.flex;
        flex.rows.removeAt(0);
    };

    $scope.set_selectedRows = function () {
        var flex = $scope.ctx.flex;
        var rows = new Array();
        rows[0] = flex.rows[1];
        rows[1] = flex.rows[3];
        rows[3] = flex.rows[5];
        flex.selectedRows = rows;
    };

    $scope.set_CellData = function () {
        var flex = $scope.ctx.flex;
        flex.setCellData(1, 1, 'Test (1,1)', true);
        flex.setCellData(1, 4, new Date(2015, 11, 31), true);
        flex.setCellData(1, 5, '123.45', true);
    };

    $scope.col_isContentHtml = function () {
        var flex = $scope.ctx.flex;
        var col2 = flex.columns[2];
        col2.isContentHtml = !col2.isContentHtml;
        var col3 = flex.columns[3];
        col3.isContentHtml = !col3.isContentHtml;
    };

    $scope.$watch('ctx.aggregate', function () {
        var flex = $scope.ctx.flex;
        var col4 = flex.columns[4];
        col4.aggregate = $scope.ctx.aggregate;
        var col5 = flex.columns[5];
        col5.aggregate = $scope.ctx.aggregate;
        var col6 = flex.columns[6];
        col6.aggregate = $scope.ctx.aggregate;
        var col7 = flex.columns[7];
        col7.aggregate = $scope.ctx.aggregate;
    });

    $scope.cvRefresh = function () {
        var cv = $scope.ctx.flex.collectionView;
        cv.refresh();
    };


    $scope.initFinancial = function (s, e) {
        var f = new wijmo.grid.filter.FlexGridFilter(s);
        var g = new wijmo.grid.columngroups.ColumnGroupProvider(s, fundColumns);
        g.selectOnClick = true;
        s.itemsSource = fundData;
    }

    // financial example data

    var fundData = [{
        name: 'Constant Growth IXTR',
        currency: 'USD',
        perf: {
            ytd: .0523,
            m1: 0.0142,
            m6: 0.0443,
            m12: 0.0743
        },
        alloc: {
            stock: 0.17,
            bond: 0.32,
            cash: 0.36,
            other: 0.15
        }
    }, {
        name: 'Optimus Prime MMCT',
        currency: 'EUR',
        perf: {
            ytd: .0343,
            m1: 0.043,
            m6: 0.0244,
            m12: 0.0543
        },
        alloc: {
            stock: 0.61,
            bond: 0.8,
            cash: 0.9,
            other: 0.22
        }
    }, {
        name: 'Serenity Now ZTZZZ',
        currency: 'YEN',
        perf: {
            ytd: .0522,
            m1: 0.0143,
            m6: 0.0458,
            m12: 0.0732
        },
        alloc: {
            stock: 0.66,
            bond: 0.09,
            cash: 0.19,
            other: 0.06
        }
    }];
    var fundColumns = [{
        header: 'Name',
        binding: 'name',
        width: '2*'
    }, {
        header: 'Curr',
        binding: 'currency',
        width: '*'
    }, {
        header: 'Performance',
        columns: [{
            header: 'YTD',
            binding: 'perf.ytd',
            format: 'p2',
            width: '*'
        }, {
            header: '1 M',
            binding: 'perf.m1',
            format: 'p2',
            width: '*'
        }, {
            header: '6 M',
            binding: 'perf.m6',
            format: 'p2',
            width: '*'
        }, {
            header: '12 M',
            binding: 'perf.m12',
            format: 'p2',
            width: '*'
        }]
    }, {
        header: 'Allocation',
        columns: [{
            header: 'Stocks',
            binding: 'alloc.stock',
            format: 'p0',
            width: '*'
        }, {
            header: 'Bonds',
            binding: 'alloc.bond',
            format: 'p0',
            width: '*'
        }, {
            header: 'Cash',
            binding: 'alloc.cash',
            format: 'p0',
            width: '*'
        }, {
            header: 'Other',
            binding: 'alloc.other',
            format: 'p0',
            width: '*'
        }]
    }];

    var cols = ',product,country,active,product/country,product/country/amount'.split(',');
    $scope.groupByCols = cols;
    var pages = '0,1,2,3,4,5,6,7,8,9,10'.split(',');    
    $scope.pageSizes = pages;

    $scope.addFooterRow = function (s, e) {
        var row = new wijmo.grid.GroupRow(); // create a GroupRow to show aggregates
        s.columnFooters.rows.push(row); // add the row to the column footer panel
        s.bottomLeftCells.setCellData(0, 0, '\u03A3'); // sigma on the header
    }

    // initialize grid's sticky toolbar
    $scope.initStickyToolbar = function (s, e) {
        // move header element into grid layout
        var host = s.hostElement,
            hdr = document.querySelector('.grid-header');
        hdr.style.position = 'relative';
        hdr.style.zIndex = 10;
        host.insertBefore(hdr, host.children[0]);
        // make header 'sticky'
        var sticky = s.columnHeaders.hostElement.parentElement;
        s.updatedLayout.addHandler(function () {
            hdr.style.top = sticky.style.top;
            wijmo.toggleClass(hdr, 'wj-state-sticky', wijmo.hasClass(sticky, 'wj-state-sticky'));
        });
    };

    // toggle filter, group panel
    $scope.toggleFilter = function () {
        var filter = $scope.ctx.theFilter;
        filter.showFilterIcons = !filter.showFilterIcons;
    }
    $scope.toggleGroupPanel = function () {
        var style = $scope.ctx.theGroupPanel.hostElement.style;
        style.display = (style.display == 'none' ? '' : 'none');
        $scope.ctx.flex.invalidate(); // force layout update
        // wijmo.Control.invalidateAll(); also works, but less efficient...
    }



});