(function() {
	'use strict';

	angular
		.module('app', ['wj'])
		.controller('appCtrl', function($scope) {
			$scope.minDate = new Date(2014, 7, 1);
			$scope.maxDate = new Date(2015, 0, 1);

			$scope.minDateStr = "2014-08-01";
			$scope.maxDateStr = "2015-01-01";

			$scope.value = new Date();
		});
})();