﻿(function () {
    'use strict';

    angular
		.module('app', ['wj'])
		.controller('appCtrl', function ($scope) {
		    $scope.data = [
				{
				    text: 'Item 1',
				    value: 5
				},
				{
				    text: 'Item 2',
				    value: 10
				},
				{
				    text: 'Item 3',
				    value: 15
				}
			];

		    $scope.ctx = {
		        chart: null
		    };

		    $scope.$watch('ctx.chart', function () {
		        if ($scope.ctx.chart) {
		            var chart = $scope.ctx.chart;
		            chart.beginUpdate();
		            chart.series[0].symbolMarker = "Box";
		            chart.endUpdate();
		        }
		    });

		});
})();