﻿'use strict';

// declare app module
var app = angular.module('app');

// controller
app.controller('135482_appCtrl', function ($scope, $http) {

	$scope.ctx = {
		cal: null,
		today: new Date(),
        maxDate: new Date(2015, 12, 31),
        minDate: new Date(2015, 1, 1)
	}
	

	$("#chkDisabled").change(function () {
		$scope.ctx.cal.isDisabled = $("#chkDisabled").is(":checked");
	});

});