﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    // data context
    $scope.ctx = {
        chart: null,
        pal: 0,
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        data: [],
        groupWidth: '70%'
    };
    $scope.menu = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 900, "y2": 600, "y3": 400, "y4": 300 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 800, "y2": 400, "y3": 500, "y4": 200 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 700, "y2": 700, "y3": 700, "y4": 500 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 600, "y2": 800, "y3": 800, "y4": 600 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 500, "y2": 100, "y3": 300, "y4": 100 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7),"y1": 400, "y2": 200, "y3": 900, "y4": 400 },
	  { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 0, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 300, "y2": 300, "y3": 600, "y4": 700 },

    ];

    $scope.menuPaletteItemClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.palette = wijmo.chart.Palettes[$scope.ctx.palettes[menu.selectedIndex]];
    }

    $scope.menuGroupWidthItemClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.options = { groupWidth: menu.selectedValue };
    }

    $scope.isColumnOrBar = function () {
        var chart = $scope.ctx.chart;
        return chart && (chart.chartType == wijmo.chart.ChartType.Column || chart.chartType == wijmo.chart.ChartType.Bar);
    };

    // populate data
    var names = ['Oranges', 'Apples', 'Pears', 'Bananas', 'Pineapples'],
        data = [];
    for (var i = 0; i < names.length; i++) {
        $scope.ctx.data.push({
            name: names[i],
            mar: Math.random() * 3,
            apr: Math.random() * 10,
            may: Math.random() * 5
        });
    }

    $scope.yMax = function () {
        var fchart = $scope.ctx.chart;
        fchart.beginUpdate();
        fchart.axisY.max = 400;
        fchart.endUpdate();
    };
    $scope.yMin = function () {
        var fchart = $scope.ctx.chart;
        fchart.beginUpdate();
        fchart.axisY.min = 200;
        fchart.endUpdate();
    };
    $scope.xMax = function () {
        var fchart = $scope.ctx.chart;
        fchart.beginUpdate();
        fchart.axisX.max = 850;
        fchart.endUpdate();
    };
    $scope.xMin = function () {
        var fchart = $scope.ctx.chart;
        fchart.beginUpdate();
        fchart.axisX.min = 200;
        fchart.endUpdate();
    };
});
