﻿'use strict';

var app = angular.module("app", ['wj']);

app.controller('appCtrl', function ($scope) {
    $scope.ctx = {
    flexChart: null,
    pieChart:null,
    chartAni: { animationMode: 'Point' },
    pieAni: { animationMode: 'Point' },
    data: []
    };
    $scope.data = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 900, "y2": 600, "y3": 400, "y4": 300 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 800, "y2": 400, "y3": 500, "y4": 200 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 700, "y2": 700, "y3": 700, "y4": 500 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 600, "y2": 800, "y3": 800, "y4": 600 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 500, "y2": 100, "y3": 300, "y4": 100 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 400, "y2": 200, "y3": 900, "y4": 400 },
	  { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 0, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 300, "y2": 300, "y3": 600, "y4": 700 },
    ];
    //Update FlexChart 
    $scope.$watch('ctx.flexChart', function () {
        var flexChart = $scope.ctx.flexChart;
        if (!flexChart) {
            return;
        }
        updateChart();
    });

    $scope.$watch('ctx.chartAni.animationMode', function () {
        var flexChart = $scope.ctx.flexChart,
            animationMode = $scope.ctx.chartAni.animationMode;

        if (!animationMode || animationMode === '') {
            return;
        }

        updateChart();
    });

    function updateChart() {
        var flexChart = $scope.ctx.flexChart;

        if (flexChart) {
            flexChart.refresh(true);
        }
    }

    //Update FlexPie
    $scope.$watch('ctx.pieChart', function () {
        var pieChart = $scope.ctx.pieChart;
        if (!pieChart) {
            return;
        }
        updatePieChart();
    });
    $scope.$watch('ctx.pieAni.animationMode', function () {
        var pieChart = $scope.ctx.pieChart,
            animationMode = $scope.ctx.pieAni.animationMode;

        if (!animationMode || animationMode === '') {
            return;
        }
        updatePieChart();
    });

    function updatePieChart() {
        var pieChart = $scope.ctx.pieChart;

        if (pieChart) {
            pieChart.refresh(true);
        }
    }

    //Added by Selena
    // generate some random data
    function getData(numCount) {
        var data = new wijmo.ObservableArray();
        //var data = [];

        for (var i = 0; i < numCount; i++) {
            data.push(getRandomData('random' + getRandomValue(1000)));
        }
        return data;
    }

    function getRandomData(idx) {
        return {
            //x: getRandomValue(100),
            x: idx,
            y: getRandomValue(200),
            y1: getRandomValue(400),
            y2: getRandomValue(600),
            y3: getRandomValue(800),
            y4: getRandomValue(1000)
        };
    }

    function getRandomValue(max) {
        return Math.round(Math.random() * max);
    }

    var flexChartPoints = 10;

    $scope.randomData = getData(flexChartPoints);

    //Item Add
    $scope.itemAdd = function (args) {
        var idx = args.selectedIndex;
        if (idx > -1) {
            func('add', idx);
        }
    };
    //Click item Remove
    $scope.itemRemove = function (args) {
        var idx = args.selectedIndex;
        if (idx > -1) {
            func('remove', idx);
        }
    };

    //Function to add/remove points
    function func(oper, idx) {
        var str = '', funcName;
        if (idx === 1) {
            str = 'FirstPoint';
        } else if (idx === 2) {
            str = 'LastPoint';
        }
        funcName = oper + 'ChartSeries' + str;
        $scope[funcName]();
    }

    //Add first point
    $scope.addChartSeriesFirstPoint = function () {
        $scope.randomData.insert(0, getRandomData('added' + getRandomValue(1000)));
    };
    //Add last point
    $scope.addChartSeriesLastPoint = function () {
        $scope.randomData.push(getRandomData('added' + getRandomValue(1000)));
    };
    //Remove First point
    $scope.removeChartSeriesFirstPoint = function () {
        if ($scope.randomData.length) {
            $scope.randomData.removeAt(0);
        }
    };
    //Remove Last point
    $scope.removeChartSeriesLastPoint = function () {
        if ($scope.randomData.length) {
            $scope.randomData.pop();
        }
    };
    //Remove Series
    $scope.removeChartSeries = function () {
        var chart = $scope.ctx.flexChart;

        if (chart.series.length <= 0) {
            return;
        }
        chart.series.pop();
    };


});
