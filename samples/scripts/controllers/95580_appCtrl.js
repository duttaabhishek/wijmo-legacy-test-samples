﻿'use strict';

// declare app module
var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope, $compile, dataSvc) {


    // data context
    $scope.ctx = {
        flex: null,
        flexDataMap: true,
        data: dataSvc.getData(3),
        countries: dataSvc.getCountries(),
        dataMaps: true,
        products: dataSvc.getProducts(),
        colors: dataSvc.getColors(),
        culture: 'en'
    };

    $scope.cvGroup = new wijmo.CollectionView($scope.ctx.data);
    $scope.groupBy = '';

    // update CollectionView group descriptions when groupBy changes
    $scope.$watch('groupBy', function () {
        var cv = $scope.cvGroup;
        cv.groupDescriptions.clear();
        if ($scope.groupBy) {
            var groupNames = $scope.groupBy.split(',');
            for (var i = 0; i < groupNames.length; i++) {
                var groupName = groupNames[i];
                if (groupName == 'amount') { // group amounts in ranges
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.amount >= 5000 ? '> 5,000' : item.amount >= 500 ? '500 to 5,000' : '< 500';
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else { // group everything else by value
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName);
                    cv.groupDescriptions.push(groupDesc);
                }
            }
        }
    });

    // connect to flexDataMap when it becomes available
    $scope.$watch('ctx.flexDataMap', function () {
        var flexDataMap = $scope.ctx.flexDataMap;
        if (flexDataMap) {

            // notify AngularJS of selection changes
            flexDataMap.selectionChanged.addHandler(function () {
                $scope.current = flexDataMap.collectionView ? flexDataMap.collectionView.currentItem : null;
                if (!$scope.$$phase) {
                    $scope.$apply('current');
                    $scope.$apply('ctx.flexDataMap.selection');
                }
            });

            // update data maps, formatting, paging now and when the itemsSource changes
            updateDataMaps();
            flexDataMap.itemsSourceChanged.addHandler(function () {
                updateDataMaps();
                if (flexDataMap.collectionView && $scope.ctx.pageSize != null) {
                    flexDataMap.collectionView.pageSize = $scope.ctx.pageSize;
                }
            });

            // keep the control in edit mode
            flexDataMap.selectionChanged.addHandler(function () {
                if ($scope.ctx.alwaysEdit == true) {
                    setTimeout(function () {
                        flexDataMap.startEditing(false);
                    }, 50);
                }
            });
        }
    });

    // build a data map from a string array using the indices as keys
    function buildDataMap(items) {
        var map = [];
        for (var i = 0; i < items.length; i++) {
            map.push({ key: i, value: items[i] });
        }
        return new wijmo.grid.DataMap(map, 'key', 'value');
    }

    // apply/remove data maps
    function updateDataMaps() {
        var flexDataMap = $scope.ctx.flexDataMap;
        if (flexDataMap) {
            var colCountry = flexDataMap.columns.getColumn('countryId');
            var colProduct = flexDataMap.columns.getColumn('productId');
            var colColor = flexDataMap.columns.getColumn('colorId');
            if (colCountry && colProduct && colColor) {
                if ($scope.ctx.dataMaps == true) {

                    //Old method till build 22   
                    //colCountry.dataMap = new wijmo.grid.DataMap(dataSvc.getCountries());
                    //colProduct.dataMap = new wijmo.grid.DataMap(dataSvc.getProducts());
                    //colColor.dataMap = new wijmo.grid.DataMap(dataSvc.getColors());

                    colCountry.dataMap = buildDataMap(dataSvc.getCountries());
                    colProduct.dataMap = buildDataMap(dataSvc.getProducts());
                    colColor.dataMap = buildDataMap(dataSvc.getColors());

                    colColor.showDropDown = false;

                } else {
                    colCountry.dataMap = null;
                    colProduct.dataMap = null;
                    colColor.dataMap = null;
                }
            }
        }
    }


    // as an array
    $scope.ctx.countries = [
        'Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua', 'Argentina', 'Armenia',
        'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize',
        'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire', 'Bosnia', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi',
        'Cambodia', 'Cameroon', 'Canada', 'Canary Islands', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Channel Islands',
        'Chile', 'China', 'Christmas Island', 'Cocos Island', 'Colombia', 'Comoros', 'Congo', 'Cook Islands', 'Costa Rica', "Cote D'Ivoire",
        'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador',
        'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland',
        'France', 'French Guiana', 'French Polynesia', 'French Southern Ter', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar',
        'Great Britain', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guyana', 'Haiti', 'Honduras',
        'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan',
        'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea North', 'Korea South', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho',
        'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malaysia', 'Malawi', 'Maldives',
        'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Midway Islands', 'Moldova', 'Monaco',
        'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Nambia', 'Nauru', 'Nepal', 'Netherland Antilles', 'Netherlands', 'Nevis',
        'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Norway', 'Oman', 'Pakistan', 'Palau Island',
        'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Island', 'Poland', 'Portugal', 'Puerto Rico',
        'Qatar', 'Republic of Montenegro', 'Republic of Serbia', 'Reunion', 'Romania', 'Russia', 'Rwanda', 'St Barthelemy', 'St Eustatius',
        'St Helena', 'St Kitts-Nevis', 'St Lucia', 'St Maarten', 'Saipan', 'Samoa', 'Samoa American', 'San Marino', 'Saudi Arabia', 'Scotland',
        'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa',
        'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Tahiti', 'Taiwan', 'Tajikistan', 'Tanzania',
        'Thailand', 'Togo', 'Tokelau', 'Tonga', 'Trinidad Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks & Caicos Is', 'Tuvalu', 'Uganda',
        'Ukraine', 'United Arab Emirates', 'UK', 'United Kingdom', 'US', 'United States of America', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City State',
        'Venezuela', 'Vietnam', 'Virgin Islands (British)', 'Virgin Islands (USA)', 'Wake Island', 'Yemen', 'Zaire', 'Zambia', 'Zimbabwe'
    ];


});
