﻿'use strict';

// declare app module
var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope, $compile, dataSvc) {

    // generate some random data
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
        data = [];
    for (var i = 0; i < 7; i++) {
        data.push({
            id: i,
            country: countries[i % countries.length],
            date: new Date(2014, i % 12, i % 28),
            amount: (i%123)* 10000,
            active: i % 4 == 0
        });
    }
   
   $scope.AddNewRow=function()
   {
	var data=$scope.ctx.flex.collectionView;
             data.newItemCreator=function(){
                       var newId=data.itemCount;
                       return {id:newId,
                       		country:countries[ newId % countries.length],
                       		date: new Date(2014, newId % 12, newId % 28),
                       		amount:(newId%123)*1000,
                       		active:newId%2==0};
             };
             data.addNew();
             data.addNew();
             data.addNew();
             data.addNew();
             data.addNew();


             data.commitNew();
   }

    // add data array to scope
    $scope.data =new wijmo.CollectionView(data);

    // initialize selection mode
    $scope.selectionMode = 'CellRange';

    // expose the data as a CollectionView to show grouping
    $scope.cvGroup = new wijmo.CollectionView(data);
    $scope.groupBy = '';
    
    // create items, round numbers and dates so there's no rounding
    var items = dataSvc.getData(7);
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        item.amount = item.amount.toFixed(2) * 1;
        item.start = new Date(item.start.getFullYear(), item.start.getMonth(), item.start.getDate());
    }

    // create CollectionView
    var view = new wijmo.CollectionView(items);

    // track changes
    view.trackChanges = true;


    // data context
    $scope.ctx = {
        flex: null,
        flexDataMap: true,
        data: dataSvc.getData(500),
        countries: dataSvc.getCountries(),
        dataMaps: true,
        products: dataSvc.getProducts(),
        colors: dataSvc.getColors(),
        culture: 'en',
        dataTrack: view
    };   

    // expose the data as a CollectionView to show filtering
    $scope.filter = '';
    var toFilter, lcFilter;
    $scope.cvFilter = new wijmo.CollectionView(data);
    $scope.cvFilter.pageSize = 10;
    $scope.cvFilter.filter = function (item) { // ** filter function
        if (!$scope.filter) {
            return true;
        }
        return item.country.toLowerCase().indexOf(lcFilter) > -1;
    };
    $scope.$watch('filter', function () { // ** refresh view when filter changes
        if (toFilter) {
            clearTimeout(toFilter);
        }
        toFilter = setTimeout(function () {
            lcFilter = $scope.filter.toLowerCase();
            $scope.cvFilter.refresh();
        }, 500);
    });

    // update CollectionView group descriptions when groupBy changes
    $scope.$watch('groupBy', function () {
        var cv = $scope.cvFilter;
        cv.groupDescriptions.clear();
        if ($scope.groupBy) {
            var groupNames = $scope.groupBy.split(',');
            for (var i = 0; i < groupNames.length; i++) {
                var groupName = groupNames[i];
                if (groupName == 'date') { // group dates by year
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        //return item.date.getFullYear();
                        //81476 Sample Modification
                        return item.date ? item.date.getFullYear() : 0;
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else if (groupName == 'amount') { // group amounts in ranges
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.amount >= 5000 ? '> 5,000' : item.amount >= 500 ? '500 to 5,000' : '< 500';
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else { // group everything else by value
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName);
                    cv.groupDescriptions.push(groupDesc);
                }
            }
        }
    });

    // expose the data as a CollectionView to show paging
    $scope.cvPaging = new wijmo.CollectionView(data);
    $scope.cvPaging.pageSize = 10;

    // get the color to be used for displaying an amount
    $scope.getAmountColor = function (amount) {
        if (amount < 500) return 'darkred';
        if (amount < 2500) return 'black';
        return 'darkgreen';
    }

    // as an array
    $scope.ctx.countries = [
        'Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua', 'Argentina', 'Armenia',
        'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize',
        'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire', 'Bosnia', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi',
        'Cambodia', 'Cameroon', 'Canada', 'Canary Islands', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Channel Islands',
        'Chile', 'China', 'Christmas Island', 'Cocos Island', 'Colombia', 'Comoros', 'Congo', 'Cook Islands', 'Costa Rica', "Cote D'Ivoire",
        'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador',
        'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland',
        'France', 'French Guiana', 'French Polynesia', 'French Southern Ter', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar',
        'Great Britain', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guyana', 'Haiti', 'Honduras',
        'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan',
        'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea North', 'Korea South', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho',
        'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malaysia', 'Malawi', 'Maldives',
        'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Midway Islands', 'Moldova', 'Monaco',
        'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Nambia', 'Nauru', 'Nepal', 'Netherland Antilles', 'Netherlands', 'Nevis',
        'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Norway', 'Oman', 'Pakistan', 'Palau Island',
        'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Island', 'Poland', 'Portugal', 'Puerto Rico',
        'Qatar', 'Republic of Montenegro', 'Republic of Serbia', 'Reunion', 'Romania', 'Russia', 'Rwanda', 'St Barthelemy', 'St Eustatius',
        'St Helena', 'St Kitts-Nevis', 'St Lucia', 'St Maarten', 'Saipan', 'Samoa', 'Samoa American', 'San Marino', 'Saudi Arabia', 'Scotland',
        'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa',
        'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Tahiti', 'Taiwan', 'Tajikistan', 'Tanzania',
        'Thailand', 'Togo', 'Tokelau', 'Tonga', 'Trinidad Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks & Caicos Is', 'Tuvalu', 'Uganda',
        'Ukraine', 'United Arab Emirates', 'UK','United Kingdom', 'US','United States of America', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City State',
        'Venezuela', 'Vietnam', 'Virgin Islands (British)', 'Virgin Islands (USA)', 'Wake Island', 'Yemen', 'Zaire', 'Zambia', 'Zimbabwe'
    ];

    // ** inline editing
    $scope.$watch('ctx.flexInline', function () {
        var flex = $scope.ctx.flexInline;
        if (flex) {

            // prevent default editing
            flex.isReadOnly = true;

            // make rows taller to accommodate edit buttons
            flex.rows.defaultSize = 36;

            // use formatter to create buttons and custom editors
            flex.itemFormatter = itemFormatter;

            // commit row changes when scrolling the grid
            flex.scrollPositionChanged.addHandler(function () {
                if ($scope.ctx.editIndex > -1) {
                    $scope.commitRow($scope.ctx.editIndex);
                }
            });
        }
    });
    function itemFormatter(panel, r, c, cell) {
        if (panel.cellType == wijmo.grid.CellType.Cell) {
            var col = panel.columns[c],
                html = cell.innerHTML;
            if (r == $scope.ctx.editIndex) {
                switch (col.name) {
                    case 'buttons':
                        html = '<div>' +
                                '&nbsp;&nbsp;' +
                                '<button class="btn btn-primary btn-sm" ng-click="commitRow(' + r + ')">' +
                                    '<span class="glyphicon glyphicon-ok"></span> OK' +
                                '</button>' +
                                '&nbsp;&nbsp;' +
                                '<button class="btn btn-warning btn-sm" ng-click="cancelRow(' + r + ')">' +
                                    '<span class="glyphicon glyphicon-ban-circle"></span> Cancel' +
                                '</button>' +
                            '</div>';
                        break;
                    case 'date':
                        html = '<input id="theDate" class="form-control" value="' + panel.getCellData(r, c, true) + '"/>';
                        break;
                    case 'country':
                        html = '<input id="theCountry" class="form-control" value="' + panel.getCellData(r, c, true) + '"/>';
                        break;
                }
            } else {
                switch (col.name) {
                    case 'buttons':
                        cell.style.padding = '3px';
                        html = '<div>' +
                                '&nbsp;&nbsp;' +
                                '<button class="btn btn-default btn-sm" ng-click="editRow(' + r + ')">' +
                                    '<span class="glyphicon glyphicon-pencil"></span> Edit' +
                                '</button>' +
                                '&nbsp;&nbsp;' +
                                '<button class="btn btn-default btn-sm" ng-click="deleteRow(' + r + ')">' +
                                    '<span class="glyphicon glyphicon-remove"></span> Delete' +
                                '</button>' +
                            '</div>';
                        break;
                }
            }

            // update cell and compile its contents into the scope 
            // (required to wire up the ng-click directives)
            if (html != cell.innerHTML) {
                cell.innerHTML = html;
                cell.style.padding = '3px';
                $compile(cell)($scope);
            }
        }
    }
    $scope.editRow = function (row) {
        $scope.ctx.editIndex = row;
        $scope.ctx.flexInline.rows[row].height = 44;
    }
    $scope.deleteRow = function (row) {
        var ecv = $scope.ctx.flexInline.collectionView;
        ecv.removeAt(row);
    }
    $scope.commitRow = function (row) {

        // save changes
        var flex = $scope.ctx.flexInline;
        flex.setCellData(row, 'date', $("#theDate").val());
        flex.setCellData(row, 'country', $("#theCountry").val());

        // done editing
        $scope.cancelRow(row);
    }
    $scope.cancelRow = function (row) {
        $scope.ctx.editIndex = -1;
        $scope.ctx.flexInline.rows[row].height = -1;
    }
});
