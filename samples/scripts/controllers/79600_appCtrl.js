﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {

        chart: null,
        chart1: null,
        chart2: null,
        chart3: null,
        chart4: null,
        data: [],
        menu: []

    };

    //date binding
    $scope.menu = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 800, "y2": 600, "y3": 200, "y4": 300 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 600, "y2": 400, "y3": 300, "y4": 200 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 800, "y2": 700, "y3": 500, "y4": 500 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 1000, "y2": 900, "y3": 400, "y4": 600 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 1200, "y2": 1100, "y3": 600, "y4": 1000 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 6, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 1000, "y2": 1300, "y3": 800, "y4": 1200 },
	  { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 7, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 1400, "y2": 1500, "y3": 1000, "y4": 1400 },
    ];

    //string number binding
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(',');
    for (var i = 0; i < countries.length; i++) {
        $scope.ctx.data.push({
            country: countries[i],
            downloads: Math.round(Math.random() * 20000),
            sales: Math.random() * 10000,
            expenses: Math.random() * 5000
        });
    }
});