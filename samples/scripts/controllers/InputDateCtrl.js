﻿'use strict';

var app = angular.module('app');

// input controller: provides a list of countries and some input values
app.controller('inputDateCtrl', function appCtrl($scope, $filter, $location) {

    // data context
    $scope.ctx = {

        // data
        value: new Date(2014, 5, 17),
        displayDate: new Date(2015, 0, 1),
        maxDate: new Date(2015, 5, 6),
        minDate: new Date(2013, 3, 4),
        inpDate1: null,
        

        // culture
        culture: 'en'
    };

    $scope.set_focus = function () {

        var inpDate = $scope.ctx.inpDate1;

        inpDate.focus();

    };

    // when the culture changes, load the new culture, apply, and invalidate
    $scope.$watch('ctx.culture', function () {
        $.ajax({
            url: '/scripts/vendor/wijmo.culture.' + $scope.ctx.culture + '.min.js',
            dataType: 'script',
            success: function (data) {
                eval(data);

                // culture applied, now load translations
                $.ajax({
                    url: '/translations/strings.' + $scope.ctx.culture + '.js',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        $scope.i18n = data;

                        // show changes
                        $scope.$apply();
                        invalidateWijmoControls(document.body);
                    }
                });
            },
        });
    });

    // invalidate all Wijmo controls on the page
    function invalidateWijmoControls(e) {
        var ctl = wijmo.Control.getControl(e);
        if (ctl) {
            ctl.invalidate();
        }
        if (e.children) {
            for (var i = 0; i < e.children.length; i++) {
                invalidateWijmoControls(e.children[i]);
            }
        }
    }

     $scope.itemFormatter = function (date, element) {
        var weekday = date.getDay(),
            holiday = getHoliday(date);
        wijmo.toggleClass(element, 'date-weekend', weekday == 0 || weekday == 6);
        wijmo.toggleClass(element, 'date-holiday', holiday);
        element.title = holiday;
    }
    $scope.itemValidator = function (date, element) {
        switch (date.getDay()) {
            case 0:
            case 6:
                return false; // no appointments on weekends
        }
        if (getHoliday(date)) {
            return false; // no appointments on holidays
        }
        return true; // not a weekend or a holiday, this date is OK
    }

    // gets the holiday for a given date
    function getHoliday(date) {
        var day = date.getDate(),
            month = date.getMonth() + 1;
        switch (month + '/' + day) { // simple holidays (fixed dates)
            case '1/1': return 'New Year\'s Day';
            case '6/14': return 'Flag Day';
            case '7/4': return 'Independence Day';
            case '11/11': return 'Veteran\'s Day';
            case '12/25': return 'Christmas Day';
        }
        var weekDay = date.getDay(),
            weekNum = Math.floor((day - 1) / 7) + 1;
        switch (month + '/' + weekNum + '/' + weekDay) {
            case '1/3/1': return 'Martin Luther King\'s Birthday'; // 3rd Mon/Jan
            case '2/3/1': return 'Washington\'s Birthday'; // 3rd Mon/Feb
            case '5/3/6': return 'Armed Forces Day'; // 3rd Sat/May
            case '9/1/1': return 'Labor Day'; // 1st Mon/Sep
            case '10/2/1': return 'Columbus Day'; // 2nd Mon/Oct
            case '11/4/4': return 'Thanksgiving Day'; // 4th Thu/Nov
        }
        return ''; // no holiday today
    }
    
});
