﻿// get application
var app = angular.module('app');

// add controller
app.controller('appCtrl', function ($scope) {
		    $scope.fruits = [
     { "name": "Orange", "x": 1, "date1": new Date(2014, 1, 1), "y1": 20, "y2": 8, "y3": 10, "y4": 16 },
     { "name": "apple", "x": 2, "date1": new Date(2014, 2, 2), "y1": 22, "y2": 12, "y3": 16, "y4": 19 },
     { "name": "lime", "x": 3, "date1": new Date(2014, 3, 3), "y1": 19, "y2": 10, "y3": 17, "y4": 15 },
     { "name": "lemon", "x": 4, "date1": new Date(2014, 4, 4), "y1": 24, "y2": 12, "y3": 15, "y4": 22 },
     { "name": "mango", "x": 5, "date1": new Date(2014, 5, 5), "y1": 25, "y2": 15, "y3": 23, "y4": 18 },
			];


		    $scope.ctx = {
		        chart: null
		    };

		    $scope.$watch('ctx.chart', function () {
		        if ($scope.ctx.chart) {
		            var chart = $scope.ctx.chart;
		            chart.beginUpdate();
		            chart.axisX.reversed = true;
		            chart.axisY.reversed = true;
		            chart.endUpdate();
		        }
		    });

		});
