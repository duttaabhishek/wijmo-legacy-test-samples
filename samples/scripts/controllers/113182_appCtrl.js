﻿// get application
var app = angular.module('app');

// add controller
app.controller('appCtrl', function appCtrl($scope) {

    data = [
        { country: 'US', sales: 4553.43, expenses: 4928.23, downloads: 18180 },
        { country: 'Germany', sales: 6734.56, expenses: 8453.24, downloads: 4328 },
        { country: 'UK', sales: 8234.05, expenses: 8324.23, downloads: 23470 },
        { country: 'Japan', sales: 9234.53, expenses: 1367.54, downloads: 21356 }
    ];

    // expose data as a CollectionView to get events
    $scope.data = new wijmo.CollectionView(data);

//    $scope.data = [
//        { country: 'US', sales: 4553.43, expenses: 4928.23, downloads: 18180 },
//        { country: 'Germany', sales: 6734.56, expenses: 8453.24, downloads: 4328 },
//        { country: 'UK', sales: 8234.05, expenses: 8324.23, downloads: 23470 },
//        { country: 'Japan', sales: 9234.53, expenses: 1367.54, downloads: 21356 }
//    ];

    $scope.toggle = function () {
        $scope.hidden = !$scope.hidden;
        $scope.data.refresh();
    }

});
