﻿'use strict';

angular.module('app').controller('standaloneController', function ($scope) {
	$scope.ctx = {
		flexSheet: null,
		sheets: []
	};

	// initialize the flexSheet control when document ready.
    $scope.initialized = function (s) {
        var column;

        for (var i = 0; i < s.sheets.length; i++) {
            s.sheets.selectedIndex = i;
            if (s.sheets[i].name === 'Sheet1') {
                generateFirstSheet(s);
            }

            if (s.sheets[i].name === 'Sheet2') {
                generateSecondSheet(s);
            }

            if (s.sheets[i].name === 'Sheet3') {
                generateThirdSheet(s);
            }

            $scope.ctx.sheets.push(s.sheets[i].name);
        }
        s.selectedSheetIndex = 0;
        s.refresh();
        $scope.$apply('ctx.sheets');

        var flexSheet = s;
        flexSheet.beginningEdit.addHandler(function (sender, e) {
            if (e.row == 1 && e.col == 0) {
                e.cancel = true;
            }
        });
    };

    function generateFirstSheet(flexSheet) {
        flexSheet.setCellData(0, 0, 'First sheet');
    }

    function generateSecondSheet(flexSheet) {
        flexSheet.setCellData(0, 0, 'Second sheet');
    }

    function generateThirdSheet(flexSheet) {
        flexSheet.setCellData(0, 0, 'Third sheet');
    }

    $scope.sheetVisible = function () {
        var flexSheet = $scope.ctx.flexSheet;
        var sheet = flexSheet.sheets[0];
        sheet.visible = !sheet.visible;
//        alert(sheet.visible);
    };
		

});