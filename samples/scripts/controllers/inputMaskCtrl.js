﻿'use strict';

var app = angular.module('app');

// input controller: provides a list of countries and some input values
app.controller('inputMaskCtrl', function appCtrl($scope, $filter, $location, dataSvc) {

    // data context
    $scope.ctx = {

        // data
        today: new Date($.now()),
        inpMask1: null,
        displayDate: new Date(2015, 0, 1),
        maxDate: new Date($.now()),
        minDate: new Date($.now()),
        mask: '>LL-AA-0000',
        value:'ABC21234',
        promptChar:'~',
        placeholder:'EnterInputData',
        // culture
        culture: 'en'
    };



    // when the culture changes, load the new culture, apply, and invalidate
    $scope.$watch('ctx.culture', function () {
        $.ajax({
            url: '../scripts/vendor/wijmo.culture.' + $scope.ctx.culture + '.min.js',
            dataType: 'script',
            success: function (data) {
                eval(data);

                // culture applied, now load translations
                $.ajax({
                    url: 'translations/strings.' + $scope.ctx.culture + '.js',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        $scope.i18n = data;

                        // show changes
                        $scope.$apply();
                        invalidateWijmoControls(document.body);
                    }
                });
            },
        });
    });

    $scope.get_maskFull = function () {

        var inpMask = $scope.ctx.inpMask1;

        alert("maskFull:"+inpMask.maskFull);
        
    };

    $scope.set_focus = function () {

        var inpMask = $scope.ctx.inpMask1;
        //alert("testing");
        inpMask.focus();

    };

    $scope.selectAll = function () {

        var inpMask = $scope.ctx.inpMask1;

        inpMask.selectAll();

    };



    $scope.maskValueChanged = function (sender, args) {
        //alert("chkvalueChanged event fired");
        if ($("#chkvalueChanged").is(":checked")) {
            var va = sender;
            var value = "valueChanged event fired";

            $("#EventList").append('<li>' + va.value + " :" + value + '</li>');

        }


    }
    // invalidate all Wijmo controls on the page
    function invalidateWijmoControls(e) {
        var ctl = wijmo.Control.getControl(e);
        if (ctl) {
            ctl.invalidate();
        }
        if (e.children) {
            for (var i = 0; i < e.children.length; i++) {
                invalidateWijmoControls(e.children[i]);
            }
        }
    }


});
