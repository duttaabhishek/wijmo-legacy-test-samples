﻿'use strict';

var app = angular.module('app', ['wj']);

// input controller: provides a list of countries and some input values
app.controller('inputPopupCtrl', function inputPopupCtrl($scope, $filter, $location) {

    $scope.ctx = {
        pop: null
    };
    $scope.hiding = function (s, e) {
        switch (s.dialogResult) {

            // process the form variables here... 
            case 'submit':
                console.log('** Submitting **');
                break;

            // show 'create account' dialog 
            case 'wj-hide-create':
                $scope.dialogs.create.show(true);
                break;

            // ignore the form changes... 
            default:
                console.log('Closing without submitting...');
                break;
        }
    }
    
});
