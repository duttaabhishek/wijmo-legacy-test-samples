﻿'use strict';

var app = angular.module('app', ['wj']);
app.controller('136333_appCtrl', function appCtrl($scope) {

    // dialogs
    $scope.modal = true;
    $scope.showDialog = function (dlgName) {
        var dlg = $scope[dlgName];
        if (dlg) {
            var inputs = dlg.hostElement.querySelectorAll('input');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type != 'checkbox') {
                    inputs[i].value = '';
                }
            }
            dlg.modal = $scope.modal;
            dlg.hideTrigger = dlg.modal ? wijmo.input.PopupTrigger.None : wijmo.input.PopupTrigger.Blur;
            dlg.show();
        }
    };

    $scope.hideDialog = function (evt, msg) {

        // hide dialog
        for (var e = evt.target; e; e = e.parentElement) {
            if (wijmo.hasClass(e, 'wj-popup')) {
                var dlg = wijmo.Control.getControl(e);
                dlg.hide();
            }
        }

        // show alert
        alert(msg);
    }
});