﻿'use strict';

var app = angular.module('app', ['wj']);
app.controller('125534_appCtrl', function appCtrl($scope) {

    // handle menu clicks: this method gets invoked when the menu's itemClicked event fires
    $scope.menuItemClicked = function (sender, args) {
        var menu = sender;
        alert('Thanks for selecting option ' + menu.selectedIndex + ' from menu **' + menu.header + '**!');
    }

});