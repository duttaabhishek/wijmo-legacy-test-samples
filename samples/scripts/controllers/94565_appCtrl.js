﻿'use strict';

// declare app module
var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope) {

    // generate some random data
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
        data = [];
    for (var i = 0; i < 10; i++) {
        data.push({
            id: i,
            company:"Company "+i,
            country: countries[i % countries.length],
            date: new Date(2014, i % 12, i % 28),
            amount: (i % 123) * 10000,
            active: i % 4 == 0
        });
    }    

    // add data array to scope
    $scope.data = new wijmo.CollectionView(data);

    // initialize selection mode
    $scope.selectionMode = 'CellRange';
    $scope.groupBy = '';

    // expose the data as a CollectionView to show filtering
     // initialize filter
    $scope.filter = {        
        country: ''
    };
    var toFilter, lcFilter;
    $scope.cvData = new wijmo.CollectionView(data);
    $scope.cvData.pageSize = 12;
    // apply filter
    $scope.cvData.filter = function (item) {
        var fa = $scope.filter.country;
        if (fa && fa.indexOf('(all') < 0 && item.country.toLowerCase().indexOf(fa.toLowerCase()) < 0) {
            return false;
        }
        // all passed
        return true;
    };
    // update CollectionView group descriptions when groupBy changes
    $scope.$watch('groupBy', function () {
        var cv = $scope.cvData;
        cv.groupDescriptions.clear();
        if ($scope.groupBy) {
            var groupNames = $scope.groupBy.split(',');
            for (var i = 0; i < groupNames.length; i++) {
                var groupName = groupNames[i];
                if (groupName == 'date') { // group dates by year
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        
                        ///return item.date.getFullYear(); (TFS:81476)
                        return item.date ? item.date.getFullYear() : 0; 

                    });
                    cv.groupDescriptions.push(groupDesc);
                } else if (groupName == 'amount') { // group amounts in ranges
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.amount >= 5000 ? '> 5,000' : item.amount >= 500 ? '500 to 5,000' : '< 500';
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else { // group everything else by value
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName);
                    cv.groupDescriptions.push(groupDesc);
                }
            }
        }
    });

    $scope.flex = null;
    $scope.$watch('flex', function () {
        var flex = $scope.flex;
        if (flex) {
            // enable merging
            flex.allowMerging = wijmo.grid.AllowMerging.ColumnHeaders;

            // add extra column header row
            var row = new wijmo.grid.Row(),
                ch = flex.columnHeaders;

            // initialize header cells
            row.allowMerging = true;
            for (var i = 0; i < flex.columns.length; i++) {
                flex.columns[i].allowMerging = true;
            }
            ch.rows.insert(0, row);
            setHeader(ch, 0, 0, 1, 0, 'Company Name');
            setHeader(ch, 0, 1, 0, 1, 'Country');
            setHeader(ch, 1, 1, 1, 1, '');
            setHeader(ch, 0, 2, 0, 4, 'Other Information');
            ch.rows[1].height = ch.rows.defaultSize + 12;

            // enable custom item formatter
            flex.itemFormatter = itemFormatter;
        }
    });

    $scope.countries = new wijmo.ObservableArray();
    $scope.cvData.collectionChanged.addHandler(function () {
        updateCountries();
    });
    updateCountries();

    // update country list
    function updateCountries() {
        var countries = $scope.countries;

        countries.beginUpdate();
        // empty the array, see:
        // http://stackoverflow.com/questions/1232040/how-to-empty-an-array-in-javascript
        while (countries.length > 0) countries.pop();

        // add empty string so user can remove the filter
        countries.push('(all countries)');

        // populate avoiding duplicates
        var items = $scope.cvData.sourceCollection;
        for (var i = 0; i < items.length; i++) {
            var a = items[i].country;
            if (countries.indexOf(a) < 0) {
                countries.push(a);
            }
        }

        // sort countries
        countries.sort();
        countries.endUpdate();
    }

    // country filter
    var cmbCountry = new wijmo.input.ComboBox($('<div/>')[0], {
        placeholder: 'country',
        itemsSource: $scope.countries,
        isEditable : false
    });
    cmbCountry.textChanged.addHandler(function () {
        updateFilter('country', cmbCountry.text);
    });
    cmbCountry.isDroppedDownChanged.addHandler(function () {
        for (var p = cmbCountry.hostElement; p; p = p.parentElement) {
            if (wijmo.hasClass(p, 'wj-colheaders')) {
                p.parentElement.style.overflow = cmbCountry.isDroppedDown ? 'visible' : 'hidden';
            }
        }
    });
    stopPropagation(cmbCountry.hostElement);

   // active filter
    var chkActive = $('<input type="checkbox"/>');

    // watch filter, refresh collection 300ms after the last change
    var toFilter = null;
    function updateFilter(part, value) {
        if ($scope.filter[part] != value) {

            // update filter
            $scope.filter[part] = value;

            // reschedule update
            if (toFilter) clearTimeout(toFilter);
            toFilter = setTimeout(function () {

                // refresh view, keep focused element
                var focused = document.activeElement;
                $scope.cvData.refresh();
                setTimeout(function () {
                    if (focused) focused.focus();
                }, 20);
            }, 100);
        }
    }


 // item formatter
    function itemFormatter(panel, r, c, cell) {
        var flex = panel.grid,
            row = flex.rows[r],
            col = flex.columns[c],
            sel = flex.selection,
            editCell = flex.activeEditor && sel.row == r && sel.col == c;

        // add filters to column headers
        if (panel.cellType == wijmo.grid.CellType.ColumnHeader && r == 1) {
            switch (col.binding) {

                 // author filter
                case 'country':
                    cell.innerHTML = '';
                    cell.appendChild(cmbCountry.hostElement);
                    cell.style.overflow = 'visible';
                    break;
            }
        }
    }

    // stop propagation of mouse and keyboard events in order to
    // prevent the grid from responding to these events.
    function stopPropagation(element) {
        element.style.fontWeight = 'normal';
        var events = ['mousedown', 'keypress', 'keydown'];
        for (var i = 0; i < events.length; i++) {
            element.addEventListener(events[i], function (e) {
                e.stopPropagation();
            });
        }
    }


    // set cell header
    function setHeader(p, r1, c1, r2, c2, hdr) {
        for (var r = r1; r <= r2; r++) {
            for (var c = c1; c <= c2; c++) {
                p.setCellData(r, c, hdr);
            }
        }
    }

    // create (and populate) ComboBox from <select> element
    var cbs = new wijmo.input.ComboBox('#theComboSelect');

   /// create ComboBox
    var cmb = new wijmo.input.ComboBox('#theCombo', {
        itemsSource: $scope.countries,
        placeholder: 'select a country',
        isEditable: false
    });


});
