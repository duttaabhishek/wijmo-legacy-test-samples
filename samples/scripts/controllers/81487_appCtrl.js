﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {

        chart: null,
        data: [],
        candle: []
    };

    //to test y annotation can show string value
    $scope.data = [
     { "name": "Orange", "food": "Fried Rice", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 1, 1), "y1": 10, "y2": 8, "y3": 10, "y4": 16, "hi": 500, "lo": 100, "open": 200, "close": 600 },
     { "name": "apple", "food": "Fried Noodle", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 2, 2), "y1": 20, "y2": 12, "y3": 16, "y4": 19, "hi": 400, "lo": 200, "open": 100, "close": 700 },
     { "name": "lime", "food": "Fried Egg", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 3, 3), "y1": 30, "y2": 10, "y3": 17, "y4": 15, "hi": 200, "lo": 300, "open": 300, "close": 400 },
     { "name": "lemon", "food": "Fried Vermicelli", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 4, 4), "y1": 40, "y2": 12, "y3": 15, "y4": 22, "hi": 300, "lo": 100, "open": 200, "close": 800 },
     { "name": "mango", "food": "Chicken Soup", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 5, 5), "y1": 50, "y2": 15, "y3": 23, "y4": 18, "hi": 500, "lo": 100, "open": 200, "close": 300 },
    ];

    $scope.candle = [
    { "name": "Orange", "x": 1, "date1": new Date(2014, 1, 1), "high": 17, "low": 5, "open": 10, "close": 15 },
     { "name": "apple", "x": 2, "date1": new Date(2014, 2, 2), "high": 21, "low": 10, "open": 12, "close": 17 },
     { "name": "lime", "x": 3, "date1": new Date(2014, 3, 3), "high": 23, "low": 15, "open": 19, "close": 21 },
     { "name": "lemon", "x": 4, "date1": new Date(2014, 4, 4), "high": 27, "low": 20, "open": 23, "close": 25 },
     { "name": "mango", "x": 5, "date1": new Date(2014, 5, 5), "high": 35, "low": 25, "open": 30, "close": 32 },
     ];
});