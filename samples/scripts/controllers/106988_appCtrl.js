﻿'use strict';

// define app, include Wijmo 5 directives
var app = angular.module('app', ['wj']);

// controller
app.controller('appCtrl', function ($scope) {

    var data = {
        capacity: {
            points: [
							{ x: 0, y: 102 },
							{ x: 1, y: 102 },
							{ x: 2, y: 102 },
							{ x: 3, y: 102 },
							{ x: 4, y: 102 },
							{ x: 5, y: 102 },
						]
        },
        demand: [
						{
						    name: "NSC 6",
						    points: [
								{ x: 0, y: 30 },
								{ x: 1, y: 30 },
								{ x: 2, y: 20 },
								{ x: 3, y: 40 },
								{ x: 4, y: 60 },
								{ x: 5, y: 50 },
							]
						}, {
						    name: "DDG 113",
						    points: [
								{ x: 0, y: 40 },
								{ x: 1, y: 40 },
								{ x: 2, y: 20 },
								{ x: 3, y: 20 },
								{ x: 4, y: 30 },
								{ x: 5, y: 30 },
							]
						}, {
						    name: "LPD 26",
						    points: [
								{ x: 0, y: 30 },
								{ x: 1, y: 10 },
								{ x: 2, y: 20 },
								{ x: 3, y: 40 },
								{ x: 4, y: 20 },
								{ x: 5, y: 10 },
							]
						}
					]
    };

    $scope.data = data;

});