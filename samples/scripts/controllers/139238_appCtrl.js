﻿'use strict';

angular.module('app').controller('appCtrl', function ($scope, dataSvc) {
    $scope.ctx = {
        data: dataSvc.getData(50),
		flexSheet: null,
		sheets: []
	};


    // initialize the flexSheet control when document ready.
    $scope.initialized = function (s) {
        s.deferUpdate(function () {
            for (var i = 0; i < s.sheets.length; i++) {
                s.sheets.selectedIndex = i;
                switch (s.sheets[i].name) {
                    case 'Country':
                        initDataMapForBindingSheet(s);
                        break;
                    case 'Report':
                        generateUseCaseTemplateSheet(s);
                        break;
                    case 'Formulas':
                        generateFormulasSheet(s);
                        break;
                }
            }
            s.selectedSheetIndex = 0;
        });
    };

    // initialize the dataMap for the bound sheet.
    function initDataMapForBindingSheet(flexSheet) {
        var column;

        if (flexSheet) {
            column = flexSheet.columns.getColumn('countryId');
            if (column && !column.dataMap) {
                column.dataMap = buildDataMap(dataSvc.getCountries());
            }
            column = flexSheet.columns.getColumn('productId');
            if (column && !column.dataMap) {
                column.dataMap = buildDataMap(dataSvc.getProducts());
            }
            column = flexSheet.columns.getColumn('colorId');
            if (column && !column.dataMap) {
                column.dataMap = buildDataMap(dataSvc.getColors());
            }
        }
    };

    // build a data map from a string array using the indices as keys
    function buildDataMap(items) {
        var map = [];
        for (var i = 0; i < items.length; i++) {
            map.push({ key: i, value: items[i] });
        }
        return new wijmo.grid.DataMap(map, 'key', 'value');
    };

    // Generate the use case template sheet.
    function generateUseCaseTemplateSheet(flexSheet) {
        setContentForUseCaseTemplate(flexSheet);

        applyStyleForUseCaseTemplate(flexSheet);
    }

    // Set content for the use case template sheet.
    function setContentForUseCaseTemplate(flexSheet) {
        flexSheet.setCellData(0, 9, 'For Office Use Only');
        flexSheet.setCellData(1, 1, 'Expense Report');
        flexSheet.setCellData(3, 1, 'PURPOSE:');
        flexSheet.setCellData(3, 2, 'On business');
        flexSheet.setCellData(3, 5, 'Attachment:');
        flexSheet.setCellData(3, 6, 'Yes');
        flexSheet.setCellData(3, 9, 'PAY PERIOD:');
        flexSheet.setCellData(3, 10, 'From');
        flexSheet.setCellData(3, 11, '2015-3-1');
        flexSheet.setCellData(4, 10, 'To');
        flexSheet.setCellData(4, 11, '2015-4-1');
        flexSheet.setCellData(5, 1, 'EMPLOYEE IMFORMATION:');
        flexSheet.setCellData(6, 1, 'Name');
        flexSheet.setCellData(6, 2, 'Robert King');
        flexSheet.setCellData(6, 5, 'Position');
        flexSheet.setCellData(6, 6, 'Sales Representative');
        flexSheet.setCellData(6, 9, 'SSN');
        flexSheet.setCellData(6, 10, 'A12345');
        flexSheet.setCellData(7, 1, 'Department');
        flexSheet.setCellData(7, 2, 'Sales');
        flexSheet.setCellData(7, 5, 'Manager');
        flexSheet.setCellData(7, 6, 'Andrew Fuller');
        flexSheet.setCellData(7, 9, 'Employee ID');
        flexSheet.setCellData(7, 10, 'E123456');
        flexSheet.setCellData(9, 1, 'Date');
        flexSheet.setCellData(9, 2, 'Account');
        flexSheet.setCellData(9, 3, 'Description');
        flexSheet.setCellData(9, 4, 'Hotel');
        flexSheet.setCellData(9, 5, 'Transport');
        flexSheet.setCellData(9, 6, 'Fuel');
        flexSheet.setCellData(9, 7, 'Meals');
        flexSheet.setCellData(9, 8, 'Phone');
        flexSheet.setCellData(9, 9, 'Entertainment');
        flexSheet.setCellData(9, 10, 'Misc');
        flexSheet.setCellData(9, 11, 'Total');
        flexSheet.setCellData(17, 1, 'Total');
        flexSheet.setCellData(18, 10, 'Subtotal');
        flexSheet.setCellData(19, 9, 'Cash Advances');
        flexSheet.setCellData(20, 10, 'Total');
        flexSheet.setCellData(20, 1, 'APPROVED:');
        flexSheet.setCellData(20, 5, 'NOTES:');

        setExpenseData(flexSheet);
    }

    // set expense detail data for the use case template sheet.
    function setExpenseData(flexSheet) {
        var rowIndex,
			    colIndex,
			    value,
			    rowAlpha,
			    cellRange;

        for (rowIndex = 10; rowIndex <= 17; rowIndex++) {
            for (colIndex = 1; colIndex <= 11; colIndex++) {
                if (rowIndex === 17) {
                    if (colIndex >= 4 && colIndex <= 11) {
                        rowAlpha = wijmo.grid.sheet.FlexSheet.convertNumberToAlpha(colIndex);
                        cellRange = rowAlpha + '11' + ':' + rowAlpha + '17';
                        flexSheet.setCellData(rowIndex, colIndex, '=sum(' + cellRange + ')');
                    }
                } else {
                    if (colIndex === 11) {
                        cellRange = 'E' + (rowIndex + 1) + ':' + 'K' + (rowIndex + 1);
                        flexSheet.setCellData(rowIndex, colIndex, '=sum(' + cellRange + ')');
                    } else if (colIndex >= 4 && colIndex < 11) {
                        value = 200 * Math.random();
                        flexSheet.setCellData(rowIndex, colIndex, value);
                    } else if (colIndex === 3) {
                        flexSheet.setCellData(rowIndex, colIndex, 'Visit VIP customers.');
                    } else if (colIndex === 2) {
                        flexSheet.setCellData(rowIndex, colIndex, '12345678');
                    }
                }
            }
        }

        flexSheet.setCellData(10, 1, '2015-3-1');
        flexSheet.setCellData(11, 1, '2015-3-3');
        flexSheet.setCellData(12, 1, '2015-3-7');
        flexSheet.setCellData(13, 1, '2015-3-11');
        flexSheet.setCellData(14, 1, '2015-3-18');
        flexSheet.setCellData(15, 1, '2015-3-21');
        flexSheet.setCellData(16, 1, '2015-3-27');
        flexSheet.setCellData(18, 11, '=L21-L20');
        flexSheet.setCellData(19, 11, 1000);
        flexSheet.setCellData(20, 11, '=L18');
    }

    // Apply styles for the use case template sheet.
    function applyStyleForUseCaseTemplate(flexSheet) {
        flexSheet.columns[0].width = 10;
        flexSheet.columns[1].width = 100;
        flexSheet.columns[3].width = 230;
        flexSheet.columns[5].width = 95;
        flexSheet.columns[6].width = 130;
        flexSheet.columns[9].width = 105;
        for (var i = 4; i <= 11; i++) {
            flexSheet.columns[i].format = 'c2';
        }
        flexSheet.rows[1].height = 45;
        flexSheet.applyCellsStyle({
            fontStyle: 'italic',
            backgroundColor: '#E1DFDF'
        }, [new wijmo.grid.CellRange(0, 9, 0, 11)]);
        flexSheet.mergeRange(new wijmo.grid.CellRange(0, 9, 0, 11));
        flexSheet.applyCellsStyle({
            fontSize: '24px',
            fontWeight: 'bold',
            color: '#696964'
        }, [new wijmo.grid.CellRange(1, 1, 1, 3)]);
        flexSheet.mergeRange(new wijmo.grid.CellRange(1, 1, 1, 3));
        flexSheet.applyCellsStyle({
            fontWeight: 'bold',
            color: '#808097'
        }, [new wijmo.grid.CellRange(3, 1, 3, 1),
			    new wijmo.grid.CellRange(3, 5, 3, 5),
			    new wijmo.grid.CellRange(3, 9, 3, 9),
			    new wijmo.grid.CellRange(5, 1, 5, 2)]);
        flexSheet.applyCellsStyle({
            textAlign: 'right'
        }, [new wijmo.grid.CellRange(3, 10, 4, 10),
			    new wijmo.grid.CellRange(6, 1, 7, 1),
			    new wijmo.grid.CellRange(6, 5, 7, 5),
			    new wijmo.grid.CellRange(6, 9, 7, 9)]);
        flexSheet.applyCellsStyle({
            backgroundColor: '#E1DFDF'
        }, [new wijmo.grid.CellRange(3, 11, 4, 11)]);
        flexSheet.mergeRange(new wijmo.grid.CellRange(5, 1, 5, 2));
        flexSheet.applyCellsStyle({
            fontWeight: 'bold',
            backgroundColor: '#FAD9CD'
        }, [new wijmo.grid.CellRange(9, 1, 9, 11),
			    new wijmo.grid.CellRange(17, 1, 17, 11)]);
        flexSheet.applyCellsStyle({
            backgroundColor: '#F4B19B'
        }, [new wijmo.grid.CellRange(10, 1, 16, 11)]);
        flexSheet.applyCellsStyle({
            fontWeight: 'bold',
            textAlign: 'right'
        }, [new wijmo.grid.CellRange(18, 9, 20, 10)]);
        flexSheet.mergeRange(new wijmo.grid.CellRange(19, 9, 19, 10));
        flexSheet.applyCellsStyle({
            fontWeight: 'bold',
            color: '#808097',
            textAlign: 'center'
        }, [new wijmo.grid.CellRange(20, 1, 20, 1),
			    new wijmo.grid.CellRange(20, 5, 20, 5)]);
    }

    // Generate the formulas sheet.
    function generateFormulasSheet(flexSheet) {
        setContentForFormulasSheet(flexSheet);
    }

    // Set data for the formulas sheet.
    function setContentForFormulasSheet(flexSheet) {      
        flexSheet.setCellData(0, 0, "6. Date function");
        flexSheet.setCellData(1, 1, "6.1. Now");
        flexSheet.setCellData(1, 2, "Returns the serial number of the current date and time.");
        flexSheet.setCellData(2, 1, "Sample:");
        flexSheet.setCellData(2, 2, "date(2016,4,1)");
        flexSheet.setCellData(2, 3, "Result:");
        flexSheet.setCellData(2, 4, "=date(2016,4,1)");
        flexSheet.setCellData(3, 1, "6.2. Year");
        flexSheet.setCellData(3, 2, "Converts a serial number to a year.");
        flexSheet.setCellData(4, 1, "Sample:");
        flexSheet.setCellData(4, 2, "Year(E3)");
        flexSheet.setCellData(4, 3, "Result:");
        flexSheet.setCellData(4, 4, "=Year(E3)");
        flexSheet.setCellData(5, 1, "6.3. Month");
        flexSheet.setCellData(5, 2, "Converts a serial number to a month.");
        flexSheet.setCellData(6, 1, "Sample:");
        flexSheet.setCellData(6, 2, "Month(E3)");
        flexSheet.setCellData(6, 3, "Result:");
        flexSheet.setCellData(6, 4, "=Month(E3)");
        flexSheet.setCellData(7, 1, "6.4. Day");
        flexSheet.setCellData(7, 2, "Converts a serial number to a day of the month.");
        flexSheet.setCellData(8, 1, "Sample:");
        flexSheet.setCellData(8, 2, "Day(E3)");
        flexSheet.setCellData(8, 3, "Result:");
        flexSheet.setCellData(8, 4, "=Day(E3)");       
    }

});