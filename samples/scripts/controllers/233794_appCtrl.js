﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {
        piechart: null,
        sunchart: null,
        itemsSource:[],
        itemsSource1: []
    }
    // populate itemsSource
    var names = ['January', 'February', 'March', 'April'],
	    days = ['Monday', 'Tuesday', 'Wednesday', 'Thusday', 'Friday', 'Saturday', 'Sunday'],
        data = [];
    for (var i = 0; i < names.length; i++) {
        data.push({
            name: names[i],
            value: Math.round(Math.random() * 10000),
            value1: Math.round(Math.random() * 7000),
            value2: Math.round(Math.random() * 5000),
            value3: Math.round(Math.random() * 8000)
        });
    }
    $scope.ctx.itemsSource = data;

    //sunburst
     $scope.sundata = [
        {
            name:'Janulary',
            items:[{
                name:'Jan',
                value:1.1
            },
            {
                name:'Feb',
                items: [{
                    name: 'Week1',
                    value:1.2
                },
                {
                    name: 'Week2',
                    value: 0.8
                },
                {
                    name: 'Week3',
                    value: 0.6
                },
                {
                    name: 'Week4',
                    value: 0.5
                }
                ]
            },
            {
                name:'Mar',
                value:0.3
            }]
        },
{
        name:'February',
        items:[{
            name:'Apr',
            value:1.1
        },
        {
            name:'May',
            value:0.8
        },
        {
            name:'June',
            value:0.3
        }]
},
{
    name: 'March',
    items: [{
        name: 'July',
        value: 0.7
    },
    {
        name: 'Aug',
        value: 0.6
    },
    {
        name: 'Sept',
        value: 0.1
    }]
},
    ];
                 
    $scope.childItemsPath = 'items';
    $scope.bindingName = 'name';

    //231835
    // populate itemsSource
    var names1 = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'January2017', 'February2017', 'March2017', 'April2017'],
	    days1 = ['Monday', 'Tuesday', 'Wednesday', 'Thusday', 'Friday', 'Saturday', 'Sunday'],
        data1 = [];
    for (var i = 0; i < names1.length; i++) {
        data1.push({
            name: names1[i],
            value: Math.round(Math.random() * 10000),
            value1: Math.round(Math.random() * 7000),
            value2: Math.round(Math.random() * 5000),
            value3: Math.round(Math.random() * 8000)
        });
    }
    $scope.ctx.itemsSource1 = data1;

    $scope.sundata1 = [
        {
            name: 'Janulary',
            items: [{
                name: 'Jan',
                value: 1.1
            },
            {
                name: 'Feb',
                items: [{
                    name: 'Week1',
                    value: 1.2
                },
                {
                    name: 'Week2',
                    value: 0.8
                },
                {
                    name: 'Week3',
                    value: 0.6
                },
                {
                    name: 'Week4',
                    value: 0.5
                }
                ]
            },
            {
                name: 'Mar',
                value: 0.3
            }]
        },
{
    name: 'February',
    items: [{
        name: 'Apr',
        value: 1.1
    },
        {
            name: 'May',
            value: 0.8
        },
        {
            name: 'June',
            value: 0.3
        }]
},
{
    name: 'March',
    items: [{
        name: 'July',
        value: 0.7
    },
    {
        name: 'Aug',
        value: 0.6
    },
    {
        name: 'Sept',
        value: 0.1
    }]
},
{
    name: 'April',
    items: [{
        name: 'Oct',
        value: 0.3
    },
    {
        name: 'Nov',
        value: 0.6
    },
    {
        name: 'Dec',
        value: 0.1
    }]
},
{
    name: 'May',
    items: [{
        name: 'Jan',
        value: 0.6
    },
    {
        name: 'Mar',
        value: 0.5
    },
    {
        name: 'May',
        value: 0.2
    }]
},
{
    name: 'June',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'July',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'August',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'September',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'October',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'November',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'December',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'January2017',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'February2017',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'March2017',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
{
    name: 'April2017',
    items: [{
        name: 'June',
        value: 0.4
    },
    {
        name: 'Aug',
        value: 0.2
    },
    {
        name: 'Sep',
        value: 0.3
    }]
},
    ];

    $scope.childItemsPath = 'items';
    $scope.bindingName = 'name';
});