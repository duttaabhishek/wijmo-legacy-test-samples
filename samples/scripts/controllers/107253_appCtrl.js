﻿// define app, include Wijmo 5 directives
var app = angular.module('app', ['wj']);

// controller
app.controller('appCtrl', function ($scope) {

    // create some random data
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
        data = [];
    for (var i = 0; i < 100; i++) {
        data.push({
            count: Math.round(Math.random() * 50),
            grouping: i
        });
    }

    // expose data as a CollectionView to get events
    $scope.data = new wijmo.CollectionView(data);

    $scope.threshold = 10;

    $scope.above = new wijmo.CollectionView(data);
    $scope.above.filter = function (item) {
        return item.grouping > $scope.threshold;
    };

    $scope.below = new wijmo.CollectionView(data);
    $scope.below.filter = function (item) {
        return item.grouping <= $scope.threshold;
    };

    $scope.forceRefresh = function () {
        $scope.above.refresh();
        $scope.below.refresh();
    };
});