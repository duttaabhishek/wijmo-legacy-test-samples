﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {
        chart: null,
        pal: 0,
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        data: [],
        mydata: [],
        boxData: [],
        catData: [],
        chartAni: { animationMode: 'Point' },
        boxPlot: {
            groupWidth: 0.9,
            gapWidth: 0.1,
            showMeanLine: false,
            showMeanMarker: false,
            quartileCalculation: 'include',
            showInnerPoints: false,
            showOutliers: false
        }
    }
    //ObservableArray Binding
    var countries = 'United State,Germany,United Kingdom,Japan,Italy,Greece'.split(',');
    for (var i = 0; i < countries.length; i++) {
        $scope.ctx.data.push({
            country: countries[i],
            downloads: Math.round(Math.random() * 20000),
            sales: Math.random() * 10000,
            expenses: Math.random() * 5000,
            uploads: Math.random() * 2000,
            profits: Math.random() * 1000,
            loss: Math.random() * 500,
            extra: Math.random() * 300,
            secondextra: Math.random() * 200
        });
        $scope.ctx.boxData.push({
            country: countries[i],
            downloads: [getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData()],
            sales: [getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData()],
            expenses: [getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData(), getData()]
        });
    }
    function getData() {
        return Math.round(Math.random() * 100);
    }
    $scope.catData = [
        { "CATEGORY": "APPETIZERS", "YEAR2014": [25249, 24106, 44023, 55715], "YEAR2015": [48932, 75845, 69701, 16540], "YEAR2016": [34878, 27602, 23359, 28600], "x": 100 },
        { "CATEGORY": "MAINS", "YEAR2014": [93577, 35750, 49035, 43099], "YEAR2015": [43514, 19478, 22129, 52334], "YEAR2016": [78463, 65489, 55033, 47806], "x": 200 },
        { "CATEGORY": "DESSERTS", "YEAR2014": [41118, 56178, 59780, 48489], "YEAR2015": [52435, 64000, 61703, 50412], "YEAR2016": [56737, 26920, 52624, 68981], "x": 300 },
        { "CATEGORY": "DRINKS", "YEAR2014": [56682, 50784, 63779, 57507], "YEAR2015": [12356, 69233, 36752, 56886], "YEAR2016": [37849, 54151, 63606, 34822], "x": 400 },
        ]
    $scope.schoolData = [
        {"Course":"English","SchoolA":[63,63,46,58,63,63,60,59],"SchoolB":[53,50,53,56,54,52,56,54],"SchoolC":[45,65,66,67,64,67,67,65]},
         {"Course":"Physics","SchoolA":[61,60],"SchoolB":[55,56],"SchoolC":[65,64]},
        { "Course": "Maths","SchoolA":[62,60,62,61,63],"SchoolB":[51,51,53,56,58],"SchoolC":[64,67,66,45,64]}
    ]
    $scope.neckWidth = 0.2;
    $scope.neckHeight = 0.2;
    $scope.groupWidth = 0.9;
    $scope.gapWidth = 0.1;

    $scope.optionChanged = function (sender, args) {
        $scope.ctx.chart.invalidate();
    };
   
    //series style
    $scope.seriesStyle = function () {
        var fchart = $scope.ctx.chart;
        fchart.series[0].style = { strokeWidth: 3, fill: 'pink' };
    };

});