﻿'use strict';

var app = angular.module("app", ['wj']);

app.controller('chartAnalyticCtrl', function ($scope) {
    $scope.ctx = {
        machart: null,
        tlchart: null,
        movavg: {
            visibility: "Visible"
        },
        trend: {
            visibility: "Visible",
            order: 2,
            fitType: "Linear"
        },
        style: {
            stroke: "green"
        },
        altStyle: {
            stroke: 'rgb(136, 115, 230)'
        },
        //waterfall Chart
        wfchart: null,
        waterfall: null,
        data: [],
        intermediateTotalPositions: [3, 6, 9, 12],
        intermediateTotalLabels: ['Q1', 'Q2', 'Q3', 'Q4'],
        styles: {
            connectorLines: {
                stroke: '#333',
                'stroke-dasharray': '5 5'
            },
            start: {
                fill: '#7dc7ed'
            },
            falling: {
                fill: '#dd2714',
                stroke: '#a52714'
            },
            rising: {
                fill: '#0f9d58',
                stroke: '#0f9d58'
            },
            intermediateTotal: {
                fill: '#7dc7ed'
            },
            total: {
                fill: '#7dc7ed'
            }
        },
        //errorBar
         errChart: null,
         itemsSource: [],
         chartType: 'Column',
         errorbar: {
            direction: "Both",
            endStyle: "NoCap",
            errorAmount: "StandardDeviation"
        },
        //Sunburst chart
        chart:null,
        innerRadius: 0,
        sunData:[],
    };
    // generate some random data
    $scope.fruits = [
     { "name": "Orange", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(3, 1), "hi": 400, "lo": 100, "open": 200, "close": 300, "hi1": 700, "lo1": 400, "open1": 500, "close1": 600, "y1": 7000 },
     { "name": "apple", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(4, 1), "hi": 400, "lo": 200, "open": 200, "close": 400, "hi1": 700, "lo1": 400, "open1": 500, "close1": 600, "y1": 8000 },
     { "name": "lime", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(5, 1), "hi": 400, "lo": 200, "open": 300, "close": 300, "hi1": 700, "lo1": 400, "open1": 500, "close1": 600, "y1": 9000 },
     { "name": "mango", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(6, 1), "hi": 400, "lo": 100, "open": 200, "close": 400, "hi1": 700, "lo1": 400, "open1": 500, "close1": 600, "y1": 6000 },
	 { "name": "lime", "x": 5, "date1": new Date(2014, 3, 3), "date2": new Date(5, 1), "hi": 400, "lo": 200, "open": 300, "close": 400, "hi1": 700, "lo1": 400, "open1": 500, "close1": 600, "y1": 5000 },
     { "name": "mango", "x": 6, "date1": new Date(2014, 4, 4), "date2": new Date(6, 1), "hi": 200, "lo": 100, "open": 200, "close": 200, "hi1": 700, "lo1": 400, "open1": 500, "close1": 600, "y1": 3000 }
    ];

    $scope.menu = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 7000, "y2": 6000, "y3": 4000, "y4": 3000 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 8000, "y2": 4000, "y3": 5000, "y4": -2000 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 9000, "y2": 7000, "y3": 7000, "y4": 5000 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 6000, "y2": 8000, "y3": 8000, "y4": 6000 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 5000, "y2": 1000, "y3": 3000, "y4": -1000 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 3000, "y2": 2000, "y3": 9000, "y4": 4000 },
	  { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 0, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 4000, "y2": 3000, "y3": 6000, "y4": 7000 },
    ];

    $scope.data = [
   { "date": "01/05/15", "open": 77.98, "high": 79.25, "low": 76.86, "close": 77.19, "volume": 26452191 },
    { "date": "01/06/15", "open": 77.23, "high": 77.59, "low": 75.36, "close": 76.15, "volume": 27399288 },
    { "date": "01/07/15", "open": 76.76, "high": 77.36, "low": 75.82, "close": 76.15, "volume": 22045333 },
    { "date": "01/08/15", "open": 76.74, "high": 78.23, "low": 76.08, "close": 78.18, "volume": 23960953 },
    { "date": "01/09/15", "open": 78.2, "high": 78.62, "low": 77.2, "close": 77.74, "volume": 21157007 },
    { "date": "01/12/15", "open": 77.84, "high": 78, "low": 76.21, "close": 76.72, "volume": 19190194 },
    { "date": "01/13/15", "open": 77.23, "high": 78.08, "low": 75.85, "close": 76.45, "volume": 25179561 },
    { "date": "01/14/15", "open": 76.42, "high": 77.2, "low": 76.03, "close": 76.28, "volume": 25918564 },
    { "date": "01/15/15", "open": 76.4, "high": 76.57, "low": 73.54, "close": 74.05, "volume": 34133974 },
    { "date": "01/16/15", "open": 74.04, "high": 75.32, "low": 73.84, "close": 75.18, "volume": 21791529 },
    { "date": "01/20/15", "open": 75.72, "high": 76.31, "low": 74.82, "close": 76.24, "volume": 22821614 },
    { "date": "01/21/15", "open": 76.16, "high": 77.3, "low": 75.85, "close": 76.74, "volume": 25096737 },
    { "date": "01/22/15", "open": 77.17, "high": 77.75, "low": 76.68, "close": 77.65, "volume": 19519458 },
    { "date": "01/23/15", "open": 77.65, "high": 78.19, "low": 77.04, "close": 77.83, "volume": 16746503 },
    { "date": "01/26/15", "open": 77.98, "high": 78.47, "low": 77.29, "close": 77.5, "volume": 19260820 }
];
   //MovingAverage
    $scope.period = 2;
    $scope.type = 'Simple';
    $scope.$watch('type', function () {
        updateMovingAverage();
    });

    function updateMovingAverage() {
        if (!$scope.type || $scope.period == null || !$scope.machart) {
            return;
        }
        var sender = $scope.machart,
            movingAverage = sender.series[1];

        movingAverage.type = wijmo.chart.analytics.MovingAverageType[$scope.type];
    }

    //style
    $scope.optionChanged = function (sender, args) {
        $scope.machart.invalidate();
    };

//    //Waterfall Chart 

    $scope.optionChangedwf = function (sender, args) {
        $scope.ctx.wfchart.invalidate();
    };
    $scope.intermediateTotalLabels = function () {
        var wfchart = $scope.ctx.waterfall;
        wfchart.intermediateTotalLabels = ['Q1', 'Q2', 'Q3', 'Q4'];
    };
    $scope.intermediateTotalPositions = function () {
        var wfchart = $scope.ctx.waterfall;
        wfchart.intermediateTotalPositions = [3, 6, 9, 12];
    };
    $scope.legendCount = function () {
        var wfchart = $scope.ctx.wfchart;
        var count = wfchart.series[0].legendItemLength();
        $("#EventList").append('<li>' + count + '</li>')
    };
    $scope.totalLabel = function () {
        var wfchart = $scope.ctx.wfchart;
        wfchart.series[0].totalLabel = "My Total Label";
        wfchart.refresh(true);
    };
    $scope.startLabel = function () {
        var wfchart = $scope.ctx.wfchart;
        wfchart.series[0].startLabel = "My Start Label";
        wfchart.refresh(true);
    };
   
    $scope.palettes= ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'];

    //sunburst
    $scope.inputInnerRadius = null;
    $scope.sunchart = null;
    $scope.childItemsPath = 'items';
    $scope.bindingName = 'name';
    $scope.sunData = [
        {
            name: '1st',
            items: [{
                name: 'January',
                value: 1.1
            },
            {
                name: 'February',
                items: [{
                    name: 'Week1',
                    value: 1.2
                },
                {
                    name: 'Week2',
                    value: 0.8
                },
                {
                    name: 'Week3',
                    value: 0.6
                },
                {
                    name: 'Week3',
                    value: 0.5
                }
                ]
            },
            {
                name: 'March',
                value: 0.3
            }]
        },
{
    name: '2nd',
    items: [{
        name: 'April',
        value: 1.1
    },
        {
            name: 'May',
            value: 0.8
        },
        {
            name: 'June',
            value: 0.3
        }]
},
{
    name: '3rd',
    items: [{
        name: 'July',
        value: 0.7
    },
    {
        name: 'August',
        value: 0.6
    },
    {
        name: 'September',
        value: 0.1
    }]
},
{
    name: '4th',
    items: [{
        name: 'October',
        value: 0.5
    },
    {
        name: 'November',
        value: 0.4
    },
    {
        name: 'December',
        value: 0.3
    }]
}
];
//palette
 $scope.menuPaletteItemClicked = function (sender, args) {
        var menu = sender;
        $scope.sunchart.palette = wijmo.chart.Palettes[$scope.palettes[menu.selectedIndex]];
    }
  
    // Wijmo Menu's itemClicked event handler
    $scope.itemClicked = function (menu) {
        if ($scope.ctx.sunchart == null) {
            return;
        }
        $scope.ctx.sunchart.saveImageToFile('chart.' + menu.selectedItem.value || 'png');
    };

    $scope.$watch('ctx.innerRadius', function () {
        var input = $scope.inputInnerRadius,
        val = $scope.ctx.innerRadius;

        if (!input || val < input.min || val > input.max) {
            return;
        }
        $scope.innerRadius = val;
    });

     //ErrorBar

    var countries = 'US,Germany,UK,Japan,Italy,Greece,China,France,Russia'.split(','),
       appData = [];
    for (var i = 0; i < countries.length; i++) {
        appData.push({
            country: countries[i],
            downloads: getData(),
            sales: getData()
        });
    }
    $scope.ctx.itemsSource = appData;

    function getData() {
        var val = Math.round(Math.random() * 100);
        return val > 10 ? val : val + 10;
    }


    //selectionChanged Event
    $scope.chartSelectionChanged = function () {
            $("#sbEventList").append("selection changed event fire.");

    }
    //rendering Event
    $scope.sbRendering = function () {
            $("#sbRenderingEventList").append("Rendering event fire.");
        }
    //rendering Event
    $scope.sbRendered = function () {
            $("#sbRenderedEventList").append("Rendered event fire.");
        }
   //losttFocus Event
        $scope.chartLostFocus = function () {
            if ($("#chklostFocus").is(":checked")) {
                var value = " LostFocus event fired";
                $("#lostFocusEventList").append('<li>' + value + '</li>')
            }
            else {
                $("#lostFocusEventList").append("");
            }
        }
        $scope.plotMargin = function () {
            var fchart = $scope.sunchart;
            fchart.plotMargin = 100;
        };
                //gotFocus Event
        $scope.sbchartGotFocus = function () {
                $("#EventList").append("GotFocus event Fire!");
            }
            $scope.tooltipContent = function () {
                var fchart = $scope.sunchart;
                fchart.tooltip.content = '<b>Test</b><br/>';
            };

   
    //rendering
    $scope.errBarRendering = function () {
        if ($("#chkRendering").is(":checked")) {
            var value = "rendering event fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else {
            $("#EventList").append("");
        }
    }

    $scope.errBarRendered = function () {
        if ($("#chkRendered").is(":checked")) {
            var value = "rendered event fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else {
            $("#EventList").append("");
        }
    }
    $scope.seriesName = function () {
        var fchart = $scope.ctx.errChart;
        fchart.series[0].name = "my series";
        fchart.refresh(true);
    };
    //errorBarstyle
    $scope.errorBarStyle = function () {
        var fchart = $scope.ctx.errChart;
        fchart.series[0].errorBarStyle = { strokeWidth: 2, stroke: 'red' };
    };


});

