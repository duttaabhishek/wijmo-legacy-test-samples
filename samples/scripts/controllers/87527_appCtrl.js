﻿
'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    $scope.ctx = {

        chart: null,
        //chartlegend:null,
        data: [],
    }

    //bar chart binding
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(',');
    for (var i = 0; i < countries.length; i++) {
        $scope.ctx.data.push({
            country: countries[i],
            downloads: Math.round(Math.random() * 20000),
            sales: Math.random() * 10000,
            expenses: Math.random() * 5000
        });
    }
});