﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {

    // data context
    $scope.ctx = {
        chart: null,
        pal: 0,
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        data: []
    };
    $scope.data = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": -2000, "y2": 1000, "y3": 6000, "y4": -6000 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 4000, "y2": 2000, "y3": 5000, "y4": -2000 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": -6000, "y2": 3000, "y3": 7000, "y4": -5000 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 8000, "y2": 4000, "y3": 8000, "y4": -8000 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": -10000, "y2": 5000, "y3": 4000, "y4": -4000 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 12000, "y2": 6000, "y3": 9000, "y4": -10000 }

    ];

    $scope.menu = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 900, "y2": 600, "y3": 400, "y4": 300 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 800, "y2": 400, "y3": 500, "y4": 200 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 700, "y2": 700, "y3": 700, "y4": 500 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 600, "y2": 800, "y3": 800, "y4": 600 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 500, "y2": 100, "y3": 300, "y4": 100 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 400, "y2": 200, "y3": 900, "y4": 400 },
	  { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 0, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 300, "y2": 300, "y3": 600, "y4": 700 },
    ];

});
