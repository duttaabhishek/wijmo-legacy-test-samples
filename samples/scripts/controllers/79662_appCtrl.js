(function() {
	'use strict';

	angular
		.module('app', ['wj'])
		.controller('79662_appCtrl', function ($scope) {
			
			$scope.minDate = "2013-06-06";
			$scope.maxDate = "2015-06-06";

			$scope.value ="2014-08-07";
		});
})();