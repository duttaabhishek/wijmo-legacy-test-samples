(function() {
	'use strict';

	angular
		.module('app', ['wj'])
		.controller('127301_appCtrl', function($scope) {
			
			
			$scope.value =new Date();
			$scope.culture = "en";
			$scope.allowMerging = 0;
		
// when the culture changes, load the new culture, apply, and invalidate
    $scope.$watch('culture', function () {
        $.ajax({



            url:  '/scripts/vendor/wijmo.culture.' + $scope.culture + '.min.js',
            dataType: 'script',
            success: function (data) {
                eval(data);

                // culture applied, now load translations
                $.ajax({
                    url: 'translations/strings.' + $scope.culture + '.js',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        $scope.i18n = data;

                        // show changes
                        $scope.$apply();
                        invalidateWijmoControls(document.body);
                    }
                });
            },
        });
    });

    // invalidate all Wijmo controls on the page
    function invalidateWijmoControls(e) {
        var ctl = wijmo.Control.getControl(e);
        if (ctl) {
            ctl.invalidate();
        }
        if (e.children) {
            for (var i = 0; i < e.children.length; i++) {
                invalidateWijmoControls(e.children[i]);
            }
        }
    }

});    

})();