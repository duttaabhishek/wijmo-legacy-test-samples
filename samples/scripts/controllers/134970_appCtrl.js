﻿var app = angular.module("app", ['wj']);

function appCtrl($scope) {
   

    $scope.chartProps = {
        chartType: wijmo.chart.finance.FinancialChartType.LineBreak,
        legendPosition: wijmo.chart.Position.Right,
        bindingY: 'close',
        header: 'Facebook, Inc. (FB)',
        movingAveragePeriod: 2,
        movingAverageType: wijmo.chart.analytics.MovingAverageType.Simple,
        rangeSelector: null,
        data:[]
    };
      $scope.tpChart = null;
    // chart types
    var bindingYs = {
        0: 'close',
        2: 'close',
        5: 'high,low,open,close',
        6: 'high,low,open,close',
        7: 'high,low,open,close',
        8: 'high,low,open,close',
        9: 'high,low,open,close',
        10: 'high,low,open,close',
        11: 'close,volume',
        12: 'high,low,open,close,volume',
        13: 'high,low,open,close,volume',
        14: 'high,low,open,close,volume'
    };

    $scope.$watch('chartProps.chartType', function () {
        var chartProps = $scope.chartProps, tpChart = $scope.tpChart;
        if (!tpChart) {
            return;
        }
        chartProps.bindingY = bindingYs[chartProps.chartType];

        switch (chartProps.chartType) {
            case wijmo.chart.finance.FinancialChartType.LineBreak:
                tpChart.options = {
                    lineBreak: {
                        newLineBreaks: 3
                    }
                };
                break;
            case wijmo.chart.finance.FinancialChartType.Renko:
                tpChart.options = {
                    renko: {
                        boxSize: 1,
                        rangeMode: 'Fixed',
                        fields: 'Close'
                    }
                };
                break;
            case wijmo.chart.finance.FinancialChartType.Kagi:
                tpChart.options = {
                    kagi: {
                        reversalAmount: 1,
                        rangeMode: 'Fixed',
                        fields: 'Close'
                    }
                };
                break;
            default:
                break;
        }
    });

    $scope.data = [
   { "date": "01/05/15", "open": 77.98, "high": 79.25, "low": 76.86, "close": 77.19, "volume": 26452191 },
    { "date": "01/06/15", "open": 77.23, "high": 77.59, "low": 75.36, "close": 76.15, "volume": 27399288 },
    { "date": "01/07/15", "open": 76.76, "high": 77.36, "low": 75.82, "close": 76.15, "volume": 22045333 },
    { "date": "01/08/15", "open": 76.74, "high": 78.23, "low": 76.08, "close": 78.18, "volume": 23960953 },
    { "date": "01/09/15", "open": 78.2, "high": 78.62, "low": 77.2, "close": 77.74, "volume": 21157007 },
    { "date": "01/12/15", "open": 77.84, "high": 78, "low": 76.21, "close": 76.72, "volume": 19190194 },
    { "date": "01/13/15", "open": 77.23, "high": 78.08, "low": 75.85, "close": 76.45, "volume": 25179561 },
    { "date": "01/14/15", "open": 76.42, "high": 77.2, "low": 76.03, "close": 76.28, "volume": 25918564 },
    { "date": "01/15/15", "open": 76.4, "high": 76.57, "low": 73.54, "close": 74.05, "volume": 34133974 },
    { "date": "01/16/15", "open": 74.04, "high": 75.32, "low": 73.84, "close": 75.18, "volume": 21791529 },
    { "date": "01/20/15", "open": 75.72, "high": 76.31, "low": 74.82, "close": 76.24, "volume": 22821614 },
    { "date": "01/21/15", "open": 76.16, "high": 77.3, "low": 75.85, "close": 76.74, "volume": 25096737 },
    { "date": "01/22/15", "open": 77.17, "high": 77.75, "low": 76.68, "close": 77.65, "volume": 19519458 },
    { "date": "01/23/15", "open": 77.65, "high": 78.19, "low": 77.04, "close": 77.83, "volume": 16746503 },
    { "date": "01/26/15", "open": 77.98, "high": 78.47, "low": 77.29, "close": 77.5, "volume": 19260820 },
    { "date": "01/27/15", "open": 76.71, "high": 76.88, "low": 75.63, "close": 75.78, "volume": 20109977 },
    { "date": "01/28/15", "open": 76.9, "high": 77.64, "low": 76, "close": 76.24, "volume": 53306422 },
    { "date": "01/29/15", "open": 76.85, "high": 78.02, "low": 74.21, "close": 78, "volume": 61293468 },
    { "date": "01/30/15", "open": 78, "high": 78.16, "low": 75.75, "close": 75.91, "volume": 42649491 },
    { "date": "02/02/15", "open": 76.11, "high": 76.14, "low": 73.75, "close": 74.99, "volume": 41955258 },
    { "date": "02/03/15", "open": 75.19, "high": 75.58, "low": 73.86, "close": 75.4, "volume": 26957714 },

        ];
     
   
}