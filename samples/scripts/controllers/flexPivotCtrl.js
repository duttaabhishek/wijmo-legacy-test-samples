﻿'use strict';

// add controller to app
angular.module('app').controller('flexPivotCtrl', function appCtrl($scope) {

    // define sample data sets
    $scope.dataSets = [
        { name: 'Simple (1,000 items)', value: getSimpleDataSet(1000) },
        { name: 'Simple (10,000 items)', value: getSimpleDataSet(10000) },
        { name: 'Complex (100 items)', value: getDataSet(100) },
        { name: 'Complex (50,000 items)', value: getDataSet(50000) },
        { name: 'Complex (100,000 items)', value: getDataSet(100000) },
        { name: 'Simple (100 items)', value: getSimpleDataSet3(100) }
        //{ name: 'Northwind Orders (read-only)', value: getNorthwindOrders() },
        //{ name: 'Northwind Sales (read-only)', value: getNorthwindSales() }
        //{ name: '(none)', value: null }
    ];

    // define ShowTotals options/values
    $scope.showTotals = [
        { name: 'None', value: wijmo.olap.ShowTotals.None },
        { name: 'Grand totals', value: wijmo.olap.ShowTotals.GrandTotals },
        { name: 'Subtotals', value: wijmo.olap.ShowTotals.Subtotals },
    ];

    // chart types
    $scope.chartTypes = [
        { name: 'Column', value: wijmo.olap.PivotChartType.Column },
        { name: 'Bar', value: wijmo.olap.PivotChartType.Bar },
        { name: 'Scatter', value: wijmo.olap.PivotChartType.Scatter },
        { name: 'Line', value: wijmo.olap.PivotChartType.Line },
        { name: 'Area', value: wijmo.olap.PivotChartType.Area },
        { name: 'Pie', value: wijmo.olap.PivotChartType.Pie },
    ];
    $scope.legendVisibility = [
        { name: 'Always', value: wijmo.olap.LegendVisibility.Always },
        { name: 'Never', value: wijmo.olap.LegendVisibility.Never },
        { name: 'Automatic', value: wijmo.olap.LegendVisibility.Auto }
    ];
    $scope.cultures = [
        { name: 'English', value: 'en' },
        { name: 'Japanese', value: 'ja' },
        { name: 'Chinese', value: 'zh' },
        { name: 'Portuguese', value: 'pt' },
        { name: 'Russian', value: 'ru' },
    ];

    $scope.fieldsMaxItems = [
        { name: '1', value: 1 },
        { name: '2', value: 2 },
        { name: '3', value: 3 },
        { name: '4', value: 4 },
        { name: '5', value: 5 },
        { name: '6', value: 6 },
        { name: '7', value: 7 },
        { name: '8', value: 8 },
        { name: '9', value: 9 },
        { name: '10', value: 10 }
    ];

    // pre-defined views
    $scope.viewDefs = [
        {
            name: "Sales by Product",
            def: "{\"showZeros\":false,\"showColumnTotals\":2,\"showRowTotals\":2,\"defaultFilterType\":3,\"fields\":[{\"binding\":\"id\",\"header\":\"Id\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"product\",\"header\":\"Product\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"country\",\"header\":\"Country\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"date\",\"header\":\"Date\",\"dataType\":4,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"format\":\"d\",\"isContentHtml\":false},{\"binding\":\"sales\",\"header\":\"Sales\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"downloads\",\"header\":\"Downloads\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"active\",\"header\":\"Active\",\"dataType\":3,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"discount\",\"header\":\"Discount\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false}],\"rowFields\":{\"items\":[\"Product\"]},\"columnFields\":{\"items\":[]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\"]}}"
        },
        {
            name: "Sales by Country",
            def: "{\"showZeros\":false,\"showColumnTotals\":2,\"showRowTotals\":2,\"defaultFilterType\":3,\"fields\":[{\"binding\":\"id\",\"header\":\"Id\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"product\",\"header\":\"Product\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"country\",\"header\":\"Country\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"date\",\"header\":\"Date\",\"dataType\":4,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"format\":\"d\",\"isContentHtml\":false},{\"binding\":\"sales\",\"header\":\"Sales\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"downloads\",\"header\":\"Downloads\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"active\",\"header\":\"Active\",\"dataType\":3,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"discount\",\"header\":\"Discount\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false}],\"rowFields\":{\"items\":[\"Country\"]},\"columnFields\":{\"items\":[]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\"]}}"
        },
        {
            name: "Sales and Downloads by Country",
            def: "{\"showZeros\":false,\"showColumnTotals\":2,\"showRowTotals\":2,\"defaultFilterType\":3,\"fields\":[{\"binding\":\"id\",\"header\":\"Id\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"product\",\"header\":\"Product\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"country\",\"header\":\"Country\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"date\",\"header\":\"Date\",\"dataType\":4,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"format\":\"d\",\"isContentHtml\":false},{\"binding\":\"sales\",\"header\":\"Sales\",\"dataType\":2,\"aggregate\":3,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"downloads\",\"header\":\"Downloads\",\"dataType\":2,\"aggregate\":3,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"active\",\"header\":\"Active\",\"dataType\":3,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"discount\",\"header\":\"Discount\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false}],\"rowFields\":{\"items\":[\"Country\"]},\"columnFields\":{\"items\":[]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\",\"Downloads\"]}}"
        },
        {
            name: "Sales Trend by Product",
            def: "{\"showZeros\":false,\"showColumnTotals\":0,\"showRowTotals\":0,\"defaultFilterType\":3,\"fields\":[{\"binding\":\"id\",\"header\":\"Id\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"product\",\"header\":\"Product\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"country\",\"header\":\"Country\",\"dataType\":1,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"date\",\"header\":\"Date\",\"dataType\":4,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"format\":\"yyyy \\\"Q\\\"q\",\"isContentHtml\":false},{\"binding\":\"sales\",\"header\":\"Sales\",\"dataType\":2,\"aggregate\":3,\"showAs\":2,\"descending\":false,\"format\":\"p2\",\"isContentHtml\":false},{\"binding\":\"downloads\",\"header\":\"Downloads\",\"dataType\":2,\"aggregate\":3,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false},{\"binding\":\"active\",\"header\":\"Active\",\"dataType\":3,\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false},{\"binding\":\"discount\",\"header\":\"Discount\",\"dataType\":2,\"aggregate\":1,\"showAs\":0,\"descending\":false,\"format\":\"n0\",\"isContentHtml\":false}],\"rowFields\":{\"items\":[\"Date\"]},\"columnFields\":{\"items\":[\"Product\"]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\"]}}"
        }
    ];

    $scope.rawData100 = $scope.dataSets[5].value;
    $scope.complexData= $scope.dataSets[2].value;

    // initialize the view definition
    $scope.init = function (sender, e) {
        var ng = sender.engine;
        ng.rowFields.push('Product', 'Country');
        ng.valueFields.push('Sales', 'Downloads');
        ng.showRowTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.showColumnTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.viewDefinitionChanged.addHandler(function (s, e) {
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });
    }

    // initialize the view definition containing Column Fields
    $scope.init3 = function (sender, e) {
        var ng = sender.engine;
        ng.rowFields.push('Active');
        ng.columnFields.push('Product', 'Country');
        ng.valueFields.push('Sales', 'Downloads');
        ng.showRowTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.showColumnTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.viewDefinitionChanged.addHandler(function (s, e) {
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });
    }

    $scope.init2 = function (sender, e) {
        var ng = sender.engine;
        ng.itemsSource= getSimpleDataSet2(100),
        ng.rowFields.push('Product', 'Country');
        ng.valueFields.push('Sales');
        ng.showRowTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.showColumnTotals = wijmo.olap.ShowTotals.Subtotals;
    }

    // Ray
    $scope.init4 = function (sender, e) {
        var ng = sender.engine;
        ng.rowFields.push('Product', 'Country');
        ng.valueFields.push('Sales', 'Downloads', 'Active', 'Date');
        ng.showRowTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.showColumnTotals = wijmo.olap.ShowTotals.Subtotals;
        ng.viewDefinitionChanged.addHandler(function (s, e) {
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });
    }

    $scope.ngFmt = new wijmo.olap.PivotEngine({
        autoGenerateFields: false,
        itemsSource: getSimpleDataSet(100),
        showColumnTotals: wijmo.olap.ShowTotals.GrandTotals,
        showRowTotals: wijmo.olap.ShowTotals.None,
        fields: [
            { binding: 'date', header: 'Date', format: 'MMM/d/yyyy' },
            { binding: 'sales', header: 'Sales', format: 'c' }
        ]
    });
    $scope.ngFmt.rowFields.push('Date');
    $scope.ngFmt.valueFields.push('Sales');

    // save/restore view definitions
    $scope.saveView = function () {
        var ng = $scope.thePanel.engine;
        if (ng.isViewDefined) {
            localStorage.viewDefinition = ng.viewDefinition;
        }
    }
    $scope.loadView = function (def) {
        var ng = $scope.thePanel.engine;
        if (def) {
            // load pre-defined view (against specific dataset)
            $scope.rawData = $scope.dataSets[0].value;
            ng.itemsSource = $scope.rawData;
            ng.viewDefinition = def;
        } else {
            // load view from localStorage (whatever the user saved)
            ng.viewDefinition = localStorage.viewDefinition;
        }
    }

    $scope.collapseToLevel0 = function () {
         $scope.pivotGrid.collapseColumnsToLevel(0);
         $scope.pivotGrid.collapseRowsToLevel(0);
    };
    $scope.collapseToLevel1 = function () {
         $scope.pivotGrid.collapseColumnsToLevel(1);
         $scope.pivotGrid.collapseRowsToLevel(1);
    };
    $scope.collapseToLevel2 = function () {
         $scope.pivotGrid.collapseColumnsToLevel(2);
         $scope.pivotGrid.collapseRowsToLevel(2);
    };

    // export pivot table and raw data to Excel
    $scope.export = function () {
        var ng = $scope.thePanel.engine;

        // create book with current view
        var book = wijmo.grid.xlsx.FlexGridXlsxConverter.save($scope.pivotGrid, {
            includeColumnHeaders: true,
            includeRowHeaders: true
        });
        book.sheets[0].name = 'Main View';
        addTitleCell(book.sheets[0], getViewTitle(ng));

        // add sheet with transposed view
        transposeView(ng);
        var transposed = wijmo.grid.xlsx.FlexGridXlsxConverter.save($scope.pivotGrid, {
            includeColumnHeaders: true,
            includeRowHeaders: true
        });
        transposed.sheets[0].name = 'Transposed View';
        addTitleCell(transposed.sheets[0], getViewTitle(ng));
        book.sheets.push(transposed.sheets[0]);
        transposeView(ng);

        // add sheet with raw data
        if ($scope.rawGrid.rows.length < 20000) {
            var raw = wijmo.grid.xlsx.FlexGridXlsxConverter.save($scope.rawGrid, {
                includeColumnHeaders: true,
                includeRowHeaders: false
            });
            raw.sheets[0].name = 'Raw Data';
            book.sheets.push(raw.sheets[0]);
        }

        // save the book
        book.save('wijmo.olap.xlsx');
    }

    /**************************************************
    utilities
    */

    // save/load/transpose/export views
    function transposeView(ng) {
        ng.deferUpdate(function () {

            // save row/col fields
            var rows = [],
                cols = [];
            for (var r = 0; r < ng.rowFields.length; r++) {
                rows.push(ng.rowFields[r].header);
            }
            for (var c = 0; c < ng.columnFields.length; c++) {
                cols.push(ng.columnFields[c].header);
            }

            // clear row/col fields
            ng.rowFields.clear();
            ng.columnFields.clear();

            // restore row/col fields in transposed order
            for (var r = 0; r < rows.length; r++) {
                ng.columnFields.push(rows[r]);
            }
            for (var c = 0; c < cols.length; c++) {
                ng.rowFields.push(cols[c]);
            }
        });
    }

    // build a title for the current view
    function getViewTitle(ng) {
        var title = '';
        for (var i = 0; i < ng.valueFields.length; i++) {
            if (i > 0) title += ', ';
            title += ng.valueFields[i].header;
        }
        title += ' by ';
        if (ng.rowFields.length) {
            for (var i = 0; i < ng.rowFields.length; i++) {
                if (i > 0) title += ', ';
                title += ng.rowFields[i].header;
            }
        }
        if (ng.rowFields.length && ng.columnFields.length) {
            title += ' and by ';
        }
        if (ng.columnFields.length) {
            for (var i = 0; i < ng.columnFields.length; i++) {
                if (i > 0) title += ', ';
                title += ng.columnFields[i].header;
            }
        }
        return title;
    }

    // adds a title cell into an xlxs sheet
    function addTitleCell(sheet, title) {

        // create cell
        var cell = new wijmo.xlsx.WorkbookCell();
        cell.value = title;
        cell.style = new wijmo.xlsx.WorkbookStyle();
        cell.style.font = new wijmo.xlsx.WorkbookFont();
        cell.style.font.bold = true;

        // create row to hold the cell
        var row = new wijmo.xlsx.WorkbookRow();
        row.cells[0] = cell;

        // and add the new row to the sheet
        sheet.rows.splice(0, 0, row);
    }

    // gets a simple data set for basic demos
    function getSimpleDataSet(cnt) {
        var dtOct = new Date(2016, 9, 1),
            dtDec = new Date(2016, 11, 1),
            data = [
                { product: 'Wijmo', country: 'USA', sales: 10, downloads: 100, date: dtOct, active: true },
                { product: 'Wijmo', country: 'Japan', sales: 10, downloads: 100, date: dtOct, active: false },
                { product: 'Aoba', country: 'USA', sales: 10, downloads: 100, date: dtDec, active: true },
                { product: 'Aoba', country: 'Japan', sales: 10, downloads: 100, date: dtDec, active: false }
            ];
        var countries = 'US,Japan'.split(','),
            products = 'Wijmo,Aoba'.split(',');
        for (var i = 0; i < cnt - 4; i++) {
            data.push({
                product: products[i % products.length],
                country: countries[i % countries.length],
                active: i % 2 == 0,
                date: new Date(2016 + i % 2, i % 12, i % 27 + 1),
                sales: i * 10000 - 5000,
                downloads: i * 10000 - 5000
            });
        }
        return new wijmo.CollectionView(data);
    }

    function getSimpleDataSet2(cnt) {
        var countries = 'US,Japan'.split(','),
            products = 'Wijmo,Aoba'.split(','),
            data = [];
        for (var i = 0; i < cnt; i++) {
            data.push({
                product: products[i % products.length],
                country: countries[i % countries.length],
                active: i % 2 == 0,
                date: new Date(2016 + i % 2, i % 12, i % 27 + 1),
                sales: i * 123 + 100,
                downloads: i * 50 + 100,
                discount: i / 7
            });
        }
        return new wijmo.CollectionView(data);
    }

    function getSimpleDataSet3(cnt) {
        var dtOct = new Date(2017, 9, 1),
            dtDec = new Date(2017, 11, 1),
            data = [
                { product: 'Wijmo', country: 'USA', sales: 45000, downloads: 50000, date: dtOct, active: true },
                { product: 'Wijmo', country: 'Japan', sales: 45000, downloads: 50000, date: dtOct, active: false },
                { product: 'Aoba', country: 'USA', sales: 45000, downloads: 50000, date: dtDec, active: true },
                { product: 'Aoba', country: 'Japan', sales: 45000, downloads: 50000, date: dtDec, active: false }
            ];
        var countries = 'USA,Japan'.split(','),
            products = 'Wijmo,Aoba'.split(',');
        for (var i = 0; i < cnt - 4; i++) {
            data.push({
                product: products[i % products.length],
                country: countries[i % countries.length],
                active: i % 2 == 0,
                date: new Date(2017 + i % 2, i % 12, i % 27 + 1),
                sales: i * 2 + 1000,
                downloads: i * 2 + 1000
            });
        }
        return new wijmo.CollectionView(data);
    }

    // gets a slightly more complex data set for more advanced demos
    function getDataSet(cnt) {
        var countries = 'US,Germany,UK,Japan,Italy,Greece,Spain,Portugal'.split(','),
            products = 'Wijmo,Aoba,Xuni,Olap'.split(','),
            data = [];
        for (var i = 0; i < cnt; i++) {
            data.push({
                id: i,
                product: products[i % products.length],
                country: countries[i % countries.length],
                date: new Date(2016 + i % 2, i % 12, i % 27 + 1),
                sales: i * 10000 - 5000,
                downloads: i * 10000 - 5000,
                active: i % 4 == 0,
                discount: i / 7
            });
        }
        return new wijmo.CollectionView(data);
    }

//    // get Northwind data (these are not very good sources for this demo, but are so easy to get...)
//    function getNorthwindOrders() {
//        var url = 'http://services.odata.org/V4/Northwind/Northwind.svc/';
//        return new wijmo.odata.ODataCollectionView(url, 'Order_Details_Extendeds');
//    }
//    function getNorthwindSales() {
//        var url = 'http://services.odata.org/V4/Northwind/Northwind.svc/';
//        return new wijmo.odata.ODataCollectionView(url, 'Product_Sales_for_1997');
//    }


    $scope.culture = 'en';
    $scope.$watch('culture', function () {

        // remove old localization reference
        var loc = document.getElementById('loc');
        if (loc) {
            document.head.removeChild(loc);
        }

        // add new localization reference
        loc = document.createElement('script');
        loc.id = 'loc';
        loc.type = 'text/javascript';
        loc.async = false;
        loc.src = '../scripts/vendor/wijmo.culture.' + $scope.culture + '.min.js';
        document.getElementsByTagName('head')[0].appendChild(loc);

        // show changes
        invalidateAll();
    });
    function invalidateAll() {
        setTimeout(function () {
            wijmo.Control.invalidateAll();

            if ($scope.thePanel) { // invalidate the PivotEngine as well
                $scope.thePanel.engine.invalidate();
            }
        }, 50); 
    }

    //For OlapModal
    $('#dlgPivotPanel').on('shown.bs.modal', function (e) {
        wijmo.Control.invalidateAll(e.target);
    });
    var ngModal = new wijmo.olap.PivotEngine();
    $scope.theEngine = ngModal;
    ngModal.autoGenerateFields = false;
    ngModal.itemsSource = getDataModal(1000);
    ngModal.fields.push(new wijmo.olap.PivotField(ngModal, 'person.name', 'Name'));
    ngModal.fields.push(new wijmo.olap.PivotField(ngModal, 'person.first', 'First Name'));
    ngModal.fields.push(new wijmo.olap.PivotField(ngModal, 'person.last', 'Last Name'));
    ngModal.fields.push(new wijmo.olap.PivotField(ngModal, 'timeInHours', 'Time (hrs)', { format: 'n3', dataType: wijmo.DataType.Number, aggregate: 'Sum' }));
    ngModal.fields.push(new wijmo.olap.PivotField(ngModal, 'bug.fogbugzId', 'Fogbugz #', { format: 'f0', dataType: wijmo.DataType.Number }));
    ngModal.fields.push(new wijmo.olap.PivotField(ngModal, 'bug.severity', 'Severity', { format: 'f0', dataType: wijmo.DataType.Number }));
    ngModal.rowFields.push('Last Name');
    ngModal.columnFields.push('Severity');
    ngModal.valueFields.push('Time (hrs)');
    function getDataModal(cnt) {
        var data = [];
        for (var i = 0; i < cnt; i++) {
            var minutes = Math.round(Math.random() * 160);
            var firstNames = 'Liam,Dylan,Jacob,Noah,Jayden,Ethan,Matthew,Sebastian,Alexander,Daniel,Angel'.split(',');
            var lastNames = 'Smith,Lam,Martin,Brown,Roy,Tremblay,Lee,Gagnon,Wilson,Navin'.split(',');
            data.push({
                id: i,
                person: getPerson(i, firstNames, lastNames),
                bug: getBug(i),
                timeInMinutes: minutes,
                timeInHours: minutes / 60,
            });
        }
        return data;
    }
    function getPerson(i, firstNames, lastNames) {
        var first = firstNames[i % firstNames.length];
        var last = lastNames[i % lastNames.length];
        return {
            id: i,
            name: first + ' ' + last,
            first: first,
            last: last,
            email: first[0] + last + '@componentone.com',
            value: i * 400
        }
    }
    function getBug(i) {
        return {
            id: i,
            fogbugzId: i * 10000 - 5000,
            severity: i % 4 == 0 ? 'High' : i % 4 == 1 ? 'Average' : 'Low'
        }
    }

    $scope.selectRows= function () {
        var grid = $scope.rawGrid;
        var rows = new Array();
        rows[0] = grid.rows[0];
        rows[1] = grid.rows[1];
        grid.selectedRows = rows;
        grid.focus();

    };




});
