﻿// define app, include Wijmo 5 directives
var app = angular.module('app', ['wj']);

// controller
app.controller('appCtrl', function ($scope) {

    // same data, flat format
    $scope.data = [
        { state: { code: 'WA', name: 'Washington' }, isSelected: false, isState: false, name: 'Seattle', pop: 652 },
        { state: { code: 'WA', name: 'Washington' }, isSelected: false, isState: false, name: 'Spokane', pop: 210 },
        { state: { code: 'OR', name: 'Oregon' }, isSelected: false, isState: false, name: 'Portland', pop: 609 },
        { state: { code: 'OR', name: 'Oregon' }, isSelected: false, isState: false, name: 'Eugene', pop: 159 },
        { state: { code: 'CA', name: 'California' }, isSelected: false, isState: false, name: 'Los Angeles', pop: 3884 },
        { state: { code: 'CA', name: 'California' }, isSelected: false, isState: false, name: 'San Diego', pop: 1356 },
        { state: { code: 'CA', name: 'California' }, isSelected: false, isState: false, name: 'San Francisco', pop: 837 }
    ];

    $scope.report = new wijmo.CollectionView($scope.data);

});