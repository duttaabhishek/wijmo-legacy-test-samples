﻿'use strict';

// declare app module
var app = angular.module('app');

// controller
app.controller('appCtrl', function ($scope, $http) {

    var data = [
        { id: 0, name: 'US', date: '1/1/2015', country: 'US', downloads: 10835, sales: 474.28, expenses: 2418.27, checked: true },
        { id: 1, name: 'Germany', date: '2/2/2015', country: 'Germany', downloads: 10218, sales: 7476.87, expenses: 4048.67, checked: false },
        { id: 2, name: 'UK', date: '3/3/2015', country: 'UK', downloads: 15790, sales: 6203.51, expenses: 243.15, checked: false },
        { id: 3, name: 'Japan', date: '4/4/2015', country: 'Japan', downloads: 14130, sales: 2562.53, expenses: 733.90, checked: false },
        { id: 4, name: 'Italy', date: '5/5/2015', country: 'Italy', downloads: 1231, sales: 9906.36, expenses: 1312.48, checked: true },
        { id: 5, name: 'Greece', date: '6/6/2015', country: 'Greece', downloads: 3971, sales: 7310.17, expenses: 1777.79, checked: false },
        { id: 6, name: 'US', date: '7/7/2015', country: 'US', downloads: 16266, sales: 2443.69, expenses: 1758.37, checked: false },
        { id: 7, name: 'Germany', date: '8/8/2015', country: 'Germany', downloads: 7701, sales: 6989.79, expenses: 2788.79, checked: false },
        { id: 8, name: 'UK', date: '9/9/2015', country: 'UK', downloads: 19954, sales: 8618.44, expenses: 1441.59, checked: true },
        { id: 9, name: 'Japan', date: '10/10/2015', country: 'Japan', downloads: 5553, sales: 5466.72, expenses: 23.02, checked: false }
    ];

    // expose data
    var view = new wijmo.CollectionView(data);
    view.groupDescriptions.push(new wijmo.PropertyGroupDescription('name'));
    view.groupDescriptions.push(new wijmo.PropertyGroupDescription('checked'));
    $scope.data = view;    

});