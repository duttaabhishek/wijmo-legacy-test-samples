﻿'use strict';

// declare app module
var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope) {
 $scope.ctx = {
        chart: null,
        palette: 'standard',
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        itemsSource: [],
        fruits: [],
        labels: 0,
		};

$scope.fruits = [
    { "name": "Orange", "food": "Fried Rice", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 1, 1), "y1": 10, "y2": 8, "y3": 10, "y4": -16 },
     { "name": "apple", "food": "Fried Noodle", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 2, 2), "y1": 20, "y2": 12, "y3": 16, "y4": -19 },
     { "name": "lime", "food": "Fried Egg", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 3, 3), "y1": 30, "y2": 10, "y3": 17, "y4": -15 },
     { "name": "lemon", "food": "Fried Vermicelli", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 4, 4), "y1": 40, "y2": 12, "y3": 15, "y4": -22 },
     { "name": "mango", "food": "Chicken Soup", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 5, 5), "y1": 50, "y2": 15, "y3": 23, "y4": -18 },
     { "name": "Watermelon", "food": "Pork Soup", "x": 6, "date1": new Date(2014, 6, 6), "date2": new Date(2014, 6, 6), "y1": 60, "y2": 15, "y3": 23, "y4": -18 },
     { "name": "Kiwi", "food": "Beef Soup", "x": 7, "date1": new Date(2014, 7, 7), "date2": new Date(2014, 7, 7), "y1": 70, "y2": 15, "y3": 23, "y4": -18 }
     ];

  });