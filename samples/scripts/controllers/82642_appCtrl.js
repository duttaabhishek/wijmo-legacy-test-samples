﻿'use strict';

// declare app module
var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope) {

    // generate some random data
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
        data = [];
    for (var i = 0; i < 5; i++) {
        data.push({
            id: i,
            country: countries[i % countries.length],
            date: new Date(2014, i % 12, i % 28),
            amount: (i%123)* 10000,
            active: i % 4 == 0
        });
    }

    // add data array to scope
    $scope.data = data;

    // initialize selection mode
    $scope.selectionMode = 'CellRange';
    $scope.groupBy = 'country';

    // expose the data as a CollectionView to show filtering
    $scope.filter = 'China';
    var toFilter, lcFilter;
    $scope.cvData = new wijmo.CollectionView(data);
    $scope.cvData.pageSize = 12;
    $scope.cvData.filter = function (item) { // ** filter function
        if (!$scope.filter) {
            return true;
        }
        return item.country? item.country.toLowerCase().indexOf(lcFilter) > -1 : false;
    };
    $scope.$watch('filter', function () { // ** refresh view when filter changes
        if (toFilter) {
            clearTimeout(toFilter);
        }
        toFilter = setTimeout(function () {
            lcFilter = $scope.filter.toLowerCase();
            $scope.cvData.refresh();
        }, 100);
    });

    // update CollectionView group descriptions when groupBy changes
    $scope.$watch('groupBy', function () {
        var cv = $scope.cvData;
        cv.groupDescriptions.clear();
        if ($scope.groupBy) {
            var groupNames = $scope.groupBy.split(',');
            for (var i = 0; i < groupNames.length; i++) {
                var groupName = groupNames[i];
                if (groupName == 'date') { // group dates by year
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.date.getFullYear();
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else if (groupName == 'amount') { // group amounts in ranges
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.amount >= 5000 ? '> 5,000' : item.amount >= 500 ? '500 to 5,000' : '< 500';
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else { // group everything else by value
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName);
                    cv.groupDescriptions.push(groupDesc);
                }
            }
        }
    });

    // get the color to be used for displaying an amount
    $scope.getAmountColor = function (amount) {
        if (amount < 500) return 'darkred';
        if (amount < 2500) return 'black';
        return 'darkgreen';
    }

    // tell scope when current item changes
    $scope.cvData.currentChanged.addHandler(function () {
        $scope.$apply('cvData.currentItem');
    });
});
