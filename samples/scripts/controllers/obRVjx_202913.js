"use strict";
/**
 * Provides sample data and methods.
 */
var sample;
(function (sample) {
    'use strict';
    sample.culture = {
        currencyRate: 1,
        dateFormat: 'FY{0}',
        productNames: ['Chai', 'Chang', 'Aniseed Syrup', 'Ikura', 'Konbu'],
        customerNames: ['Doe Enterprises', 'Hill Corporation', 'Holmes World', 'Valentine Hearts', 'Frankson Media'],
        personNames: ['Nancy Davolio', 'Andrew Fuller', 'Janet Leverling', 'Margaret Peacock', 'Steven Buchanan'],
        countryNames: ['USA', 'Japan', 'China', 'Germany', 'United Kingdom', 'Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire', 'Bosnia', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Canary Islands', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Channel Islands', 'Chile', 'Christmas Island', 'Cocos Island', 'Colombia', 'Comoros', 'Congo', 'Cook Islands', 'Costa Rica', "Cote D'Ivoire", 'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Ter', 'Gabon', 'Gambia', 'Georgia', 'Ghana', 'Gibraltar', 'Great Britain', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guyana', 'Haiti', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea North', 'Korea South', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malaysia', 'Malawi', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Midway Islands', 'Moldova', 'Monaco', 'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Nambia', 'Nauru', 'Nepal', 'Netherland Antilles', 'Netherlands', 'Nevis', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Norway', 'Oman', 'Pakistan', 'Palau Island', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Island', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Republic of Montenegro', 'Republic of Serbia', 'Reunion', 'Romania', 'Russia', 'Rwanda', 'St Barthelemy', 'St Eustatius', 'St Helena', 'St Kitts-Nevis', 'St Lucia', 'St Maarten', 'Saipan', 'Samoa', 'San Marino', 'Saudi Arabia', 'Scotland', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Tahiti', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tokelau', 'Tonga', 'Trinidad Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks & Caicos Is', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City State', 'Venezuela', 'Vietnam', 'Virgin Islands (British)', 'Virgin Islands (USA)', 'Wake Island', 'Yemen', 'Zaire', 'Zambia', 'Zimbabwe']
    };
    var _productsCache = null;
    function products() {
        if (_productsCache != null)
            return _productsCache;
        _productsCache = [];
        for (var i = 0; i < sample.culture.productNames.length; i++) {
            _productsCache.push({
                id: i,
                name: sample.culture.productNames[i],
                price: random(20, 10) * sample.culture.currencyRate
            });
        }
        return _productsCache;
    }
    sample.products = products;
    function countries() {
        var items = [];
        for (var i = 0; i < sample.culture.countryNames.length; i++) {
            items.push({
                id: i,
                name: sample.culture.countryNames[i]
            });
        }
        return items;
    }
    sample.countries = countries;
    function countryNames() {
        return sample.culture.countryNames;
    }
    sample.countryNames = countryNames;
    function ordersRaw(length) {
        if (length === void 0) { length = 5; }
        var items = [];
        for (var i = 0; i < length; i++) {
            var date = new Date((new Date()).getFullYear() - 4, 0, 1);
            var pId = i% sample.culture.productNames.length;
            items.push({
                id: i,
                productId:  i,
                date: _addDays(date, random(365 * 4)),
                quantity: random(20, 1),
                discount: random(3) == 0
            });
        }
        return items;
    }
    sample.ordersRaw = ordersRaw;
    function orders(length) {
        if (length === void 0) { length = 5; }
        var _orders = ordersRaw(length);
        var _products = products();
        var items = [];
        for (var i = 0; i < length; i++) {
            items.push({
                id: _orders[i].id,
                product: _products[_orders[i].productId].name,
                date: _orders[i].date,
                amount: _orders[i].quantity * _products[_orders[i].productId].price * (_orders[i].discount ? 8 : 10) / 10,
                discount: _orders[i].discount
            });
        }
        return items;
    }
    sample.orders = orders;
    function ordersDetail(length) {
        if (length === void 0) { length = 5; }
        var _orders = ordersRaw(length);
        var _products = products();
        var items = [];
        for (var i = 0; i < length; i++) {
            items.push({
                product: _products[_orders[i].productId].name,
                date: _orders[i].date,
                quantity: _orders[i].quantity,
                amount: _orders[i].quantity * _products[_orders[i].productId].price * (_orders[i].discount ? 8 : 10) / 10,
                discount: _orders[i].discount
            });
        }
        return items;
    }
    sample.ordersDetail = ordersDetail;
    function salesRaw() {
        var _sales = [400, 300, 200, 100, 50];
        var items = [];
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < sample.culture.productNames.length; j++) {
                items.push({
                    date: sample.culture.dateFormat.replace('{0}', new Date().getFullYear() - 4 + i),
                    product: sample.culture.productNames[j]
                });
                _sales[j] = Math.floor(_sales[j] * random(95, 115) / 100);
                items[items.length - 1].target = _sales[j];
                _sales[j] = Math.floor(_sales[j] * random(80, 120) / 100);
                items[items.length - 1].result = _sales[j];
            }
        }
        return items;
    }
    sample.salesRaw = salesRaw;
    function sales() {
        var _sales = salesRaw();
        var _dates = [];
        var items = [];
        for (var i = 0; i < _sales.length; i++) {
            var index = _dates.indexOf(_sales[i].date);
            if (index > -1) {
                items[index].target += _sales[i].target;
                items[index].result += _sales[i].result;
            }
            else {
                _dates.push(_sales[i].date);
                items.push({
                    date: _sales[i].date,
                    target: _sales[i].target,
                    result: _sales[i].result
                });
            }
        }
        return items;
    }
    sample.sales = sales;
    function salesByProduct() {
        var _sales = salesRaw();
        var _products = [];
        var items = [];
        for (var i = 0; i < _sales.length; i++) {
            var index = _products.indexOf(_sales[i].product);
            if (index > -1) {
                items[index].target += _sales[i].target;
                items[index].result += _sales[i].result;
            }
            else {
                _products.push(_sales[i].product);
                items.push({
                    product: _sales[i].product,
                    target: _sales[i].target,
                    result: _sales[i].result
                });
            }
        }
        return items;
    }
    sample.salesByProduct = salesByProduct;
    function objectLog(value, name, htmlElement) {
        if (htmlElement === void 0) { htmlElement = document.body; }
        var e1 = document.createElement('p');
        e1.innerText = name;
        htmlElement.appendChild(e1);
        var e2 = document.createElement('pre');
        e2.innerText = JSON.stringify(value, null, '  ');
        htmlElement.appendChild(e2);
    }
    sample.objectLog = objectLog;
    /**
     * Writes Wijmo control's event logs to console.
     * @param control  Control to be added event log.
     * @param ignoredEvents  Events that are not shown in log.
     * @param showArgs  Whether event args are shown in log.
     */
    function eventLog(control, ignoredEvents, showArgs) {
        if (showArgs === void 0) { showArgs = false; }
        for (var eventName in control) {
            var event = control[eventName];
            if (event instanceof Object && event.hasOwnProperty('_handlers')) {
                if (ignoredEvents != undefined && ignoredEvents.indexOf(eventName) > -1) {
                    continue;
                }
                if (showArgs) {
                    control[eventName].addHandler(function (s, e) {
                        console.log(this.toString(), e);
                    }, eventName);
                }
                else {
                    control[eventName].addHandler(function (s, e) {
                        console.log(this.toString());
                    }, eventName);
                }
            }
        }
    }
    sample.eventLog = eventLog;
    /**
     * Generates random number between 2 numbers.
     * @param max  Max value of random number.
     * @param min  Minimum value of randum number.
     * @return  Random number.
     */
    function random(max, min) {
        if (min === void 0) { min = 0; }
        return Math.floor(Math.random() * (max - min)) + min;
    }
    sample.random = random;
    /**
     * Gets ramdom item from array items.
     * @param items  Array items.
     * @return  Random item.
     */
    function randomArray(items) {
        return items[random(items.length)];
    }
    sample.randomArray = randomArray;
    function _addDays(value, days) {
        return new Date(value.getFullYear(), value.getMonth(), value.getDate() + days);
    }
    ;
    function _addYears(value, years) {
        return new Date(value.getFullYear() + years, value.getMonth(), value.getDate());
    }
    ;
})(sample || (sample = {}));