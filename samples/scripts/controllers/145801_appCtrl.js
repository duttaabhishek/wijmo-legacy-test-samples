﻿'use strict';

angular.module('app').controller('standaloneController', function ($scope) {
	$scope.ctx = {
		flexSheet: null,
		sheets: []
	};

	// initialize the flexSheet control when document ready.
	$scope.initialized = function (s) {
		var column;

		for (var i = 0; i < s.sheets.length; i++) {
			s.sheets.selectedIndex = i;
			if (s.sheets[i].name === 'Sheet1') {
				generateFirstSheet(s);
			}
			
			$scope.ctx.sheets.push(s.sheets[i].name);
		}
		//s.selectedSheetIndex = 0;
		s.refresh();
		$scope.$apply('ctx.sheets');

		var flexSheet = s;
		flexSheet.beginningEdit.addHandler(function (sender, e) {
		    if (e.row == 1 && e.col == 0) {
		        e.cancel = true;
		    }
		});
	};
	
	function generateFirstSheet(flexSheet) {
		flexSheet.setCellData(0, 0, 1)
        flexSheet.setCellData(0, 1, 2)
        flexSheet.setCellData(0, 2, '=sum(A1:B1)')
	}
	
	$scope.fsheet_setCellData = function () {
	    var flexSheet = $scope.ctx.flexSheet;
	    flexSheet.setCellData(0,0,2);
	};
	
	

});