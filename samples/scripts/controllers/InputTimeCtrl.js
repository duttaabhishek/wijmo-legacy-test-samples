﻿'use strict';

var app = angular.module('app');

// input controller: provides a list of countries and some input values
app.controller('inputTimeCtrl', function appCtrl($scope, $filter, $location, dataSvc) {

    // data context
    $scope.ctx = {

        // data
        //value: new Date(2014, 5, 17, 9,45,30),
        value: new Date(2014, 5, 17, 18,30,0),
        inpTime1: null,
        displayDate: new Date(2014, 5, 17, 18,30,0),
        maxDate: new Date($.now()),
        minDate: new Date($.now()),

        

        // culture
        culture: 'en'
    };

    $scope.set_focus = function () {

        var inpTime = $scope.ctx.inpTime1;

        inpTime.focus();

    };

    // when the culture changes, load the new culture, apply, and invalidate
    $scope.$watch('ctx.culture', function () {
        $.ajax({
            url:  '/scripts/vendor/wijmo.culture.' + $scope.ctx.culture + '.min.js',
            dataType: 'script',
            success: function (data) {
                eval(data);

                // culture applied, now load translations
                $.ajax({
                    url: '/translations/strings.' + $scope.ctx.culture + '.js',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        $scope.i18n = data;

                        // show changes
                        $scope.$apply();
                        invalidateWijmoControls(document.body);
                    }
                });
            },
        });
    });

   
   // invalidate all Wijmo controls on the page
    function invalidateWijmoControls(e) {
        var ctl = wijmo.Control.getControl(e);
        if (ctl) {
            ctl.invalidate();
        }
        if (e.children) {
            for (var i = 0; i < e.children.length; i++) {
                invalidateWijmoControls(e.children[i]);
            }
        }
    }

    
});
