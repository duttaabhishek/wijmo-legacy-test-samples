﻿var app = angular.module("app", ['wj']);

function appCtrl($scope) {

    $scope.chartProps = {
        chartType: wijmo.chart.finance.FinancialChartType.Line,
        legendPosition: wijmo.chart.Position.Right,
        bindingY: 'close',
        header: 'Facebook, Inc. (FB)',
        footer: 'FinancialChartTesting',
        movingAveragePeriod: 2,
        movingAverageType: wijmo.chart.analytics.MovingAverageType.Simple,
        rangeSelector: null,
        data: [],
         properties: {
            retracements: {
                labelPosition: 'Left',
                uptrend: true,               
                low: 70              
            },
            arcs: {
                labelPosition: 'Top',
                start: new wijmo.chart.DataPoint(3, 75),
                end: new wijmo.chart.DataPoint(8, 79),
                visibility: 'Visible'
            },
            fans: {
                labelPosition: 'Top',
                start: new wijmo.chart.DataPoint(2, 75),
                end: new wijmo.chart.DataPoint(7, 79),
                visibility: 'Visible'

            },

            fitType: 'Linear',
            order: 2,
            sampleCount: 150,
            // Bollinger Bands
            bollingerPeriod: 20,
            bollingerMultiplier: 2,
        }
    };

    $scope.tpChart = null; 
    $scope.sampleCount = 150;
    $scope.order = 2;
    $scope.inputSampleCount = null;
    $scope.inputOrder = null;
   

    $scope.ctx = {
        reversalInput: null,
        chart: null,
        options:{
            lineBreak: {
                newLineBreaks: 3
            },
            renko: {
                boxSize: 2,
                rangeMode: 'Fixed',
                fields: 'Close'
            },
            kagi: {
                reversalAmount: 1,
                rangeMode: 'Fixed',
                fields: 'Close'
            }
        },
        style: {
            stroke: 'rgb(136, 189, 230)',
            fill: 'rgba(136, 189, 230, 0.701961)'
        },
        altStyle: {
            stroke: 'rgb(136, 189, 230)',
            fill: 'transparent'
        },
        properties: {
            fitType: 'Linear',
            order: 2,
            sampleCount: 150
        },
        chart1:null,
        selector:null,
        fibo: null,
        lineMarker: null,
        fiboArcs: {
            start: new wijmo.chart.DataPoint(2, 75),
            end: new wijmo.chart.DataPoint(5, 76)
        },
         fiboFans:{
           start: new wijmo.chart.DataPoint(2, 75),
            end: new wijmo.chart.DataPoint(5, 76)
        },
        fiboTimezones: null,

        //overlays
		bollband: null,
        enve:null,
        properties: {
            // Bollinger Bands
            bollingerPeriod: 20,
            bollingerMultiplier: 2,
            
            // Moving Average Envelopes
            envelopePeriod: 20,
            envelopeType: 'Simple',
            envelopeSize: 0.03
        }
    };
    
    $scope.rangeChanged = function (sender, e) {
        var stChart = $scope.ctx.chart, rs = $scope.ctx.selector;

        stChart.axisX.min = rs.min;
        stChart.axisX.max = rs.max;
        stChart.invalidate();

    };

    $scope.data = [
        { "date": "01/05/15", "open": 77.98, "high": 79.25, "low": 76.86, "close": 77.19, "volume": 26452191 },
        { "date": "01/06/15", "open": 77.23, "high": 77.59, "low": 75.36, "close": 76.15, "volume": 27399288 },
        { "date": "01/07/15", "open": 76.76, "high": 77.36, "low": 75.82, "close": 76.15, "volume": 22045333 },
        { "date": "01/08/15", "open": 76.74, "high": 78.23, "low": 76.08, "close": 78.18, "volume": 23960953 },
        { "date": "01/09/15", "open": 78.2, "high": 78.62, "low": 77.2, "close": 77.74, "volume": 21157007 },
        { "date": "01/12/15", "open": 77.84, "high": 78, "low": 76.21, "close": 76.72, "volume": 19190194 },
        { "date": "01/13/15", "open": 77.23, "high": 78.08, "low": 75.85, "close": 76.45, "volume": 25179561 },
        { "date": "01/14/15", "open": 76.42, "high": 77.2, "low": 76.03, "close": 76.28, "volume": 25918564 },
        { "date": "01/15/15", "open": 76.4, "high": 76.57, "low": 73.54, "close": 74.05, "volume": 34133974 },
        { "date": "01/16/15", "open": 74.04, "high": 75.32, "low": 73.84, "close": 75.18, "volume": 21791529 },
        { "date": "01/20/15", "open": 75.72, "high": 76.31, "low": 74.82, "close": 76.24, "volume": 22821614 },
        { "date": "01/21/15", "open": 76.16, "high": 77.3, "low": 75.85, "close": 76.74, "volume": 25096737 },
        { "date": "01/22/15", "open": 77.17, "high": 77.75, "low": 76.68, "close": 77.65, "volume": 19519458 },
        { "date": "01/23/15", "open": 77.65, "high": 78.19, "low": 77.04, "close": 77.83, "volume": 16746503 },
        { "date": "01/26/15", "open": 77.98, "high": 78.47, "low": 77.29, "close": 77.5, "volume": 19260820 },
        { "date": "01/27/15", "open": 76.71, "high": 76.88, "low": 75.63, "close": 75.78, "volume": 20109977 },
        { "date": "01/28/15", "open": 76.9, "high": 77.64, "low": 76, "close": 76.24, "volume": 53306422 },
        { "date": "01/29/15", "open": 76.85, "high": 78.02, "low": 74.21, "close": 78, "volume": 61293468 },
        { "date": "01/30/15", "open": 78, "high": 78.16, "low": 75.75, "close": 75.91, "volume": 42649491 },
        { "date": "02/02/15", "open": 76.11, "high": 76.14, "low": 73.75, "close": 74.99, "volume": 41955258 },
        { "date": "02/03/15", "open": 75.19, "high": 75.58, "low": 73.86, "close": 75.4, "volume": 26957714 },
        { "date": "02/04/15", "open": 75.09, "high": 76.35, "low": 75.01, "close": 75.63, "volume": 20277368 },
        { "date": "02/05/15", "open": 75.71, "high": 75.98, "low": 75.21, "close": 75.62, "volume": 15062573 },
        { "date": "02/06/15", "open": 75.68, "high": 75.7, "low": 74.25, "close": 74.47, "volume": 21210994 },
        { "date": "02/09/15", "open": 74.05, "high": 74.83, "low": 73.45, "close": 74.44, "volume": 16194322 },
        { "date": "02/10/15", "open": 74.85, "high": 75.34, "low": 74.5, "close": 75.19, "volume": 15811344 },
        { "date": "02/11/15", "open": 75.09, "high": 76.75, "low": 75.03, "close": 76.51, "volume": 20877427 },
        { "date": "02/12/15", "open": 76.86, "high": 76.87, "low": 75.89, "close": 76.23, "volume": 17234976 },
        { "date": "02/13/15", "open": 76.46, "high": 76.48, "low": 75.5, "close": 75.74, "volume": 18621860 },
        { "date": "02/17/15", "open": 75.3, "high": 76.91, "low": 75.08, "close": 75.6, "volume": 25254400 },
        { "date": "02/18/15", "open": 75.94, "high": 76.9, "low": 75.45, "close": 76.71, "volume": 22426421 },
        { "date": "02/19/15", "open": 76.99, "high": 79.84, "low": 76.95, "close": 79.42, "volume": 45851177 },
        { "date": "02/20/15", "open": 79.55, "high": 80.34, "low": 79.2, "close": 79.9, "volume": 36931698 },
        { "date": "02/23/15", "open": 79.96, "high": 80.19, "low": 78.38, "close": 78.84, "volume": 24139056 },
        { "date": "02/24/15", "open": 78.5, "high": 79.48, "low": 78.1, "close": 78.45, "volume": 18897133 }
    ]
$scope.data1 = [
   { "date": "01/05/15", "open": 77000000000.98, "high": 79000000000.25, "low": 76000000000.86, "close": 77000000000.19, "volume": 26452191000 },
    { "date": "01/06/15", "open": 77000000000.23, "high": 77000000000.59, "low": 75000000000.36, "close": 76000000000.15, "volume": 27399288000 },
    { "date": "01/07/15", "open": 76000000000.76, "high": 77000000000.36, "low": 75000000000.82, "close": 76000000000.15, "volume": 22045333000 },
    { "date": "01/08/15", "open": 76000000000.74, "high": 78000000000.23, "low": 76000000000.08, "close": 78000000000.18, "volume": 23960953000},
    { "date": "01/09/15", "open": 78000000000.2, "high": 78000000000.62, "low": 77000000000.2, "close": 77000000000.74, "volume": 21157007000 },
    { "date": "01/12/15", "open": 77000000000.84, "high": 78000000000, "low": 76000000000.21, "close": 76000000000.72, "volume": 19190194000 },
    { "date": "01/13/15", "open": 770000000000.23, "high": 78000000000.08, "low": 75000000000.85, "close": 76000000000.45, "volume": 25179561000 },
    { "date": "01/14/15", "open": 76000000000.42, "high": 77000000000.2, "low": 76000000000.03, "close": 76000000000.28, "volume": 25918564000 },
    { "date": "01/15/15", "open": 760000000000.4, "high": 76000000000.57, "low": 73000000000.54, "close": 74000000000.05, "volume": 34133974000 },
    { "date": "01/16/15", "open": 74000000000.04, "high": 75000000000.32, "low": 73000000000.84, "close": 75000000000.18, "volume": 21791529000 },
    { "date": "01/20/15", "open": 750000000000.72, "high": 76000000000.31, "low": 74000000000.82, "close": 76000000000.24, "volume": 22821614000 },
    { "date": "01/21/15", "open": 760000000000.16, "high": 77000000000.3, "low": 75000000000.85, "close": 76000000000.74, "volume": 25096737000 },
    { "date": "01/22/15", "open": 770000000000.17, "high": 77000000000.75, "low": 76000000000.68, "close": 77000000000.65, "volume": 19519458000 },
    { "date": "01/23/15", "open": 77000000000.65, "high": 78000000000.19, "low": 77000000000.04, "close": 77000000000.83, "volume": 16746503000 },
    { "date": "01/26/15", "open": 770000000000.98, "high": 78000000000.47, "low": 77000000000.29, "close": 77000000000.5, "volume": 19260820000 },
    { "date": "01/27/15", "open": 760000000000.71, "high": 76000000000.88, "low": 75000000000.63, "close": 75000000000.78, "volume": 20109977000 }
	];

    $scope.optionChanged = function (sender, args) {
        $scope.ctx.chart.invalidate();
    };

    $scope.option1Changed = function (sender, args) {
        $scope.tpChart.invalidate();
    };

    //Fibonacci Tools
    // invalidate when data points change
    $scope.valueChanged = function (sender, e) {
        $scope.ctx.chart.invalidate();
    };
    $scope.value1Changed = function (sender, args) {
        $scope.tpChart.invalidate();
    };  

    //Range Selector
    //rangeChanged Event
    $scope.chartRangeChanged = function () {
        if ($("#chkrangeChanged").is(":checked")) {
            var value = " RangeChanged event fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else {
            $("#EventList").append("");
        }
    }
     //Range Selector
    $scope.remove = function () {
        var selector = $scope.ctx.selector;
        selector.remove();
    };
    $scope.xFormatN = function () {
        var tpchart = $scope.tpChart;
        tpchart.axisX.format = "n,";
    };
	    $scope.xFormatN1 = function () {
        var tpchart = $scope.tpChart;
        tpchart.axisX.format = "n,,";
		};
		$scope.yFormatN = function () {
        var tpchart = $scope.tpChart;
        tpchart.axisY.format = "n,";
    };
	    $scope.yFormatN1 = function () {
        var tpchart = $scope.tpChart;
        tpchart.axisY.format = "n,,";
		};

    $scope.isContentHtml = function () {
        var chart = $scope.tpChart;
        chart.tooltip.content = '<b>Sales</b><br/>Test';
        chart.tooltip.isContentHtml = !chart.tooltip.isContentHtml;
    };
     $scope.seriesVisibilityChanged = function () {       
            var value = "SeriesVisibilityChanged event fired";
            $("#EventList").append('<li>' + value + '</li>')        
    }

    $scope.selectionChanged = function () {       
            var value = "SelectionChanged event fired";
            $("#EventList").append('<li>' + value + '</li>')        
    }

    $scope.rendering = function () {       
            var value = "rendering event fired";
            $("#EventList").append('<li>' + value + '</li>')        
    }

    $scope.rendered = function () {       
            var value = "rendered event fired";
            $("#EventList").append('<li>' + value + '</li>')        
    }
    var bindingYs = {
        1:'close',
        0: 'close',
        2: 'close',
        4: 'close',
        5: 'high,low,open,close',
        6: 'high,low,open,close',
        7: 'high,low,open,close',
        8: 'high,low,open,close',
        9: 'high,low,open,close',
        10: 'high,low,open,close',
        11: 'close,volume',
        12: 'high,low,open,close,volume',
        13: 'high,low,open,close,volume',
        14: 'high,low,open,close,volume'
    };
    $scope.$watch('chartProps.movingAverageType', function () {
        $scope.chartProps.movingAverageName = wijmo.chart.analytics.MovingAverageType[$scope.chartProps.movingAverageType] + ' Moving Average';
    });

    $scope.$watch('movingAveragePeriod', function () {
        var input = $scope.inputPeriod,
            val = $scope.movingAveragePeriod;

        if (!input) {
            return;
        }
        if (val < input.min || val > input.max) {
            return;
        }
        $scope.chartProps.movingAveragePeriod = val;
    });

    //by kendra
    $scope.$watch('sampleCount', function () {
        var input = $scope.inputSampleCount;

        if (input == null || input.value < input.min || input.value > input.max) {
            return;
        }
        $scope.ctx.properties.sampleCount = input.value;
    });

    $scope.$watch('order', function () {
        var input = $scope.inputOrder;

        if (input == null || input.value < input.min || input.value > input.max) {
            return;
        }
        $scope.ctx.properties.order = input.value;
    });

    $scope.$watch('chartProps.chartType', function () {
        var chartProps = $scope.chartProps, tpChart = $scope.tpChart;
        if (!tpChart) {
            return;
        }
        chartProps.bindingY = bindingYs[chartProps.chartType];

        switch (chartProps.chartType) {
            case wijmo.chart.finance.FinancialChartType.LineBreak:
                tpChart.options = {
                    lineBreak: {
                        newLineBreaks: 3
                    }
                };
                break;
            case wijmo.chart.finance.FinancialChartType.Renko:
                tpChart.options = {
                    renko: {
                     reversalAmount: 1,
                        boxSize: 2,
                        //rangeMode: 'Fixed',
                        //fields: 'Close'
                    }
                };
                break;
            case wijmo.chart.finance.FinancialChartType.Kagi:
                tpChart.options = {
                    kagi: {
                      reversalAmount: 1,
                       // rangeMode: 'Fixed',
                        //fields: 'Close'
                    }
                };
                break;
            default:
                break;
        }
    });
    
    $scope.removeRange = function () {
           $scope.chartProps.rangeSelector.remove();
    }

    $scope.rangeModeChanged = function (sender, args) {
        var reversalInput = $scope.ctx.reversalInput;
        if (sender.selectedValue === 'Percentage') {
            reversalInput.format = 'p0';
            reversalInput.min = 0;
            reversalInput.max = 1;
            reversalInput.value = wijmo.clamp(reversalInput.value, 0, .01);
            reversalInput.step = 0.01;
        } else if (sender.selectedValue === 'ATR') {
            reversalInput.format = 'n0';
            reversalInput.min = 1;
            reversalInput.max = $scope.data.length - 2;
            reversalInput.value = wijmo.clamp(reversalInput.value, 14, $scope.data.length - 2);
            reversalInput.step = 1;
        } else {
            reversalInput.format = 'n0';
            reversalInput.min = 1;
            reversalInput.max = null;
            reversalInput.value = 1;
            reversalInput.step = 1;
        }

        $scope.option1Changed(sender, args);
    };
    //LineMarker
    $scope.positionChanged = function (s, point) {
        pt = point;

        $scope.changeContent = function () {
            var retval = null, hti, item;
            if ($scope.ctx.chart && pt) {
                // hit test
                hti = $scope.ctx.chart.hitTest(pt);

                // check hit test & use its values for LineMarker's content
                if (hti && hti.item) {
                    item = hti.item;
                    retval =
                        'Date: ' + item.date + '<br />' +
                        'Open: ' + wijmo.Globalize.format(item.open, 'n2') + '<br />' +
                        'High: ' + wijmo.Globalize.format(item.high, 'n2') + '<br />' +
                        'Low: '  + wijmo.Globalize.format(item.low, 'n2') + '<br />' +
                        'Close: ' + wijmo.Globalize.format(item.close, 'n2');
                }
            }

            // return content string
            return retval;
        }
    }

    //by Selena
    $scope.$watch('tpChart', function () {
        var chart = $scope.tpChart;
        if (!chart) {
            return;
        }
        updateChart();
    });
 
    function updateChart() {
        var chart = $scope.tpChart;

        if (chart) {
            chart.refresh(true);
        }
    }

    $scope.selectedFib = 'retracements';
  
    $scope.changeSelectedFib = function(sender,args) {
        $scope.visible ='Visible';
    }

    $scope.changedVisible = function(sender, args) {
        var menu = sender;

        if($scope.selectedFib === 'retracements' )
        {
           $scope.chartProps.properties.retracements.retracements.visibility = menu.selectedValue;
            
        }
        else if($scope.selectedFib === 'arcs')
        {
            $scope.chartProps.properties.arcs.arcs.visibility = menu.selectedValue;
        }
        else if($scope.selectedFib === 'fans')
        {
           $scope.chartProps.properties.fans.fans.visibility = menu.selectedValue;
        }
        else
        {
             $scope.chartProps.properties.timeZones.zones.visibility= menu.selectedValue;
        }
    }

    $scope.boxSizeChanged = function (sender, args) {
        if (sender.value < sender.min || (sender.max && sender.value > sender.max)) {
            return;
        }
        $scope.ctx.renkoChart.invalidate();
    };
    $scope.reversalAmountChanged = function (sender, args) {
        if (sender.value < sender.min || (sender.max && sender.value > sender.max)) {
            return;
        }
        $scope.ctx.chart.invalidate();
    };
   $scope.kagiRangeModeChanged = function (sender, args) {
        var reversalInput = $scope.ctx.reversalInput;
        if (sender.selectedValue === 'Percentage') {
            reversalInput.format = 'p0';
            reversalInput.min = 0;
            reversalInput.max = 1;
            reversalInput.value = wijmo.clamp(reversalInput.value, 0, .01);
            reversalInput.step = 0.01;
        } else if (sender.selectedValue === 'ATR') {
            reversalInput.format = 'n0';
            reversalInput.min = 1;
            reversalInput.max = $scope.data.length - 2;
            reversalInput.value = wijmo.clamp(reversalInput.value, 14, $scope.data.length - 2);
            reversalInput.step = 1;
        } else {
            reversalInput.format = 'n0';
            reversalInput.min = 1;
            reversalInput.max = null;
            reversalInput.value = 1;
            reversalInput.step = 1;
        }
        $scope.ctx.chart.invalidate();
    };
    $scope.rengoRangeModeChanged = function (sender, args) {
        if (sender.selectedValue === 'ATR') {
            $scope.ctx.boxSizeInput.format = 'n0';
            $scope.ctx.boxSizeInput.min = 2;
            $scope.ctx.boxSizeInput.max = $scope.data.length - 2;
            $scope.ctx.boxSizeInput.value = wijmo.clamp($scope.ctx.boxSizeInput.value, 14, $scope.data.length - 2);
            $scope.ctx.boxSizeInput.step = 1;
        } else {
            $scope.ctx.boxSizeInput.format = 'n0';
            $scope.ctx.boxSizeInput.min = 1;
            $scope.ctx.boxSizeInput.max = null;
            $scope.ctx.boxSizeInput.step = 1;
        }
        $scope.ctx.renkoChart.invalidate();
    };

    $scope.chart = null;
    $scope.options = {      
        kagi: {
            reversalAmount: 2,
            rangeMode: "Fixed",
            fields: "Close"
        },

        pointAndFigure: {
            boxSize: 1,
            reversal: 3,
            scaling: 'Traditional',
            fields: 'Close',
            period: 20
        }
    };   

    $scope.nullData = getNullData();

    $scope.optionChanged = function (s,e) {
        if ($scope.chart) {
            $scope.chart.refresh(true);
        }
    };

    function getNullData() {
        var item = [];
        item = [
            { "date": "01/23/15", "open": 24, "high": 24.73, "low": 20.16, "close": null, "volume": 42593223 },
			{ "date": "01/26/15", "open": 23.67, "high": 24.39, "low": 22.5, "close": 22.6, "volume": 8677164 },
			{ "date": "01/27/15", "open": 22, "high": 22.47, "low": 21.17, "close": 21.3, "volume": 3272512 },
			{ "date": "01/28/15", "open": 21.62, "high": 23, "low": 19.6, "close": 19.78, "volume": 5047364 },
			{ "date": "01/29/15", "open": 19.9, "high": 19.95, "low": 18.51, "close": 18.8, "volume": 3419482 },
			{ "date": "01/30/15", "open": 18.47, "high": 19.48, "low": 19, "close": 18.81, "volume": 2266439 },
			{ "date": "02/02/15", "open": 19.18, "high": 19.3, "low": 18.01, "close": 18.02, "volume": 2071168 },
			{ "date": "02/03/15", "open": 18.22, "high": 18.64, "low": 18.12, "close": 19, "volume": 1587435 },
			{ "date": "02/04/15", "open": 18.2, "high": 18.35, "low": 17, "close": 17.1, "volume": 2912224 },
			{ "date": "02/05/15", "open": 17.3, "high": 17.31, "low": 16.41, "close": 16.66, "volume": 2913334 },
			{ "date": "02/06/15", "open": 17.39, "high": 18.88, "low": 17.21, "close": 18.12, "volume": 3929164 },
			{ "date": "02/09/15", "open": 18.86, "high": 19.95, "low": 18.45, "close": 19.6, "volume": 3226650 }				
        ];
        return item;
    }

    //#region Properties for MACD
    $scope.slowPeriod = 4;
    $scope.smoothingPeriod = 9;
    $scope.fastPeriod = 8;
    $scope.macdStyles = {   // named styles
        macdLine: {
            stroke: '#bfa554',
            strokeWidth: 1
        },
        signalLine: {
            stroke: 'red',
            strokeWidth: 1
        }
    };
    
    var pt = new wijmo.Point();
    $scope.point = 5;
    $scope.annotation = {
        annoCtrl:null
    }

    $scope.changePointValue = function(sender,e) {
        $scope.annotation.annoCtrl.point = new wijmo.Point(0.4,0.45);
    
        $scope.tpChart.invalidate();
    };

    $scope.symbolSize = 5;
    //#endregion


}