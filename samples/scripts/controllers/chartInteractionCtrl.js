﻿'use strict';
var app = angular.module("app", ['wj']);
app.controller('appCtrl', ('$scope', function ($scope) {
    // any code here
    var pt = new wijmo.Point();
    $scope.ctx = {
        chart: null,
        selector:null,
        zoomChart: null,
        data: [],
        chartGestures: null
    };
    $scope.data = [
     { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 9000, "y2": 6000, "y3": 4000, "y4": 3000 },
     { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 8000, "y2": 4000, "y3": 5000, "y4": 2000 },
     { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 7000, "y2": 7000, "y3": 7000, "y4": 5000 },
     { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 6000, "y2": 8000, "y3": 8000, "y4": 6000 },
     { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 5000, "y2": 1000, "y3": 3000, "y4": 1000 },
     { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 4000, "y2": 2000, "y3": 9000, "y4": 4000 },
     { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 0, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 3000, "y2": 3000, "y3": 6000, "y4": 7000 },
    ];
    //Range Selector
    $scope.remove = function () {
        var selector = $scope.ctx.selector;
        selector.remove();
    };
    
    //Chart Gestures 
    $scope.removeGestures = function () {
        var gestures = $scope.ctx.chartGestures;
        gestures.remove();
    };

    //reset axes
    $scope.reset = function () {
        var resetAxes = $scope.ctx.chartGestures;
        resetAxes.reset();
    };

    //rangeChanged event
    $scope.rangeChanged = function () {
        var value = "rangeChanged";
        $("#eventDiv").append('<li>' + value + '</li>');
    }
   
})
);