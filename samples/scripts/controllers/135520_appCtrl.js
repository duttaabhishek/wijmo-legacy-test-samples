(function() {
	'use strict';

	angular
		.module('app', ['wj'])
		.controller('135520_appCtrl', function($scope) {		
			
		    $scope.ctx = {
                today: new Date(),
		        departureDate: new Date(2015, 10, 1)		       
		    };

		});
})();