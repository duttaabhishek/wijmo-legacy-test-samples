﻿// declare app module
var app = angular.module('app', ['wj']);

// controller provides data
app.controller('appCtrl', function appCtrl($scope) {
    var names = ['Oranges', 'Apples', 'Pears', 'Bananas', 'Pineapples'],
                data = [];

    // populate itemsSource
    for (var i = 0; i < names.length; i++) {
        data.push({
            name: names[i],
            value: Math.round(Math.random() * 100)
        });
    }
    var cv = new wijmo.CollectionView(data);
    cv.moveCurrentToPosition(-1);

    $scope.itemsSource = cv;
});


