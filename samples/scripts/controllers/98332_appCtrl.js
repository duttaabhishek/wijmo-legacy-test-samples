﻿// get application
var app = angular.module('app');

// add controller
app.controller('appCtrl', function appCtrl($scope) {
    // expose some data
    $scope.countries = 'US,Germany,UK,Japan,Italy,Greece'.split(',');
    $scope.products = 'Widget,Gadget,Doohickey'.split(',');
    $scope.colors = 'Black,White,Red,Green,Blue'.split(',');
    $scope.view = new wijmo.CollectionView(getData(1));

   // data context
    $scope.ctx = {
        flex: null
    };

    $scope.groupBy = '';
    // update CollectionView group descriptions when groupBy changes
    $scope.$watch('groupBy', function () {
        var cv = $scope.view;
        cv.groupDescriptions.clear();
        if ($scope.groupBy) {
              var groupNames = $scope.groupBy.split(',');
	for (var i = 0; i < groupNames.length; i++) {
                var groupName = groupNames[i];
                    var groupDesc = new wijmo.PropertyGroupDescription(groupName);
                    cv.groupDescriptions.push(groupDesc);
	            }        
	}
    });

    // some random data
    function getData(count) {
        var data = [],
            countries = $scope.countries,
            products = $scope.products,
            colors = $scope.colors,
            dt = new Date(2010,2,19);
        for (var i = 0; i < count; i++) {
            data.push({
                id: i,
                date: new Date(dt.getFullYear(), i % 12, 25, i % 24, i % 60, i % 60),
                country: countries[i% countries.length],
                product: products[i% products.length],
                color: colors[i% colors.length],
                amount: i * 10000 - 5000,
                discount: i / 4,
                active: i % 4 == 0,
            });
        }
        return data;
    }
});
