﻿$(document).ready(function () {

    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
        data = [];
    for (var i = 0; i < countries.length; i++) {
        data.push({
            country: countries[i],
            downloads: 10* 20000,
            sales: 15 * 10000,
            expenses: 30 * 5000
        });
    }

    // create CollectionView on the data (to get events)
    var view = new wijmo.CollectionView(data);

    // initialize the chart
    var chart = new wijmo.chart.FlexChart('#theChart', {
        itemsSource: view,
        bindingX: 'country',
        series: [{
            binding: 'sales',
            name: 'Sales'
        }, {
            binding: 'downloads',
            name: 'Downloads',
            chartType: wijmo.chart.ChartType.LineSymbols
        }],
        selectionMode: wijmo.chart.SelectionMode.Point
    });
});