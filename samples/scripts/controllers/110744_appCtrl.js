﻿// define app, include Wijmo 5 directives
var app = angular.module('app', ['wj']);

// controller
app.controller('appCtrl', function ($scope) {

    // create some random data
    var countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
        data = [];
    for (var i = 0; i < 15; i++) {
        data.push({
            country: countries[i % countries.length],
            // downloads: Math.round(Math.random() * 20000),
            // sales: Math.random() * 10000,
            // expenses: Math.random() * 5000,
            // money: { a: Math.random() * 5000 }
            downloads: Math.round(i * 20000),
            sales: i * 10000,
            expenses: i * 5000,
            money: { a: i * 5000 }
        });
    }

    // data is the main collection view:
    $scope.data = new wijmo.CollectionView(data);
    $scope.flexGrid = new wijmo.grid.FlexGrid('#flexGrid');
    $scope.flexGridFilter = new wijmo.grid.filter.FlexGridFilter($scope.flexGrid);
    $scope.flexGrid.initialize({
        autoGenerateColumns: false,
        itemsSource: $scope.data,
        columns: [
			{ header: "country", binding: "country", width: "*", minWidth: 100 },
            { header: "sales", binding: "sales", width: "*", format: "n0", aggregate: "Sum", minWidth: 100 },
            { header: "downloads", binding: "downloads", width: "*", format: "n0", aggregate: "Sum", minWidth: 100 },
            { header: "expenses", binding: "expenses", width: "*", format: "n0", aggregate: "Sum", minWidth: 100 },
            { header: "money", binding: "money.a", width: "*", format: "c2", aggregate: "Sum", minWidth: 100}]
    });
    $scope.grouping = new wijmo.PropertyGroupDescription("country");
    $scope.flexGrid.collectionView.groupDescriptions.push($scope.grouping);
});