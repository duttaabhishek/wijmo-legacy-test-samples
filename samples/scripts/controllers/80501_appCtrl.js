﻿(function () {
    'use strict';

    angular
		.module('app', ['wj'])
		.controller('appCtrl', function ($scope) {
		    $scope.fruits = [
     { "date1": new Date(2014, 1, 1), "y1": 20, "y2": 8, "y3": 10, "y4": 16 },
     { "date1": new Date(2014, 2, 2), "y1": 22, "y2": 12, "y3": 16, "y4": 19 },
     { "date1": new Date(2014, 3, 3), "y1": 19, "y2": 10, "y3": 17, "y4": 15 },
     { "date1": new Date(2014, 4, 4), "y1": 24, "y2": 12, "y3": 15, "y4": 22 },
     { "date1": new Date(2014, 5, 5), "y1": 25, "y2": 15, "y3": 23, "y4": 18 },
		    ];


		    $scope.ctx = {
		        chart: null
		    };

		    $scope.$watch('ctx.chart', function () {
		        if ($scope.ctx.chart) {
		            var chart = $scope.ctx.chart;
		            chart.beginUpdate();
		            chart.chartType = "LineSymbols";
		            chart.stacking = "Stacked100pc";
		            chart.endUpdate();
		        }
		    });

		});
})();