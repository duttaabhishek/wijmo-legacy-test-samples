﻿'use strict';

// declare app module
var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope) {
    // data context
    $scope.ctx = {
        chart: null,
        palette: 'standard',
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        itemsSource: [],
        fruits: [],
        culture: 'en'
    };

    $scope.menuPaletteItemClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.palette = wijmo.chart.Palettes[$scope.ctx.palettes[menu.selectedIndex]];
    }

    // populate itemsSource
    var names = ['Oranges', 'Apples', 'Pears', 'Bananas', 'Pineapples', 'Limes', 'Watermelons', 'Grapes', 'Pineapples', 'Strawberries'],
        data = [];
    for (var i = 0; i < names.length; i++) {
        data.push({
            name: names[i],
            value: Math.round(Math.random() * 100)
        });
    }
    $scope.ctx.itemsSource = data;
    //chart binding
    $scope.fruits = [
     { "name": "Orange", "food": "Fried Rice", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 1, 1), "y1": 10, "y2": 8, "y3": 10, "y4": -16 },
     { "name": "apple", "food": "Fried Noodle", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 2, 2), "y1": 20, "y2": 12, "y3": 16, "y4": -19 },
     { "name": "lime", "food": "Fried Egg", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 3, 3), "y1": 30, "y2": 10, "y3": 17, "y4": 15 },
     { "name": "lemon", "food": "Fried Vermicelli", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 4, 4), "y1": 40, "y2": 12, "y3": 15, "y4": -22 },
     { "name": "mango", "food": "Chicken Soup", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 5, 5), "y1": 50, "y2": 15, "y3": 23, "y4": -18 },
     { "name": "Watermelon", "food": "Pork Soup", "x": 6, "date1": new Date(2014, 6, 6), "date2": new Date(2014, 6, 6), "y1": 60, "y2": 15, "y3": 23, "y4": -18 },
     { "name": "Kiwi", "food": "Beef Soup", "x": 7, "date1": new Date(2014, 7, 7), "date2": new Date(2014, 7, 7), "y1": 70, "y2": 15, "y3": 23, "y4": -18 }
    ];

    $scope.label=function(){
            var chart = $scope.ctx.chart;
            chart.plotMargin = 10;
            chart.itemFormatter = function (engine, hitTestInfo, defaultFormatter) {
                var fsz = engine.fontSize;
                engine.fontSize = '10';
                defaultFormatter();
                var point = hitTestInfo.point.clone();
                var text = hitTestInfo.name + '=' + hitTestInfo.value.toFixed(1);
                var sz = engine.measureString(text);
                var fill = engine.fill;
                engine.fill = 'white';
                engine.drawRect(point.x - 2 - sz.width / 2, point.y - sz.height - 2, sz.width + 4, sz.height + 4);
                engine.fill = fill;
                point.x -= sz.width / 2;
                engine.drawString(text, point);
                engine.fontSize = fsz;
            }
        }

//culture
         $scope.$watch('ctx.culture', function () {
	    $.ajax({
	        url: '../scripts/vendor/wijmo.culture.' + $scope.ctx.culture + '.min.js',
	        dataType: 'script',
	        success: function (data) {
	            invalidateAll(); // invalidate all controls to show new culture
	        },
	    });
	});

    // invalidate all Wijmo controls
    // using a separate function to handle strange IE9 scope issues
	function invalidateAll() {
	    wijmo.Control.invalidateAll();
	}
    
      //rendering Event
    $scope.chartRendering = function () {
        if ($("#chkRendering").is(":checked")) {
            var value = "Rendering Event Fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else {
            $("#EventList").append("");
        }
    }

     //rendered Event
    $scope.chartRendered = function () {
        if ($("#chkRendered").is(":checked")) {
            var value = "Rendered Event Fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else {
            $("#EventList").append("");
        }
    }

     //lost focus Event
     $scope.chartLostFocus = function () {
        if ($("#chklostFocus").is(":checked"))
        {
            var value = " LostFocus event fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else
        {
            $("#EventList").append("");
        }
            
    }

     //gotFocus Event
    $scope.chartGotFocus = function () {
        if ($("#chkgotFocus").is(":checked"))
        {
            var value = " GotFocus event fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else
        {
            $("#EventList").append("");
        }
    }

     $scope.labelContent = function () {
        var fchart = $scope.ctx.chart;
        fchart.dataLabel.content="{y}";
        fchart.refresh(true);
    };

});