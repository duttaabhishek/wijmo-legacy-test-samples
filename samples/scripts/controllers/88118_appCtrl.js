﻿

var app = angular.module('app');

// app controller provides data
app.controller('appCtrl', function appCtrl($scope, $filter, $location, $compile, dataSvc) {

    var cvData = new wijmo.CollectionView(dataSvc.getData(10));
    // Add sorting 
    cvData.sortDescriptions.push(new wijmo.SortDescription("country", true));
    cvData.groupDescriptions.push(new wijmo.PropertyGroupDescription("country"));

    // data context
    $scope.ctx = {
        flex: null,
        flexInline: null,
        itemCount: 30,
        data: cvData,
        countries: dataSvc.getCountries(),
        products: dataSvc.getProducts(),
        colors: dataSvc.getColors(),
        culture: 'en'
    };




});