﻿'use strict';

var app = angular.module('app');

app.controller('appCtrl', function appCtrl($scope) {
 // data context
    $scope.ctx = {
        chart: null,
        palette: 'standard',
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        data: [],
        fruits: [],
        labels: 0,
        culture: 'en'
    };
    //chart binding
    $scope.fruits = [
    { "name": "Orange", "food": "Fried Rice", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 1, 1), "y1": 10, "y2": 8, "y3": 10, "y4": -16,"y5":1 },
     { "name": "apple", "food": "Fried Noodle", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 2, 2), "y1": 20, "y2": 12, "y3": 16, "y4": -19,"y5":2 },
     { "name": "lime", "food": "Fried Egg", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 3, 3), "y1": 30, "y2": 10, "y3": 17, "y4": 15 ,"y5":3},
     { "name": "lemon", "food": "Fried Vermicelli", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 4, 4), "y1": 40, "y2": 12, "y3": 15, "y4": -22 ,"y5":4},
     { "name": "mango", "food": "Chicken Soup", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 5, 5), "y1": 50, "y2": 15, "y3": 23, "y4": -18,"y5":5 },
     { "name": "Watermelon", "food": "Pork Soup", "x": 6, "date1": new Date(2014, 6, 6), "date2": new Date(2014, 6, 6), "y1": 60, "y2": 15, "y3": 23, "y4": -18,"y5":6 },
     { "name": "Kiwi", "food": "Beef Soup", "x": 7, "date1": new Date(2014, 7, 7), "date2": new Date(2014, 7, 7), "y1": 70, "y2": 15, "y3": 23, "y4": -18,"y5":7 },
    ];
     $scope.xFormatP = function () {
        var fchart = $scope.ctx.chart;
        fchart.axisX.format = "p";
    };
	 $scope.xFormatCur = function () {
        var fchart = $scope.ctx.chart;
        fchart.axisX.format = "C";
    };
	  $scope.yFormatP = function () {
        var fchart = $scope.ctx.chart;
        fchart.axisY.format = "p";
    };
    $scope.yFormatCur = function () {
        var fchart = $scope.ctx.chart;
        fchart.axisY.format = "C";
    };
    //show data label 
     $scope.showDataLabel = false;
     $scope.$watch('showDataLabel', function () {
            var showDataLabel = $scope.showDataLabel;

            if ($scope.ctx.chart) {
                $scope.ctx.chart.dataLabel.content = showDataLabel ? '{y}' : '';
            }
        });

        
	$scope.$watch('ctx.culture', function () {
	    $.ajax({
	        url: '../scripts/vendor/wijmo.culture.' + $scope.ctx.culture + '.min.js',
	        dataType: 'script',
	        success: function (data) {
	            invalidateAll(); // invalidate all controls to show new culture
	        },
	    });
	});

    // invalidate all Wijmo controls
    // using a separate function to handle strange IE9 scope issues
	function invalidateAll() {
	    wijmo.Control.invalidateAll();
	}
});