﻿'use strict';

var app = angular.module("app", ['wj']);
 
app.controller('appCtrl', function appCtrl($scope) {
 var pt = new wijmo.Point();
    // data context
    $scope.ctx = {
        chart: null,
        lineMarker:null,
        wfchart:null,
        waterfall:null,
        pal: 0,
        palettes: ['standard', 'cocoa', 'coral', 'dark', 'highcontrast', 'light', 'midnight', 'minimal', 'modern', 'organic', 'slate'],
        data: [],
        groupWidth: '70%',
        labels: 0,
        selector:null
    };
    $scope.data = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": -2000, "y2": 1000, "y3": 6000, "y4": -6000 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 4000, "y2": 2000, "y3": 5000, "y4": -2000 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": -6000, "y2": 3000, "y3": 7000, "y4": -5000 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 8000, "y2": 4000, "y3": 8000, "y4": -8000 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": -10000, "y2": 5000, "y3": 4000, "y4": -4000 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 12000, "y2": 6000, "y3": 9000, "y4": -10000 }
    ];
    $scope.menu = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 900, "y2": 600, "y3": 400, "y4": 300 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 800, "y2": 400, "y3": 500, "y4": 200 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 700, "y2": 700, "y3": 700, "y4": 500 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 600, "y2": 800, "y3": 800, "y4": 600 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 500, "y2": 100, "y3": 300, "y4": 100 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 400, "y2": 200, "y3": 900, "y4": 400 },
	  { "fruit": "Kiwi", "x": 7, "date1": new Date(2014, 0, 7), "date2": new Date(2014, 14, 9), "date3": new Date(2014, 4, 6), "y1": 300, "y2": 300, "y3": 600, "y4": 700 },
    ];
    $scope.fruits = [
      { "fruit": "Orange", "x": 1, "date1": new Date(2014, 0, 1), "date2": new Date(2014, 2, 3), "date3": new Date(2014, 1, 4), "y1": 2000000, "y2": 0.1, "y3": 0.6, "y4": 0.6 },
      { "fruit": "Apple", "x": 2, "date1": new Date(2014, 0, 2), "date2": new Date(2014, 4, 4), "date3": new Date(2014, 2, 6), "y1": 4000000, "y2": 0.2, "y3": 0.5, "y4": 0.2 },
      { "fruit": "Lime", "x": 3, "date1": new Date(2014, 0, 3), "date2": new Date(2014, 6, 5), "date3": new Date(2014, 2, 7), "y1": 6000000, "y2": 0.3, "y3": 0.7, "y4": 0.5 },
      { "fruit": "Lemon", "x": 4, "date1": new Date(2014, 0, 4), "date2": new Date(2014, 8, 6), "date3": new Date(2014, 3, 8), "y1": 8000000, "y2": 0.4, "y3": 0.8, "y4": 0.8 },
      { "fruit": "Mango", "x": 5, "date1": new Date(2014, 0, 5), "date2": new Date(2014, 10, 7), "date3": new Date(2014, 4, 9), "y1": 10000000, "y2": 0.5, "y3": 0.4, "y4": 0.4 },
	  { "fruit": "Watermelon", "x": 6, "date1": new Date(2014, 0, 6), "date2": new Date(2014, 12, 8), "date3": new Date(2014, 4, 7), "y1": 12000000, "y2": 0.6, "y3": 0.9, "y4": 0.9}
    ];
     $scope.candle = [
     { "name": "Orange", "food": "Fried Rice", "x": 1, "date1": new Date(2014, 1, 1), "date2": new Date(2014, 1, 1), "y1": 10, "y2": 8, "y3": 10, "y4": 16 ,"hi":500,"lo":100,"open":200,"close":300 },
     { "name": "apple", "food": "Fried Noodle", "x": 2, "date1": new Date(2014, 2, 2), "date2": new Date(2014, 2, 2), "y1": 20, "y2": 12, "y3": 16, "y4": 19,"hi":600,"lo":200,"open":200,"close":400 },
     { "name": "lime", "food": "Fried Egg", "x": 3, "date1": new Date(2014, 3, 3), "date2": new Date(2014, 3, 3), "y1": 30, "y2": 10, "y3": 17, "y4": 15,"hi":500,"lo":200,"open":300,"close":400 },
     { "name": "lemon", "food": "Fried Vermicelli", "x": 4, "date1": new Date(2014, 4, 4), "date2": new Date(2014, 4, 4), "y1": 40, "y2": 12, "y3": 15, "y4": 22,"hi":600,"lo":100,"open":200,"close":400 },
     { "name": "mango", "food": "Chicken Soup", "x": 5, "date1": new Date(2014, 5, 5), "date2": new Date(2014, 5, 5), "y1": 50, "y2": 15, "y3": 23, "y4": 18,"hi":500,"lo":100,"open":200,"close":300},
    ];
    var countries = 'UnitedState,Germany,UnitedKingdom,Japan,Italy,Greece'.split(',');
    for (var i = 0; i < countries.length; i++) {
        $scope.ctx.data.push({
            country: countries[i],
            downloads: Math.round(Math.random() * 20000),
            sales: Math.random() * 10000,
            expenses: Math.random() * 5000,
			uploads: Math.random() * 2000,
			profits: Math.random() * 1000,
			loss: Math.random() * 500,
			extra: Math.random() * 300,
			secondextra: Math.random() * 200
        });
    }
    var names = ['Oranges', 'Apples', 'Pears', 'Bananas', 'Pineapples','Limes','Watermelons','Grapes','Pineapples','Strawberries'],
        data = [];
    for (var j = 0; j < names.length; j++) {
        data.push({
            name: names[j],
            value: Math.round(Math.random() * 100)
        });
    }
    $scope.ctx.itemsSource = data;

    $scope.xMax = function () {
        var fchart = $scope.ctx.chart;
        fchart.beginUpdate();
        fchart.axisX.max = 800;
        fchart.endUpdate();
    };
    $scope.xMin = function () {
        var fchart = $scope.ctx.chart;
        fchart.beginUpdate();
        fchart.axisX.min = 400;
        fchart.endUpdate();
    };
    $scope.menuPaletteItemClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.palette = wijmo.chart.Palettes[$scope.ctx.palettes[menu.selectedIndex]];
    }
     $scope.labelsClicked = function (sender, args) {
        var menu = sender;
        $scope.ctx.chart.dataLabel.position = menu.selectedValue;
    }
    $scope.threshold = function () {
        var fchart = $scope.ctx.chart;
        fchart.tooltip.threshold = 100;
    };
     $scope.tooltipContent = function () {
        var fchart = $scope.ctx.chart;
        fchart.tooltip.content = '<b>Sales</b><br/>MyTooltip';
    };
     $scope.isContentHtml = function () {
        var fchart = $scope.ctx.chart;
        fchart.tooltip.content = '<b>Sales</b><br/>MyTooltip';
        fchart.tooltip.isContentHtml = !fchart.tooltip.isContentHtml;
    };
      $scope.symbolSize = function () {
        var fchart = $scope.ctx.chart;
        fchart.symbolSize = 5;
    };
    $scope.xFormat = function () {
        var fchart = $scope.ctx.chart;
        fchart.axisX.format = "p";
    };
    $scope.yFormat = function () {
        var fchart = $scope.ctx.chart;
        fchart.axisY.format = "C";
    };
    //Begin update
    $scope.$watch('ctx.chart', function () {
        if ($scope.ctx.chart) {
            var chart = $scope.ctx.chart;
            chart.beginUpdate();
            $("#EventList").append('<li>' + "BeginUpdate" + '</li>');
        }
    });
    //End update
    $scope.$watch('ctx.chart', function () {
        if ($scope.ctx.chart) {
            var chart = $scope.ctx.chart;
            chart.endUpdate();
           $("#EventList").append('<li>' + "EndUpdate" + '</li>');
        }
    });
    //series rendering
     $scope.chartSeriesRendering = function () {
        if ($("#chkChartseriesRendering").is(":checked")) {
            var value = "SeriesRendering event fired";
            $("#chartSeriesRenderingEventList").append('<li>' + value + '</li>')
        }
        else {
            $("#chartSeriesRenderingEventList").append("");
        }
    }

    //waterfall
     $scope.seriesRendering = function () {
        if ($("#chkwaterfallrendering").is(":checked")) {
            var value = "SeriesRendering event fired";
            $("#EventList").append('<li>' + value + '</li>')
        }
        else {
            $("#EventList").append("");
        }
    }
    //selectionChanged Event
    $scope.chartSelectionChanged = function () {
        if ($("#chkselectionChanged").is(":checked"))
            var value = " selectionChanged event fired";
        $("#sChangedEventList").append('<li>'+value+'</li>')
    }

    //LineMarker
    $scope.positionChanged = function (s, point) {
        pt = point;

        $scope.changeContent = function () {
            var html = '', chart = $scope.ctx.chart;
            chart.series.forEach(function (s, i) {
                var ht = s.hitTest(new wijmo.Point(pt.x, NaN));

                // find series lines to get its color
                var polyline = $(s.hostElement).find('polyline')[0];

                // add series info to the marker content
                if (ht.x && ht.y !== null) {
                    html += '<div style="color:' + polyline.getAttribute('stroke') + '">' + ht.name + ' = ' + ht.y.toFixed(2) + '</div>';
                }
            });
            return html;
        }
    }
    $scope.seriesIndex = function () {
        var marker = $scope.ctx.lineMarker;
        var txt = marker.seriesIndex;
        $("#lbl").append(txt)
    }

    //Add by Selena   
    $scope.chartType = wijmo.chart.ChartType.Column;
    $scope.gradientChart = null;

    $scope.chartProps = {
        gradientFill: '',
        gradientType: 'l',
        gradientDirection: 'horizontal',
        startColor: '#ff0000',
        startOffset: 0,
        startOpacity: 1,
        endColor: '#0000ff',
        endOffset: 1,
        endOpacity: 1
    };

    //#region GradientChart
    $scope.$watch('gradientChart', function () {
        var gradientChart = $scope.gradientChart;

        if (gradientChart != null) {
            applyGradientColor();
        }
    });

    function applyGradientColor() {
        if ($scope.gradientChart == null) {
            return;
        }

        var chart = $scope.gradientChart,
            color = '',
            props = $scope.chartProps,
            type = props.gradientType,
            direction = props.gradientDirection;

        color = type;
        if (type === 'l') {
            if (direction === 'horizontal') {
                color += '(0, 0, 1, 0)';
            } else {
                color += '(0, 0, 0, 1)';
            }
        } else {
            color += '(0.5, 0.5, 0.5)'
        }
        color += props.startColor;
        if (props.startOffset !== 0 && props.startOffset !== 1) {
            color += ':' + props.startOffset;
        }
        if (props.startOpacity !== 1) {
            color += ':' + props.startOpacity;
        }
        color += '-' + props.endColor;
        if (props.endOffset !== 0 && props.endOffset !== 1) {
            color += ':' + props.endOffset;
        }
        if (props.endOpacity !== 1) {
            color += ':' + props.endOpacity;
        }

        $scope.chartProps.gradientFill = color;
        chart.series[0].style = {
            fill: color
        };
    }
    //#endregion



});
