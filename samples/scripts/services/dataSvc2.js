﻿'use strict';

// defines functions that can be called from controllers and directives
angular.module('app').factory('dataSvc', function () {

    // data used to generate random items
    var countries = [ 'Germany', 'UK', 'Japan', 'Italy', 'US','Greece'];//, 'France', 'Austria', 'Canada', 'Denmark' ];
    var products = ['Widget', 'Gadget', 'Doohickey'];
    var colors = ['Black', 'White', 'Red', 'Green', 'Blue', 'Yellow'];//, 'Orange', 'Brown'];

    return {

        // get possible values for each field
        getCountries: function() {
            return countries;
        },
        getProducts: function() {
            return products;
        },
        getColors: function() {
            return colors;
        },

        // get matches for a search term
        getData: function (count, unique) {
            var data = [];
            var dt = new Date();

            // if unique items, limit to number of countries
            if (unique == true) {
                count = countries.length;
            }

            // add count items
            var c = 0, pc = 0;
            for (var i = 1; i <= count; i++) {
                if (c < 5) c++;
                else c = 0;
                if (pc < 2) pc++;
                else pc = 0;

                // constants used to create data items
                var date = new Date(2014, i % 12, 25, i % 24, i % 60, i % 60),
                    countryId = unique == true ? i : c,
                    productId = Math.floor(pc),
                    colorId = Math.floor(c);

                // create the item
                var item = {
                    id: i,
                    start: date,
                    end: date,
                    country: countries[countryId],
                    product: products[productId],
                    color: colors[colorId],
                    countryId: countryId,
                    productId: productId,
                    colorId: colorId,
                    amount:c * 10000 - 5000,
                    amount2: pc * 10000 - 5000,
                    discount: c / 4,
                    active: c % 4 == 0,
                };

                // add an array (should not auto-bind)
                item.sales = [];
                var ram=0;
                for (var j = 0; j < 12; j++) {
                if (ram==4) ram =0;
                    item.sales.push(50 + 20 * (ram - .5) + j);
                    ram++;
                }

                // add an object (should not auto-bind)
                item.someObject = {
                    name: i,
                    value: i
                };

                // add lots of columns to test virtualization
                if (false) {
                    for (var j = 0; j < 400; j++) {
                        item['x' + j] = j;
                    }
                }

                // add the item to the list
                data.push(item);
            }
            return data;
        },

        // get matches for a search term
        getDataDec: function (count, unique) {
            var data = [];
            var dt = new Date();

            // if unique items, limit to number of countries
            if (unique == true) {
                count = countries.length;
            }

            // add count items
            var c = 0, pc = 0;
            for (var i = 1; i <= count; i++) {
                if (c < 5) c++;
                else c = 0;
                if (pc < 2) pc++;
                else pc = 0;

                // constants used to create data items
                var date = new Date(2014, i % 12, 25, i % 24, i % 60, i % 60),
                    countryId = unique == true ? i : c,
                    productId = Math.floor(pc),
                    colorId = Math.floor(c);

                // create the item
                var item = {
                    id: i,
                    start: date,
                    end: date,
                    country: countries[countryId],
                    product: products[productId],
                    color: colors[colorId],
                    countryId: countryId,
                    productId: productId,
                    colorId: colorId,
                    amount:(c * 10000 - 5000)+(c/4),
                    amount2: (pc * 10000 - 5000)+(c/4),
                    discount: c / 4,
                    active: c % 4 == 0,
                };

                // add an array (should not auto-bind)
                item.sales = [];
                for (var j = 0; j < 12; j++) {
                    item.sales.push(50 + 20 * (j - .5) + j);
                }

                // add an object (should not auto-bind)
                item.someObject = {
                    name: i,
                    value: i
                };

                // add lots of columns to test virtualization
                if (false) {
                    for (var j = 0; j < 400; j++) {
                        item['x' + j] = j;
                    }
                }

                // add the item to the list
                data.push(item);
            }
            return data;
        }
    }
});
