const fs = require("fs-extra");
const path = require("path");
const config = require("../config/config.json");

let copyInfos = config.copy || [];

copyInfos.forEach(cInfo => {
  let srcLocation = path.resolve(__dirname, cInfo.src),
    targetLocation = path.resolve(__dirname, cInfo.target),
    sourceStats = fs.lstatSync(srcLocation);

  if (!sourceStats.isDirectory()) {
    fs.copySync(srcLocation, targetLocation);
  } else {
    fs.ensureDirSync(targetLocation);
    let files = fs.readdirSync(srcLocation);

    files.forEach(fileName => {
      let fileLocation = path.resolve(srcLocation, fileName),
        fileStats = fs.lstatSync(fileLocation);

      if (!fileStats.isDirectory()) {
        let add = !cInfo.extensions;
        if (cInfo.extensions) {
          for (let i = 0; i < cInfo.extensions.length; i++) {
            if (fileName.endsWith("." + cInfo.extensions[i])) {
              add = true;
              break;
            }
          }
        }

        if(cInfo.transformExtension){
          fileName = fileName.replace(/\w+$/i, cInfo.transformExtension);
        }

        if (add) {
          fs.copySync(fileLocation, path.resolve(targetLocation, fileName));
        }
      }
    });
  }
});
