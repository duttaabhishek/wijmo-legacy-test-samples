const fs = require("fs-extra");
const path = require("path");
const config = require("../config/config.json");

const mainDir = path.resolve(__dirname, config.wijmoLocation);
const targetLocation = path.resolve(__dirname, config.targetLocation);

const modulePrefix = "@grapecity";

let nonAngularModules = getModules(mainDir, folderName => {
  return /(angular)|(interop)|(cultures)|(knockout)/.test(folderName);
});
let angularModules = getModules(mainDir, folderName => {
  return !/(angular)/.test(folderName);
});
let koModules = getModules(mainDir, folderName => {
  return !/(knockout)/.test(folderName);
});

let script = getScript(nonAngularModules, modulePrefix);
script += getAngularModulesScript(angularModules, modulePrefix);
script += getKOModulesScript(koModules, modulePrefix);

fs.ensureDirSync(targetLocation);
fs.writeFileSync(path.resolve(targetLocation, "wijmo.module.js"), script);

function getModules(srcDir, skipFunction) {
  let modules = [],
    files = fs.readdirSync(srcDir);
  files.forEach(folderName => {
    let curLocation = path.resolve(srcDir, folderName);
    let folderStats = fs.lstatSync(curLocation);
    if (!folderStats.isDirectory()) {
      return;
    }

    if (skipFunction && skipFunction(folderName)) {
      return;
    }

    let filePath = path.resolve(curLocation, "index.js");
    if (!fs.existsSync(filePath)) {
      return;
    }

    modules.push(folderName);
  });
  return modules;
}

function getScript(modulesNames, modulePrefix = "@grapecity") {
  let script = "";

  modulesNames.forEach(mName => {
    let name = `${modulePrefix}/${mName}`,
      parts = mName.split("."),
      varName = parts.join("");

    script += `const ${varName} = require("${name}");\n`;
    script += parts.reduce((acc, cur) => {
      return acc + `["${cur}"]`;
    }, "window");
    script += ` = ${varName};\n`;
  });

  return script;
}

function getAngularModulesScript(moduleNames, modulePrefix = "@grapecity") {
  let script = `const angular = require("angular");\n`;
  script += `const wj = angular.module("wj", []);\n`;

  moduleNames.forEach(mName => {
    let moduleName = `${modulePrefix}/${mName}`,
      angularModName = mName.replace(`wijmo.angular.`, "");

    script += `require("${moduleName}")\n`;

    let moduleImport = `wj.requires.push("wj.${angularModName}");\n`;

    if (moduleImport.includes("base") || moduleImport.includes("core")) {
      return;
    }
    script += moduleImport;
  });

  return script;
}

function getKOModulesScript(moduleNames, modulePrefix = "@grapecity") {
  let script = `const ko = require("knockout");\n`;
  script += `window['ko'] = ko;\n`;

  moduleNames.forEach(mName => {
    let moduleName = `${modulePrefix}/${mName}`;

    script += `require("${moduleName}")\n`;
  });

  return script;
}