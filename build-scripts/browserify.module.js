const { exec } = require("child_process");
const fs = require("fs-extra");
const path = require("path");
const config = require("../config/config.json");

const outDir = path.resolve(__dirname, config.build.outputLocation);
const srcFile = path.resolve(__dirname, "../modules/wijmo.module.js");

fs.ensureDirSync(outDir);
const outFilePath = path.resolve(outDir, "wijmo.module.js");
exec(`browserify "${srcFile}" > "${outFilePath}"`, (err, stdout, stderr) => {
    if(err){
        throw err;
    }
    if(stderr){
        throw stderr;
    }

    var res = fs.readFileSync(outFilePath);
    fs.writeFileSync(outFilePath, "\ufeff"+res, {
        encoding: "utf8"
    });
});
