const fs = require("fs-extra");
const path = require("path");
const config = require("../config/config.json");
const readline = require("readline");

const srcLocation = path.resolve(__dirname, config.samplesLocation);
const resolveExtensions = ["htm", "html"];
const licenseKeyVarName = config.wijmoLicenseKeyEnvName;
const existingLicenseRgx = /[\n\r\s]*<script>([\n\r]|.)*?wijmo\.setLicenseKey\(([\n\r]|.)*?<\/script>[\n\r\s]*/gi;
const licenseRgx = /(<script.*?\/wijmo\.(?!(culture)).*?\.js.*?<\/script>)/im;
const wjVersionRgxOld = /(<body.*?>[\s\n\t\r]*)(<div\s*class="wj-version">(.|[\s\n\t\r])*?<\/div>)?/gmi;
const titleRgx = /<title>(.|[\s\n\t\r])*?<\/title>/gmi;
const newTitleRegex = /<head>([\s\n\t\r])*/gmi;

const packageInfo = require(config.packageInfoJSON);
let wjVersion = packageInfo["dependencies"]["@grapecity/wijmo"]["version"];

fs.ensureDirSync(srcLocation);

let samplesToUpdateLicense = getAllSamplesList(srcLocation);
updateLicense(samplesToUpdateLicense, process.env[licenseKeyVarName] || "");

function updateLicense(samples = [], key) {
  let totalSamples = samples.length,
    samplesConverted = 0;

  logProgress(samplesConverted / totalSamples);
  samples.forEach(sampleLocation => {
    let fileContents = fs.readFileSync(sampleLocation, {
      encoding: "utf-8"
    });

    // remove existing licence(if any)
    fileContents = fileContents.replace(
      existingLicenseRgx,
      "\n"
    );

    // add new licence
    fileContents = fileContents.replace(
      licenseRgx,
      `$1
        <script>
           wijmo.setLicenseKey('${key}');
        </script>`
    );

    // update wijmo version info(remove version info from body)
    fileContents = fileContents.replace(
      wjVersionRgxOld,
      `$1
      `
    );
    // remove existing title if any
    fileContents = fileContents.replace(
      titleRgx,
      ""
    );
    // add new title
    fileContents = fileContents.replace(
      newTitleRegex,
      `<head>
        <title>
          ${wjVersion}
        </title>`
    );


    fs.writeFileSync(sampleLocation, fileContents);
    samplesConverted++;
    logProgress(samplesConverted / totalSamples);
  });
}

function logProgress(curPercentage) {
  let str = (curPercentage * 100 || 0).toFixed(2) + " %";

  readline.clearLine(process.stdout, 0);
  readline.cursorTo(process.stdout, 0);

  process.stdout.write(`Completed: ${str}`);
}

function getAllSamplesList(srcLocation) {
  const samples = [];
  findAndFillSamples(srcLocation, samples);
  return samples;
}

function findAndFillSamples(srcLocation, samples) {
  let dirContents = fs.readdirSync(srcLocation);

  dirContents.forEach(name => {
    let curLocation = path.resolve(srcLocation, name),
      stats = fs.lstatSync(curLocation);

    if (stats.isDirectory()) {
      findAndFillSamples(curLocation, samples);
    } else if (fileMatchesResolvesExtensions(curLocation)) {
      // it is a file, add to the samples array
      samples.push(curLocation);
    }
  });
}

function fileMatchesResolvesExtensions(filePath) {
  for (let i = 0; i < resolveExtensions.length; i++) {
    if (filePath.endsWith(resolveExtensions[i])) {
      return true;
    }
  }

  return false;
}
