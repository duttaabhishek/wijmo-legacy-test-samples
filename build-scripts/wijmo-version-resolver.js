const fs = require("fs");
const path = require("path");
const config = require("../config/config.json");
const packageInfo = require("../package.json");

const versionVarName = config.wijmoVersionEnvName || "WijmoVersion";
const wijmoVersion = process.env[versionVarName];

if(wijmoVersion && packageInfo["devDependencies"]["@grapecity/wijmo.angular.all"] !== wijmoVersion){
    packageInfo["devDependencies"]["@grapecity/wijmo.angular.all"] = wijmoVersion;
    packageInfo["devDependencies"]["@grapecity/wijmo.knockout.all"] = wijmoVersion;

    // write updated json
    const packageLocation = path.resolve(__dirname, "../package.json");
    fs.writeFileSync(packageLocation, JSON.stringify(packageInfo, null, 2));
}
