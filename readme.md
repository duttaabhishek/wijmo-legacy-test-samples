# Wijmo Legacy Samples
These Wijmo samples are used to perform the Automation testing using Selenium. 
## Package Insallation
Run ***npm install*** to install the packages
## Build Project
To build the project run ***npm run build-all*** command
## Build Project on local machine
To build the project on local machine without embedding licence key and version number in samples, run ***npm run build-dev*** command

## Run the samples
Host the samples on local server(IIS) to get changes 

### Steps to host the samples on IIS

1. Press *Win+r* 
2. Type *inetmgr* and click on __OK__
3. Explore *Sites>Default Web Site*
4. Right Click | *Add Application...*
5. Provide the name of the application _wijmo-samples_ in **Alias**
6. Provide the path *~/{Project_name}/samples* in __Physical path__
7. Click on OK
8: Browse the samples using _http://localhost/wijmo-samples/{control}/{page_url}_